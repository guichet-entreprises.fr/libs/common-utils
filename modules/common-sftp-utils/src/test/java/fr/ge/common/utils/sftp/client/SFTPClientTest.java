package fr.ge.common.utils.sftp.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.github.stefanbirkner.fakesftpserver.rule.FakeSftpServerRule;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.sftp.conf.SFTPConfiguration;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Unit test for SFTP client.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SFTPClientTest extends AbstractTest {

    public static final String SFTP_PROTOCOL = "sftp";
    public static final String SFTP_HOST = "localhost";
    public static final String SFTP_USER_ID = "user";
    public static final String SFTP_USER_PASSWORD = "password";
    public static final Integer SFTP_PORT = 1234;
    public static final String SFTP_HOST_KEY_CHECKING = "no";
    public static final String SFTP_SERVER_ADDRESS = SFTP_HOST + ":" + SFTP_PORT;
    public static final String SFTP_RELATIVE_DIRECTORY_PATH = "directory";
    public static final String SFTP_RELATIVE_FILE_PATH = SFTP_RELATIVE_DIRECTORY_PATH + File.separator + "test.txt";

    @Rule
    public final FakeSftpServerRule sftpServer = new FakeSftpServerRule() //
            .setPort(SFTP_PORT) //
            .addUser(SFTP_USER_ID, SFTP_USER_PASSWORD);

    private static final Pattern DMTDU_REF = Pattern.compile("^.{33}TDMTDUPGUEN\\.zip$");

    private static final Pattern PATTERN_TXT = Pattern.compile("^.{4}\\.txt$");

    private SFTPClient client = new SFTPClient();

    /**
     * Set le up.
     */
    @Before
    public void setUp() throws IOException {
        this.client = new SFTPClient();
        this.client.setManager(new StandardFileSystemManager());

        final SFTPConfiguration config = new SFTPConfiguration();
        config.setProtocol(SFTP_PROTOCOL);
        config.setServerAddress(SFTP_HOST + ":" + SFTP_PORT);
        config.setUserId(SFTP_USER_ID);
        config.setPassword(SFTP_USER_PASSWORD);
        config.setStrictHostKeyChecking(SFTP_HOST_KEY_CHECKING);

        this.client.setConf(config);
        this.client.init();
    }

    /**
     * Tear down method.
     * 
     * @throws IOException
     */
    @After
    public void tearDown() throws IOException {
        if (null != this.client && null != this.client.getManager()) {
            this.client.getManager().close();
        }

        // -->Remove output files or directories
        this.sftpServer.deleteAllFilesAndDirectories();
    }

    /**
     * Upload single file.
     * 
     * @throws IOException
     */
    @Test
    public void testUpload() throws IOException {
        // -->prepare
        final String targetDirectory = "dist";

        // -->call
        this.client.upload(this.getClass().getClassLoader().getResource(".").getFile() + File.separator + SFTP_RELATIVE_FILE_PATH, targetDirectory);

        // -->verify
        assertEquals(SFTP_PROTOCOL + "://" + SFTP_USER_ID + ":" + SFTP_USER_PASSWORD + "@" + SFTP_SERVER_ADDRESS + "/" + SFTP_RELATIVE_FILE_PATH, client.buildURI(SFTP_RELATIVE_FILE_PATH));

        final List<String> remoteFiles = this.client.list("/" + targetDirectory, 1, DMTDU_REF);
        assertThat(remoteFiles.size(), notNullValue());
        assertThat(remoteFiles.size(), equalTo(1));
        assertThat(remoteFiles, containsInAnyOrder( //
                "test.txt" //
        ));
    }

    /**
     * Listing files inside SFTP remote directory.
     * 
     * @throws IOException
     *
     * @throws TechnicalException
     *             Technical exception
     */
    @Test
    public void testList() throws IOException {
        // -->prepare
        final InputStream stream = this.resourceAsStream("C1000A1001L000007D20191101H105012TDMTDUPGUEN.zip");

        // -->Upload same file twice into fake sftp server
        this.sftpServer.putFile("/directory/C00000000000000000000000000000000TDMTDUPGUEN.zip", stream);
        this.sftpServer.putFile("/directory/C00000000000000000000000000000001TDMTDUPGUEN.zip", stream);

        // -->call
        final List<String> remoteFiles = this.client.list("/directory", 20, DMTDU_REF);

        // -->verify
        assertThat(remoteFiles.size(), notNullValue());
        assertThat(remoteFiles.size(), equalTo(2));
        assertThat(remoteFiles, containsInAnyOrder( //
                "C00000000000000000000000000000001TDMTDUPGUEN.zip", //
                "C00000000000000000000000000000000TDMTDUPGUEN.zip" //
        ));
    }

    /**
     * Test creating file into remote server.
     *
     */
    @Test
    public void testCreate() throws FileSystemException, TechnicalException {
        // -->call
        this.client.create(SFTP_RELATIVE_FILE_PATH, FileType.FILE);

        // -->verify
        final List<String> remoteFiles = this.client.list(File.separator, 1, PATTERN_TXT);
        assertThat(remoteFiles.size(), notNullValue());
        assertThat(remoteFiles.size(), equalTo(1));
        assertThat(remoteFiles, containsInAnyOrder( //
                SFTP_RELATIVE_FILE_PATH //
        ));
    }

    /**
     * Test creating directory into remote server.
     *
     */
    @Test
    public void testCreateFolder() throws FileSystemException, TechnicalException {
        // -->call
        this.client.create(SFTP_RELATIVE_DIRECTORY_PATH, FileType.FOLDER);

        // -->verify
        final List<String> remoteFiles = this.client.list(File.separator, 1, PATTERN_TXT);
        assertThat(remoteFiles.size(), notNullValue());
        assertThat(remoteFiles.size(), equalTo(0));
    }

    /**
     * Download a remote file
     * 
     * @throws FileSystemException
     * @throws TechnicalException
     */
    @Test
    public void testDownload() throws FileSystemException, TechnicalException {
        // -->prepare
        this.client.upload(this.getClass().getClassLoader().getResource(".").getFile() + File.separator + SFTP_RELATIVE_FILE_PATH, SFTP_RELATIVE_DIRECTORY_PATH);

        // -->call
        this.client.download(SFTP_RELATIVE_FILE_PATH, this.getClass().getClassLoader().getResource(".").getFile() + File.separator + SFTP_RELATIVE_DIRECTORY_PATH);
    }
}
