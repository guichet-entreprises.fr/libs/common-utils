package fr.ge.common.utils.sftp.client;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.Selectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.sftp.conf.SFTPConfiguration;

/**
 * Client VFS utilisant le protocole SFTP pour transfèrer des fichiers entre un
 * système de fichier local et un serveur distant.
 * <p>
 * Le client implemente les méthodes de :
 * <ul>
 * <li>Télécharger</li>
 * <li>Téléverser</li>
 * <li>Créer</li>
 * <li>Lister</li>
 * </ul>
 * <p>
 * Ce client utilise une configuration <code>SFTPConfiguration</code> qui permet
 * d'indiquer au client les paramètre SFTP à utiliser
 * 
 * @see SFTPConfiguration
 */
public class SFTPClient extends AbstractClient<SFTPConfiguration> {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SFTPClient.class);

    /** The constant SERVER_ADDRESS. */
    private static final String SERVER_ADDRESS = "serverAddress";

    /** The constant USER_ID. */
    private static final String USER_ID = "userId";

    /** The constant PASSWORD. */
    private static final String PASSWORD = "password";

    /** The constant FILENAME. */
    private static final String FILENAME = "fileName";

    /** The constant SFTP_URI_TEMPLATE. */
    private static final String SFTP_URI_TEMPLATE = "sftp://${userId}:${password}@${serverAddress}/${fileName}";

    /** The constant RAM_URI_TEMPLATE. */
    private static final String RAM_URI_TEMPLATE = "ram://${fileName}";

    /**
     * {@inheritDoc}
     */
    @Override
    public String buildURI(final String filepath) {
        Map<String, String> uriProperties = new HashMap<String, String>();
        uriProperties.put(FILENAME, filepath);

        if ("sftp".equalsIgnoreCase(this.getConf().getProtocol())) {
            uriProperties.put(SERVER_ADDRESS, this.getConf().getServerAddress().trim());
            uriProperties.put(USER_ID, this.getConf().getUserId().trim());
            uriProperties.put(PASSWORD, this.getConf().getPassword().trim());

            return StrSubstitutor.replace(SFTP_URI_TEMPLATE, uriProperties);
        }

        return StrSubstitutor.replace(RAM_URI_TEMPLATE, uriProperties);

    }

    /**
     * Update a single file or directory to the remote server. If the repostiory
     * does not exist in the server, it will be created. Example :<br>
     * <code>upload("absolutPath/file.txt", "target")</code> will upload the
     * file <code>absolutPath/file.txt</code> in <code>target/file.txt</code>
     * 
     * @param filePath
     *            The local file path
     * @param target
     *            The remote directory
     * @throws TechnicalException
     *             The technical exception
     */
    public void upload(final String filePath, final String target) throws TechnicalException {
        final String[] path = StringUtils.split(filePath, File.separator);
        final String filename = path[path.length - 1];
        final String remoteToReceive = File.separator + target + File.separator + filename;

        try {
            final File file = new File(filePath);
            if (!file.exists()) {
                throw new TechnicalException("The file " + filePath + " does not exist");
            }

            final FileObject localFile = this.getManager().resolveFile(file.getAbsolutePath());
            final FileObject remoteFile = this.getManager().resolveFile(this.buildURI(remoteToReceive), this.getOptions());

            remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
            LOGGER.info("The file {} is correctly transfered to {}", remoteToReceive, this.getConf().getServerAddress());

        } catch (FileSystemException ex) {
            throw new TechnicalException("The file uploaded " + filePath + " is not correctly executed", ex);
        }
    }

    /**
     * Download a file from the remote server to the local file system. Example:
     * <br>
     * <code>download("root/src/example.txt", "target1/target2")</code> will
     * download the file <code>root/src/example.txt</code> into
     * <code>target1/target2/src/example.txt</code><br>
     * 
     * @param source
     *            The remote file to download
     * @param target
     *            The local directory
     * @throws TechnicalException
     *             The technical exception
     */
    public void download(final String source, final String target) throws TechnicalException {
        final StringBuilder localToReceive = new StringBuilder(target);
        try {
            final File srcFile = new File(source);
            final File file = new File(localToReceive.toString() + File.separator + srcFile.getName());

            final FileObject localFile = this.getManager().resolveFile(file.getAbsolutePath());
            final FileObject remoteFile = this.getManager().resolveFile(this.buildURI(source), this.getOptions());

            if (!remoteFile.exists()) {
                throw new TechnicalException("The file " + source + " is not found in " + this.getConf().getServerAddress());
            }

            localFile.copyFrom(remoteFile, Selectors.SELECT_SELF);
            LOGGER.info("The file {} is correctly uploaded from {}", source, this.getConf().getServerAddress());
        } catch (FileSystemException ex) {
            throw new TechnicalException("The file upload " + source + " is not correctly executed", ex);
        }
    }

    /**
     * Create a file or a directory into the remote server. The intermediate
     * files will be created if there are not existing. The target file will be
     * overwritten if it is already exist.
     * 
     * @param filePath
     *            The target relative file or directory path from the root
     *            directory
     * @param type
     *            Type of file to create : file or directory
     * @throws TechnicalException
     *             The technical exception
     */
    public void create(final String filePath, final FileType type) throws TechnicalException {
        String remotePath = filePath;
        try {
            final FileObject remoteFile = this.getManager().resolveFile(this.buildURI(remotePath), this.getOptions());

            if (FileType.FILE.equals(type)) {
                remoteFile.createFile();
                LOGGER.info("The file {} has been created {}", remotePath, this.getConf().getServerAddress());
            } else {
                remoteFile.createFolder();
                LOGGER.info("The directory {} has been created {}", remotePath, this.getConf().getServerAddress());
            }

        } catch (FileSystemException ex) {
            throw new TechnicalException("The file ou directory " + filePath + " cannot be created.", ex);
        }

    }

    /**
     * Return list of remote file path from remote server recursively based on
     * the input regular expression.
     * 
     * @param remotePath
     *            Relative path at the root of the SFTP user
     * @param max
     *            Maximum number of elements to return
     * @param pattern
     *            Regular expression
     * @return The list of remote file path
     * @throws TechnicalException
     *             The technical expression
     */
    public List<String> list(final String remotePath, final Integer max, final Pattern pattern) throws TechnicalException {
        try {
            final List<String> listZip = new ArrayList<String>();
            final String buildURI = this.buildURI(remotePath);
            LOGGER.info("List the following directory " + buildURI + " filtering using the pattern " + pattern.toString());
            final FileObject remoteDir = this.getManager().resolveFile(buildURI, this.getOptions());
            this.list(listZip, pattern, max, remoteDir, null, true);
            return listZip;
        } catch (FileSystemException e) {
            throw new TechnicalException("The directory " + remotePath + " cannot be listed.", e);
        }
    }

    /**
     * List files from a remote directory based on a regular expression.
     * 
     * @param listZip
     *            The result list of remore files
     * @param pattern
     *            The regular expression
     * @param max
     *            Maximum number of elements to return
     * @param directory
     *            The FileObject to scan
     * @param path
     *            The path from the root directory
     * @param recursive
     *            Enable recursive call
     * @throws TechnicalException
     *             The technical exception
     */
    private void list(final List<String> listZip, final Pattern pattern, final Integer max, final FileObject directory, final String path, final boolean recursive) throws TechnicalException {
        try {
            final Map<String, FileObject> filesMap = new HashMap<String, FileObject>();
            final Map<String, FileObject> folderMap = new HashMap<String, FileObject>();
            for (FileObject child : directory.getChildren()) {
                final String fileName = child.getName().getBaseName();
                if (FileType.FOLDER.equals(child.getType())) {
                    folderMap.put(fileName, child);
                } else {
                    // Si la liste est pleine, on sort
                    if (max != null && listZip.size() == max) {
                        break;
                    }

                    filesMap.put(fileName, child);

                    String repertoireContenant = StringUtils.EMPTY;
                    if (!StringUtils.isBlank(path)) {
                        repertoireContenant = path + File.separator;
                    }
                    listZip.add(repertoireContenant + fileName);
                }
            }

            // Récursif
            if (recursive && !folderMap.isEmpty()) {
                for (Map.Entry<String, FileObject> entry : folderMap.entrySet()) {
                    final String folderName = entry.getKey();
                    final FileObject fileObject = entry.getValue();

                    final StringBuilder subfolderPath = new StringBuilder(StringUtils.EMPTY);
                    if (!StringUtils.isBlank(path)) {
                        subfolderPath.append(path + File.separator);
                    }
                    subfolderPath.append(folderName);
                    this.list(listZip, pattern, max, fileObject, subfolderPath.toString(), true);
                }
            }
        } catch (FileSystemException e) {
            throw new TechnicalException("The directory " + directory + " cannot be listed.", e);
        }
    }
}
