package fr.ge.common.utils.sftp.conf;

/**
 * Classe de configuration du profil à utiliser pour le transfert SFTP. Elle
 * permet de paramétrer le client SFTP en indiquant:
 * <p>
 * <ul>
 * <li>L'adresse IP du serveur FTP à utiliser</li>
 * <li>L'identifiant et mot de passe de l'utilisateur côté serveur</li>
 * <li>Les options VFS à utiliser</li>
 * </ul>
 * 
 */
public class SFTPConfiguration extends VFSConfiguration {

    /** Adresse du seveur FTP à utiliser. */
    private String serverAddress;

    /** Identifiant de l'utilisateur à utiliser pour le transfère FTP. */
    private String userId;

    /** Mot de passe de l'utilisateur utilisé pour le transfère FTP . */
    private String password;

    /** indique à VFS s'il doit vérifier la client SSH client. */
    private String strictHostKeyChecking;

    /**
     * Get le server address.
     *
     * @return the serverAddress
     */
    public String getServerAddress() {
        return this.serverAddress;
    }

    /**
     * Set le server address.
     *
     * @param serverAddress
     *            the serverAddress to set
     */
    public SFTPConfiguration setServerAddress(final String serverAddress) {
        this.serverAddress = serverAddress;
        return this;
    }

    /**
     * Get le user id.
     *
     * @return the userId
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * Set le user id.
     *
     * @param userId
     *            the userId to set
     */
    public SFTPConfiguration setUserId(final String userId) {
        this.userId = userId;
        return this;
    }

    /**
     * Get le password.
     *
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Set le password.
     *
     * @param password
     *            the password to set
     */
    public SFTPConfiguration setPassword(final String password) {
        this.password = password;
        return this;
    }

    /**
     * Get le strict host key checking.
     *
     * @return the strictHostKeyChecking
     */
    public String getStrictHostKeyChecking() {
        return this.strictHostKeyChecking;
    }

    /**
     * Set le strict host key checking.
     *
     * @param strictHostKeyChecking
     *            the strictHostKeyChecking to set
     */
    public SFTPConfiguration setStrictHostKeyChecking(final String strictHostKeyChecking) {
        this.strictHostKeyChecking = strictHostKeyChecking;
        return this;
    }

}
