package fr.ge.common.utils.sftp.conf;

import java.util.Arrays;

/**
 * Classe de configuration minimale du profil VFS à étendre pour configurer une
 * implémentation de client VFS. Les paramètres génériques sont :
 * <p>
 * <ul>
 * <li>Le protocole à utiliser</li>
 * <li>Si le répertoire utlisateur est root</li>
 * <li>La durée Timeout</li>
 * </ul>
 * 
 */
public abstract class VFSConfiguration {

    /** La constante RAM. */
    private static final String RAM = "ram";

    /** La constante SFTP. */
    private static final String SFTP = "sftp";

    /** La constante FILE. */
    private static final String FILE = "file";

    /** La constante PROTOCOLS. */
    private static final String[] PROTOCOLS = { RAM, SFTP, FILE };

    /** Protocole à utiliser pour le tranfère de fichier. */
    private String protocol;

    /** timeout. */
    private int timeout;

    /**
     * Get le protocol.
     *
     * @return the protocol
     */
    public String getProtocol() {
        return this.protocol;
    }

    /**
     * Set le protocol.
     *
     * @param protocol
     *            the protocol to set
     */
    public VFSConfiguration setProtocol(final String protocol) {
        this.protocol = protocol;
        return this;
    }

    /**
     * Get le timeout.
     *
     * @return the timeout
     */
    public int getTimeout() {
        return this.timeout;
    }

    /**
     * Set le timeout.
     *
     * @param timeout
     *            the timeout to set
     */
    public VFSConfiguration setTimeout(final int timeout) {
        this.timeout = timeout;
        return this;
    }

    /**
     * Vérifi que la configuration est complete.
     */
    public void checkConfig() {
        if (this.protocol == null) {
            throw new IllegalArgumentException("Le protocole de VFSConfiguration ne peut pas être null");
        } else if (!Arrays.asList(PROTOCOLS).contains(this.protocol.toLowerCase())) {
            throw new IllegalArgumentException("Le protocole \"" + this.protocol + "\" n'est pas supporté");
        }

    }
}
