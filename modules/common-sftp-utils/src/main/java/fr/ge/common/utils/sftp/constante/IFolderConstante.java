/**
 * 
 */
package fr.ge.common.utils.sftp.constante;

/**
 * Constante liées au fichier.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public interface IFolderConstante {

    /** folder local temporary. */
    String FOLDER_LOCAL_TMP = "tmp";

}
