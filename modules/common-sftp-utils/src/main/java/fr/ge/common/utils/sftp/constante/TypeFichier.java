package fr.ge.common.utils.sftp.constante;

/**
 * Indique le type de fichier à traiter par le client SFTP.
 */
public interface TypeFichier {

    /** La constante TYPE_FILE. */
    String TYPE_FILE = "file";

    /** La constante TYPE_DIRECTORY. */
    String TYPE_DIRECTORY = "directory";

}
