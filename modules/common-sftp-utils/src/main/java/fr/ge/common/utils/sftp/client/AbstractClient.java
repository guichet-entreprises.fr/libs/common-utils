package fr.ge.common.utils.sftp.client;

import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import fr.ge.common.utils.sftp.conf.SFTPConfiguration;
import fr.ge.common.utils.sftp.conf.VFSConfiguration;

/**
 * Classe abstraite de client VFS : permet d'effectuer des commandes sur un
 * système de fichier local, distant ou virtuel. Elle se base sur la librairie
 * Apache Commons VFS 2.0 pour l'utilisation de differents protocoles.
 * <p>
 * Le client doit être initialisé avant utilisation. Il utilise une
 * configuration VFS qui variabilise au minimum des options de connexion et le
 * protocole et une configuration du système de fichier à utiliser.
 * <p>
 * Cette configuration implique l'initialisation de la propriété conf avec une
 * instance de <code>VFSConfiguration</code> et sesFS avec une instance de
 * <code>SesFSConfig</code> puis l'utilisation de la méthode init().
 * <p>
 * Le client support actuellement les protocoles :
 * <ul>
 * <li>SFTP</li>
 * <li>RAM</li>
 * <li>File</li>
 * </ul>
 *
 * @param <T>
 *            le type generique
 * @see VFSConfiguration
 * @See SesFSConfig
 */
public abstract class AbstractClient<T extends VFSConfiguration> {

    /**
     * Configuration du client VFS. Doit être setter avant l'utilisation de la
     * methode init()
     */
    protected T conf = null;

    /** Le options. */
    protected FileSystemOptions options;

    /** Le manager. */
    protected StandardFileSystemManager manager;

    /**
     * Méthode d'initialisation du client VFS. Elle doit être lancée avant
     * l'utilisation du client. La méthode init() vérifie que les configurations
     * <code>VFSConfiguration</code> et <code>SesFSConfig</code> du client ont
     * bien été chargés et elle instancie et initialise les objets
     * <code>StandardFileSystemManager</code> et <code>FileSystemOptions</code>
     * nécessaire à l'utilisation de VFS.
     *
     * @throws FileSystemException
     *             le file system exception
     */
    public void init() throws FileSystemException {
        if (this.conf == null) {
            throw new IllegalArgumentException("La propriété \"conf\" de type VFSConfiguration n'a pas été configurée dans le client");
        }

        if (this.manager == null) {
            this.manager = new StandardFileSystemManager();
        }
        this.conf.checkConfig();
        this.manager.init();
        this.options = this.createDefaultOptions();

    }

    /**
     * Récupère depuis la configuration VFS les options à utiliser.
     *
     * @return les options
     * @throws FileSystemException
     *             le file system exception
     */
    public FileSystemOptions createDefaultOptions() throws FileSystemException {
        FileSystemOptions opts = new FileSystemOptions();

        if ("sftp".equalsIgnoreCase(this.conf.getProtocol())) {
            SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, ((SFTPConfiguration) this.conf).getStrictHostKeyChecking());
            SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, Integer.valueOf(this.conf.getTimeout()));
        }

        return opts;

    }

    /**
     * Close.
     */
    public void close() {
        if (this.manager != null) {
            this.manager.close();
        }
    }

    /**
     * Set la configuration VFS à utiliser.
     *
     * @param conf
     *            configuration VFS
     */
    public AbstractClient<T> setConf(final T conf) {
        this.conf = conf;
        return this;
    }

    /**
     * Get le conf.
     *
     * @return the conf
     */
    public T getConf() {
        return this.conf;
    }

    /**
     * Set le FileSystemManager.
     *
     * @param manager
     *            le manager
     * @throws FileSystemException
     *             le file system exception
     */
    public AbstractClient<T> setManager(final StandardFileSystemManager manager) throws FileSystemException {
        this.manager = manager;
        return this;
    }

    /**
     * Get le manager.
     *
     * @return the manager
     */
    public StandardFileSystemManager getManager() {
        return this.manager;
    }

    /**
     * Construit l'URI à utiliser pour la transaction à l'aide de la
     * configuration VFS.
     *
     * @param filepath
     *            chemin relatif du fichier utiliser pour la transaction
     * @return l'URI
     */
    public abstract String buildURI(String filepath);

    /**
     * Get le options.
     *
     * @return the options
     */
    public FileSystemOptions getOptions() {
        return this.options;
    }

}