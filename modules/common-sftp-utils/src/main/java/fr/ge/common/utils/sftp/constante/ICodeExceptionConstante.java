/**
 * 
 */
package fr.ge.common.utils.sftp.constante;

/**
 * Constant listing the keys of exception provided by the project ge-ses-ct.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ICodeExceptionConstante {

    /** Error : SFTP file not found. */
    String SFTP_ERROR_FILE_NOT_FOUND = "SFTP_ERROR_FILE_NOT_FOUND";

    /**
     * Error : SFTP file is a non-empty folder, or if the file is read-only, or
     * on error deleteing this file.
     */
    String SFTP_ERROR_FILE_DELETING = "SFTP_ERROR_DELETING";

    /**
     * SFTP File has not been deleted.
     */
    String SFTP_FILE_NOT_DELETED = "SFTP_FILE_NOT_DELETED";
}
