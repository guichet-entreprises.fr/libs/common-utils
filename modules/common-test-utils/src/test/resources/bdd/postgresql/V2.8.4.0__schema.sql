-- ----------------------------------------------
-- Test table
-- ----------------------------------------------
CREATE TABLE tbl_test (
    id BIGINT NOT NULL,
    label VARCHAR(255) NOT NULL,
    content OID,
    json_value JSON,
    CONSTRAINT pk_tbl_test PRIMARY KEY (id)
);
