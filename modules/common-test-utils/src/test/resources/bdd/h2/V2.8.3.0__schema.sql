-- ----------------------------------------------
-- Test table
-- ----------------------------------------------
CREATE TABLE tbl_test (
    id BIGINT NOT NULL,
    label VARCHAR(255) NOT NULL,
    CONSTRAINT pk_tbl_test PRIMARY KEY (id)
);
