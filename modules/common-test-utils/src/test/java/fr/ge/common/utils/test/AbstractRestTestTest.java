/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-ws-server-context.xml" })
public class AbstractRestTestTest extends AbstractRestTest {

    private static final String URL_NOTHING = "/test/service/nothing";
    private static final String URL_HELLO = "/test/service/hello";
    private static final String USERNAME = "Jon Doe";
    private static final String EXPECTED_JSON = "{\"text\":\"Hello Jon Doe\"}";

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testAsString() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_HELLO).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(200)));
        assertThat(this.readAsString(response), equalTo(EXPECTED_JSON));
    }

    @Test
    public void testAsBytes() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_HELLO).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(200)));
        assertThat(this.readAsBytes(response), equalTo(EXPECTED_JSON.getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    public void testAsBean() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_HELLO).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(200)));

        final Map<String, Object> actual = this.readAsBean(response, new TypeReference<Map<String, Object>>() {
        });

        assertThat(actual, hasEntry("text", "Hello Jon Doe"));
    }

    @Test
    public void testAsStringBean() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_HELLO).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(200)));

        final String actual = this.readAsBean(response, String.class);

        assertThat(actual, equalTo(EXPECTED_JSON));
    }

    @Test
    public void testAsNullString() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_NOTHING).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(204)));
        assertThat(this.readAsString(response), nullValue());
    }

    @Test
    public void testAsNullBytes() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_NOTHING).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(204)));
        assertThat(this.readAsBytes(response), nullValue());
    }

    @Test
    public void testAsNullBean() throws Exception {
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path(URL_NOTHING).query("name", USERNAME).get();

        assertThat(response, hasProperty("status", equalTo(204)));

        final Map<String, Object> actual = this.readAsBean(response, new TypeReference<Map<String, Object>>() {
        });

        assertThat(actual, nullValue());
    }

}
