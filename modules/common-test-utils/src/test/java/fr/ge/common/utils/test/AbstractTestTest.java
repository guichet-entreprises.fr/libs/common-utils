/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils.test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author Christian Cougourdan
 */
public class AbstractTestTest extends AbstractTest {

    @Test
    public void testResourceAsBytes() throws Exception {
        final byte[] actual = this.resourceAsBytes("content.txt");

        assertThat(new String(actual, StandardCharsets.UTF_8), equalTo("This a unit test content"));
    }

    @Test
    public void testResourceAsBytesNotFound() throws Exception {
        final byte[] actual = this.resourceAsBytes("unknown.txt");
        assertThat(actual, notNullValue());
        assertThat(actual.length, equalTo(0));
    }

    @Test
    public void testResourceAsString() throws Exception {
        final String actual = this.resourceAsString("content.txt");

        assertThat(actual, equalTo("This a unit test content"));
    }

    @Test
    public void testResourceAsStringNotFound() throws Exception {
        final String actual = this.resourceAsString("unknown.txt");
        assertThat(actual, equalTo(""));
    }

    @Test
    public void testResourceAsZip() throws Exception {
        final byte[] actual = this.resourceAsBytes("archive.zip");

        assertThat(actual, notNullValue());

        final FileEntry indexFile = ZipUtil.entry("index.txt", actual);
        assertThat(indexFile, notNullValue());
        assertThat(new String(indexFile.asBytes(), StandardCharsets.UTF_8), equalTo("Nothing to find there"));

        final FileEntry listFile = ZipUtil.entry("in/list.txt", actual);
        assertThat(listFile, notNullValue());
        assertThat(new String(listFile.asBytes(), StandardCharsets.UTF_8), equalTo("Unknown unit test content"));

    }

    @Test
    public void testResourceAsZipUnknown() throws Exception {
        try {
            this.resourceAsBytes("unknown.zip");
            fail("TechnicalException expected");
        } catch (final TechnicalException ex) {
            assertThat(ex.getMessage(), equalTo("Resource [unknown.zip] not found"));
        }
    }

    @Test
    public void testRemove() throws Exception {
        final String folder = "target/abstract-test-resource-folder";
        final Path path = Paths.get(folder);
        path.toFile().mkdirs();

        assertTrue(Files.exists(path));

        this.remove(folder);

        assertFalse(Files.exists(path));
    }

    @Test
    public void testRemoveNone() throws Exception {
        this.remove("target/abstract-test-resource-folder");
    }

}
