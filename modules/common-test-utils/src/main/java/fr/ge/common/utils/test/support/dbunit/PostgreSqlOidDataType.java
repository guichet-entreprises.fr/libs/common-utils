/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.test.support.dbunit;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import org.dbunit.dataset.datatype.BytesDataType;
import org.dbunit.dataset.datatype.TypeCastException;
import org.postgresql.PGConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Christian Cougourdan
 */
public class PostgreSqlOidDataType extends BytesDataType {

    public static final Logger LOGGER = LoggerFactory.getLogger(PostgreSqlOidDataType.class);

    public PostgreSqlOidDataType() {
        super("OID", Types.BIGINT);
    }

    @Override
    public Object getSqlValue(final int column, final ResultSet resultSet) throws SQLException, TypeCastException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSqlValue(column={}, resultSet={}) - start", column, resultSet);
        }

        final Statement statement = resultSet.getStatement();
        final Connection cx = statement.getConnection();
        final boolean autoCommit = cx.getAutoCommit();
        // kinda ugly
        cx.setAutoCommit(false);

        try {
            final LargeObjectManager lobj = ((PGConnection) (cx.isWrapperFor(PGConnection.class) ? cx.unwrap(PGConnection.class) : cx)).getLargeObjectAPI();

            final long oid = resultSet.getLong(column);
            if (oid == 0) {
                LOGGER.debug("'oid' is zero, the data is NULL.");
                return null;
            }

            LargeObject obj = null;
            try {
                obj = lobj.open(oid, LargeObjectManager.READ);
                return obj.read(obj.size());
            } finally {
                if (null != obj) {
                    obj.close();
                }
            }

        } finally {
            cx.setAutoCommit(autoCommit);
        }
    }

    @Override
    public void setSqlValue(final Object value, final int column, final PreparedStatement statement) throws SQLException, TypeCastException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("setSqlValue(value={}, column={}, statement={}) - start", new Object[] { value, column, statement });
        }

        final Connection cx = statement.getConnection();
        final boolean autoCommit = cx.getAutoCommit();
        // kinda ugly
        cx.setAutoCommit(false);

        try {
            // Get the Large Object Manager to perform operations with
            final LargeObjectManager lobj = ((PGConnection) (cx.isWrapperFor(PGConnection.class) ? cx.unwrap(PGConnection.class) : cx)).getLargeObjectAPI();

            // Create a new large object
            final long oid = lobj.createLO(LargeObjectManager.READ | LargeObjectManager.WRITE);

            // Open the large object for writing
            final LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);

            // Now open the file
            final ByteArrayInputStream bis = new ByteArrayInputStream((byte[]) super.typeCast(value));

            // Copy the data from the file to the large object
            final byte buf[] = new byte[2048];
            int s = 0;
            while ((s = bis.read(buf, 0, 2048)) > 0) {
                obj.write(buf, 0, s);
            }

            // Close the large object
            obj.close();

            statement.setLong(column, oid);
        } finally {
            cx.setAutoCommit(autoCommit);
        }
    }

}
