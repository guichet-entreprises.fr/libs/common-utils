package fr.ge.common.utils.test;

import java.sql.Connection;

import org.dbunit.database.DatabaseConnection;

import fr.ge.common.utils.test.support.dbunit.CustomPostgresqlDataTypeFactory;

public class AbstractPostgreSqlDbTest extends AbstractDbTest {

    @Override
    protected DatabaseConnection getDatabaseConnection(final Connection cx) throws Exception {
        final DatabaseConnection dbUnitConnection = new DatabaseConnection(cx);
        dbUnitConnection.getConfig().setProperty("http://www.dbunit.org/properties/datatypeFactory", new CustomPostgresqlDataTypeFactory());
        return dbUnitConnection;
    }

}
