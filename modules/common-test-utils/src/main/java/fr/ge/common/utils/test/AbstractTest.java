/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class AbstractTest.
 *
 * @author Christian Cougourdan
 */
public abstract class AbstractTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /** The resources. */
    private final Map<String, byte[]> resources = new HashMap<>();

    public InputStream resourceAsStream(final String resourceName) {
        final String resourceFullName = this.resourceName(resourceName);
        final InputStream resourceAsStream = ResourceUtil.resourceAsStream(resourceFullName, this.getClass());
        if (null == resourceAsStream) {
            if (resourceName.endsWith(".zip")) {
                final byte[] resourceAsBytes = this.buildZipResource(resourceName);
                return new ByteArrayInputStream(resourceAsBytes);
            } else {
                this.logger.warn("Resource [{}] not found", resourceName);
                return null;
            }
        } else {
            return resourceAsStream;
        }
    }

    /**
     * Resource as bytes.
     *
     * @param name
     *            the name
     * @return the byte[]
     */
    public byte[] resourceAsBytes(final String name) {

        if (this.resources.containsKey(name)) {
            return this.resources.get(name);
        } else {
            byte[] asBytes;
            try (InputStream in = this.resourceAsStream(name)) {
                if (null == in) {
                    asBytes = new byte[] {};
                } else {
                    asBytes = IOUtils.toByteArray(in);
                }
                this.resources.put(name, asBytes);
            } catch (final IOException ex) {
                throw new TechnicalException("Error while reading resource [" + name + "]", ex);
            }
            return asBytes;
        }

    }

    /**
     * Builds the zip resource.
     *
     * @param name
     *            the name
     * @return the byte[]
     */
    private byte[] buildZipResource(final String name) {
        final String folderName = this.resourceName(name.replaceAll("\\.zip$", ""));

        final URL url = this.getClass().getResource(folderName);
        if (null == url) {
            throw new TechnicalException(String.format("Resource [%s] not found", name));
        } else {
            return ZipUtil.create(url);
        }
    }

    /**
     * Resource as string.
     *
     * @param name
     *            the name
     * @return the string
     */
    public String resourceAsString(final String name) {
        return new String(this.resourceAsBytes(name), StandardCharsets.UTF_8);
    }

    /**
     * Resource name.
     *
     * @param name
     *            the name
     * @return the string
     */
    public String resourceName(final String name) {
        if (name.startsWith("/")) {
            return name;
        } else {
            return String.format("%s-%s", this.getClass().getSimpleName(), name);
        }
    }

    /**
     * Remove recursively file or folder.
     *
     * @param path
     *            file or folder path
     * @throws IOException
     */
    public void remove(final String path) throws IOException {
        final Path basePath = Paths.get(path);

        if (!Files.exists(basePath)) {
            return;
        }

        Files.walk(basePath) //
                .sorted(Comparator.reverseOrder()) //
                .map(Path::toFile) //
                .forEach(File::delete);
        ;
    }

}
