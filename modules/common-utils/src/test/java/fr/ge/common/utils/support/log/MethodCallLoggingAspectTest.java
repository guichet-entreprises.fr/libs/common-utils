/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.support.log;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.aop.framework.ProxyFactory;

/**
 * 
 * @author Christian Cougourdan
 */
@RunWith(MockitoJUnitRunner.class)
public class MethodCallLoggingAspectTest {

    @Spy
    private MethodCallLoggingAspect aspect;

    private AopTestTarget proxy;

    @Before
    public void setUp() throws Exception {
        reset(this.aspect);

        final AopTestTarget target = new AopTestTarget();
        final ProxyFactory pf = new ProxyFactory(target);
        pf.addAdvice(this.aspect);

        this.proxy = (AopTestTarget) pf.getProxy();
    }

    @Test
    public void testNoArgs() throws Exception {
        this.aspect.setLogLevel("TRACE");
        this.proxy.callNoArgs();

        verify(this.aspect).addLog(any(), eq("Calling callNoArgs()"));
    }

    @Test
    public void testStringArg() throws Exception {
        this.aspect.setLogLevel("DEBUG");
        this.proxy.callStringArg("simple test");

        verify(this.aspect).addLog(any(), eq("Calling callStringArg(\"simple test\")"));
    }

    @Test
    public void testObjectArg() throws Exception {
        this.aspect.setLogLevel("INFO");
        this.proxy.callObjectArg(12L);

        verify(this.aspect).addLog(any(), eq("Calling callObjectArg(12)"));
    }

    @Test
    public void testBinaryArg() throws Exception {
        this.aspect.setLogLevel("WARN");
        final byte[] resourceAsBytes = "simple test".getBytes(StandardCharsets.UTF_8);
        this.proxy.callBinaryArgs(resourceAsBytes, new ByteArrayInputStream(resourceAsBytes));

        verify(this.aspect).addLog(any(), eq("Calling callBinaryArgs(__byte[]__, __ByteArrayInputStream__)"));
    }

    @Test
    public void testNullArg() throws Exception {
        this.proxy.callObjectArg(null);

        verify(this.aspect).addLog(any(), eq("Calling callObjectArg(null)"));
    }

    private static class AopTestTarget {

        public void callNoArgs() {
            // Nothing to do
        }

        public void callStringArg(String arg0) {
            // Nothing to do
        }

        public void callObjectArg(Object arg0) {
            // Nothing to do
        }

        public void callBinaryArgs(byte[] arg0, InputStream arg1) {
            // Nothing to do
        }

    }

}
