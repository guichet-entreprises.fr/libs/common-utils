/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.watcher;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import fr.ge.common.utils.watcher.ProcessEventResult.Result;

@RunWith(MockitoJUnitRunner.class)
public class DirectoryWatcherTest {

    private static String ROOT_DIRECTORY = "target/ge/records";

    private static String ERROR_DIRECTORY = "target/errors";

    private File rootdirectory = null;

    private File errorDirectory = null;

    @Mock
    private IProcessEvent processEvent;

    private DirectoryWatcher watcher;

    @Before
    public void setUp() throws IOException {
        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();

        this.errorDirectory = new File(ERROR_DIRECTORY);
        this.errorDirectory.mkdirs();

        this.watcher = spy(DirectoryWatcher.class) //
                .setInputDirectory(ROOT_DIRECTORY) //
                .setErrorDirectory(ERROR_DIRECTORY) //
                .setProcessEvent(this.processEvent);

        this.watcher.init();
    }

    /**
     * the sha1 calculated equals to existing sha1 in the FS
     *
     * @throws Exception
     */
    @Test
    public void uploadZipTest() throws Exception {

        Mockito.when(this.processEvent.process(any(), any())).thenReturn(new ProcessEventResult(Result.SUCCESS, null));

        final File file = new File(ROOT_DIRECTORY + File.separator + "test.zip");
        file.createNewFile();

        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.sha1"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.md5"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        this.watcher.processZipFile(Paths.get(ROOT_DIRECTORY + File.separator + "test.sha1"));

        verify(this.processEvent).process(any(), any());

        file.delete();

    }

    @Test
    public void registerDirectoryWithNotEmptySubDirectoryTest() throws Exception {
        final File file = new File(ROOT_DIRECTORY + File.separator + "test.zip");
        file.createNewFile();

        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.sha1"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());

        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.md5"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "MD5")).getBytes());

        this.watcher.registerDirectory(Paths.get(ROOT_DIRECTORY));
        assertEquals(this.watcher.getKeys().keySet().size() > 0, true);
        file.delete();
    }

    @Test
    public void registerDirectoryWithEmptySubDirectoryTest() throws Exception {

        final File file = new File(ROOT_DIRECTORY + File.separator + "test.zip");
        file.createNewFile();

        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.sha1"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());

        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.md5"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "MD5")).getBytes());

        this.watcher.registerDirectory(Paths.get(ROOT_DIRECTORY));
        assertEquals(this.watcher.getKeys().keySet().size() > 0, true);
        file.delete();
    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.deleteDirectory(this.errorDirectory);
        FileUtils.deleteDirectory(this.rootdirectory);
    }

}
