/**
 *
 */
package fr.ge.common.utils.bean.search;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * Class SearchQueryOrderTest.
 *
 * @author Christian Cougourdan
 */
public class SearchQueryOrderTest {

    /**
     * Test parse.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParse() throws Exception {
        final SearchQueryOrder query = new SearchQueryOrder("col01:desc");

        assertThat( //
                query, //
                allOf( //
                        hasProperty("column", equalTo("col01")), //
                        hasProperty("order", equalTo("desc")) //
                ) //
        );
    }

    /**
     * Test parse.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParsePath() throws Exception {
        final SearchQueryOrder query = new SearchQueryOrder("col01.test.test:desc");

        assertThat( //
                query, //
                allOf( //
                        hasProperty("column", equalTo("col01")), //
                        hasProperty("path", equalTo("test.test")), //
                        hasProperty("order", equalTo("desc")) //
                ) //
        );
    }

}
