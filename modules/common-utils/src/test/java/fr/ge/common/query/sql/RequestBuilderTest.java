/**
 * 
 */
package fr.ge.common.query.sql;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;

import fr.ge.common.query.sql.Operator;
import fr.ge.common.query.sql.QueryBuilder;

/**
 * Test a request builder.
 * 
 * @author $Author: LABEMONT $
 */
public class RequestBuilderTest {

    /**
     * Simple SQL And Clauses
     */
    @Test
    public void generateAndClauses() {
        QueryBuilder builder = new QueryBuilder();

        // one clause
        builder.addFilter("label", ":", "my label");
        assertThat(builder.getAndClauses()).hasSize(1);
        assertThat(builder.getAndClauses().get(0).getPath()).isEqualTo("label");
        assertThat(builder.getAndClauses().get(0).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(0).getValue()).isEqualTo("my label");

        // two clauses
        builder = new QueryBuilder();

        builder.addFilter("label", ":", "my label");
        builder.addFilter("code", ":", "XXX-42");
        assertThat(builder.getAndClauses()).hasSize(2);
        assertThat(builder.getAndClauses().get(1).getPath()).isEqualTo("code");
        assertThat(builder.getAndClauses().get(1).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(1).getValue()).isEqualTo("XXX-42");
    }

    /**
     * Simple Json And Clauses
     */
    @Test
    public void generateSimpleAndJsonClauses() {
        QueryBuilder builder = new QueryBuilder();

        // one clause
        builder.addFilter("details.transfertChannels.email", ":", "test-referent-cma-paris@yopmail.com");
        assertThat(builder.getFromClauses()).hasSize(0);
        assertThat(builder.getAndClauses()).hasSize(1);
        assertThat(builder.getAndClauses().get(0).getPath()).isEqualTo("details->'transfertChannels'->>'email'");
        assertThat(builder.getAndClauses().get(0).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(0).getValue()).isEqualTo("test-referent-cma-paris@yopmail.com");

        // two clauses
        builder = new QueryBuilder();

        builder.addFilter("details.transfertChannels.email", ":", "test-referent-cma-paris@yopmail.com");
        builder.addFilter("details.parent", ":", "GE/CMA");
        assertThat(builder.getAndClauses()).hasSize(2);
        assertThat(builder.getAndClauses().get(0).getPath()).isEqualTo("details->'transfertChannels'->>'email'");
        assertThat(builder.getAndClauses().get(0).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(0).getValue()).isEqualTo("test-referent-cma-paris@yopmail.com");
        assertThat(builder.getAndClauses().get(1).getPath()).isEqualTo("details->>'parent'");
        assertThat(builder.getAndClauses().get(1).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(1).getValue()).isEqualTo("GE/CMA");
    }

    /**
     * Simple Json And Clauses
     */
    @Test
    public void generateArrayJsonClauses() {
        QueryBuilder builder = new QueryBuilder();

        // one clause
        builder.addFilter("details..users.id", ":", "2018-07-ZTX-ZPM-26");
        assertThat(builder.getFromClauses()).hasSize(1);
        assertThat(builder.getFromClauses().get(0).getDefinition()).isEqualTo("json_array_elements(details->'users')");
        assertThat(builder.getFromClauses().get(0).getAlias()).isEqualTo("users_1");
        assertThat(builder.getAndClauses()).hasSize(1);
        assertThat(builder.getAndClauses().get(0).getPath()).isEqualTo("users_1->>'id'");
        assertThat(builder.getAndClauses().get(0).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(0).getValue()).isEqualTo("2018-07-ZTX-ZPM-26");

        // one clause two arrays
        builder = new QueryBuilder();

        builder.addFilter("details..users..roles", ":", "accountant");
        assertThat(builder.getFromClauses()).hasSize(2);
        assertThat(builder.getFromClauses().get(0).getDefinition()).isEqualTo("json_array_elements(details->'users')");
        assertThat(builder.getFromClauses().get(0).getAlias()).isEqualTo("users_1");
        assertThat(builder.getFromClauses().get(1).getDefinition()).isEqualTo("json_array_elements_text(users_1->'roles')");
        assertThat(builder.getFromClauses().get(1).getAlias()).isEqualTo("roles_2");
        assertThat(builder.getAndClauses()).hasSize(1);
        assertThat(builder.getAndClauses().get(0).getPath()).isEqualTo("roles_2");
        assertThat(builder.getAndClauses().get(0).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(0).getValue()).isEqualTo("accountant");

    }

    /**
     * Mixing all cases
     */
    @Test
    @Ignore("En attente que la réutilisation de FROM soit implémenté")
    public void generateMaxClauses() {
        QueryBuilder builder = new QueryBuilder();

        // 1
        builder.addFilter("details..users.id", ":", "2018-07-ZTX-ZPM-26");
        // 2
        builder.addFilter("details..users..roles", ":", "accountant");
        // 3
        builder.addFilter("details.transfertChannels.email", ":", "test-referent-cma-paris@yopmail.com");

        assertThat(builder.getFromClauses()).hasSize(2);
        assertThat(builder.getAndClauses()).hasSize(3);
        // 1
        assertThat(builder.getFromClauses().get(0).getDefinition()).isEqualTo("json_array_elements(details->'users')");
        assertThat(builder.getFromClauses().get(0).getAlias()).isEqualTo("users_1");
        assertThat(builder.getAndClauses().get(0).getPath()).isEqualTo("users_1->>'id'");
        assertThat(builder.getAndClauses().get(0).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(0).getValue()).isEqualTo("2018-07-ZTX-ZPM-26");
        // 2
        assertThat(builder.getFromClauses().get(1).getDefinition()).isEqualTo("json_array_elements_text(users_1->'roles')");
        assertThat(builder.getFromClauses().get(1).getAlias()).isEqualTo("roles_2");
        assertThat(builder.getAndClauses().get(1).getPath()).isEqualTo("roles_2");
        assertThat(builder.getAndClauses().get(1).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(1).getValue()).isEqualTo("accountant");
        // 3
        assertThat(builder.getAndClauses().get(2).getPath()).isEqualTo("details->'transfertChannels'->>'email'");
        assertThat(builder.getAndClauses().get(2).getOperator().getOperator()).isEqualTo(Operator.EQUAL);
        assertThat(builder.getAndClauses().get(2).getValue()).isEqualTo("test-referent-cma-paris@yopmail.com");

    }
}
