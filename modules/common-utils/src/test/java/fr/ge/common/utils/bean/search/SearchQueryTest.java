/**
 *
 */
package fr.ge.common.utils.bean.search;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

/**
 * Class SearchQueryTest.
 *
 * @author Christian Cougourdan
 */
public class SearchQueryTest {

    /**
     * Test empty.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEmpty() throws Exception {
        final SearchQuery query = new SearchQuery();

        assertThat( //
                query, //
                allOf( //
                        hasProperty("startIndex", equalTo(Long.parseLong(SearchQuery.DEFAULT_START_INDEX))), //
                        hasProperty("maxResults", equalTo(Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS))), //
                        hasProperty("filters", nullValue()), //
                        hasProperty("orders", nullValue()), //
                        hasProperty("searchTerms", nullValue()) //
                ) //
        );
    }

    /**
     * Test with page.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testWithPage() throws Exception {
        final SearchQuery query = new SearchQuery(3, 42);

        assertThat( //
                query, //
                allOf( //
                        hasProperty("startIndex", equalTo(3L)), //
                        hasProperty("maxResults", equalTo(42L)), //
                        hasProperty("filters", nullValue()), //
                        hasProperty("orders", nullValue()), //
                        hasProperty("searchTerms", nullValue()) //
                ) //
        );
    }

    /**
     * Test fluent.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFluent() throws Exception {
        final SearchQuery query = new SearchQuery();

        assertEquals(query, query.setStartIndex(3L));
        assertThat(query, hasProperty("startIndex", equalTo(3L)));

        assertEquals(query, query.setMaxResults(42L));
        assertThat(query, hasProperty("maxResults", equalTo(42L)));

        assertEquals(query, query.setSearchTerms("search terms"));
        assertThat(query, hasProperty("searchTerms", equalTo("search terms")));

        assertEquals(query, query.addFilter("col01", "val01"));
        assertEquals(query, query.addFilter("col02", "<", "val02"));
        assertEquals(query, query.addFilter("col03~val03"));
        assertEquals(query, query.addFilter("col04", "=", Arrays.asList("val04")));
        assertThat( //
                query.getFilters(), //
                contains( //
                        new SearchQueryFilter("col01", "val01"), //
                        new SearchQueryFilter("col02", "<", "val02"), //
                        new SearchQueryFilter("col03~val03"), //
                        new SearchQueryFilter("col04=val04") //
                ) //
        );

        assertThat( //
                query.getFilters().get(3), //
                allOf( //
                        hasProperty("column", equalTo("col04")), //
                        hasProperty("operator", equalTo("=")), //
                        hasProperty("value", equalTo("val04")), //
                        hasProperty("values", equalTo(Arrays.asList("val04"))) //
                ) //
        );

        assertEquals(query, query.addOrder("col03", "desc"));
        assertThat( //
                query.getOrders(), //
                contains( //
                        new SearchQueryOrder("col03", "desc") //
                ) //
        );

        assertEquals(query, query.setFilters(null));
        assertThat(query.getFilters(), nullValue());

        assertEquals(query, query.setFilters(new ArrayList<>()));
        assertThat(query.getFilters(), hasSize(0));

        assertEquals(query, query.setOrders(null));
        assertThat(query.getOrders(), nullValue());

        assertEquals(query, query.setOrders(new ArrayList<>()));
        assertThat(query.getFilters(), hasSize(0));
    }

    /**
     * Test equal simple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEqualSimple() throws Exception {
        final SearchQuery query1 = new SearchQuery(1, 17);
        final SearchQuery query2 = new SearchQuery(1, 17);

        assertEquals(query1, query2);

        query1.addFilter("col01", "val01").addFilter("col02", "<", "val02");
        query2.addFilter("col01", "val01").addFilter("col02", "<", "val02");

        assertEquals(query1, query2);

        query1.addOrder("col03", "desc");
        query2.addOrder("col03", "desc");

        assertEquals(query1, query2);

        query1.addFilter("col04", ">", "val04");
        query2.addFilter("col04", "<", "val04");

        assertNotEquals(query1, query2);
    }

    /**
     * Test to string.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testToString() throws Exception {
        final SearchQuery query = new SearchQuery();

        assertEquals("{ startIndex: 0, maxResults: 20, filters: [  ], orders: [  ], searchTerms: null }", query.toString());

        query.addFilter("col01", "val01").addOrder("col03", "desc");
        assertEquals("{ startIndex: 0, maxResults: 20, filters: [ \"col01:val01\" ], orders: [ \"col03:desc\" ], searchTerms: null }", query.toString());

        query.addFilter("col02", ">", "val02").addOrder("col04", "asc");
        assertEquals("{ startIndex: 0, maxResults: 20, filters: [ \"col01:val01\", \"col02>val02\" ], orders: [ \"col03:desc\", \"col04:asc\" ], searchTerms: null }", query.toString());

        query.setSearchTerms("search\n\"terms\"");
        assertEquals("{ startIndex: 0, maxResults: 20, filters: [ \"col01:val01\", \"col02>val02\" ], orders: [ \"col03:desc\", \"col04:asc\" ], searchTerms: \"search\\n\\\"terms\\\"\" }",
                query.toString());
    }

    /**
     * Test with page.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testAddFilterWithListValues() throws Exception {
        final SearchQuery query = new SearchQuery();
        final SearchQueryFilter filter = new SearchQueryFilter("groupe", "in", Arrays.asList("all", "referent"));
        query.addFilter(filter);

        assertThat( //
                query, //
                allOf( //
                        hasProperty("startIndex", equalTo(Long.parseLong(SearchQuery.DEFAULT_START_INDEX))), //
                        hasProperty("maxResults", equalTo(Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS))), //
                        hasProperty("filters", equalTo(Arrays.asList(filter))), //
                        hasProperty("orders", nullValue()), //
                        hasProperty("searchTerms", nullValue()) //
                ) //
        );
    }
}
