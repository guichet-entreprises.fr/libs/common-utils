/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Test;

/**
 * @author Christian Cougourdan
 */
public class ResourceUtilTest {

    /**
     * Test resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResource() throws Exception {
        final byte[] actual = ResourceUtil.resourceAsBytes("/META-INF/MANIFEST.MF");
        assertThat(actual, notNullValue());
        assertThat(actual.length, greaterThan(0));
    }

    /**
     * Test resource unknown.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResourceUnknown() throws Exception {
        final byte[] actual = ResourceUtil.resourceAsBytes("/unknown.file");
        assertThat(actual, notNullValue());
        assertThat(actual.length, equalTo(0));
    }

    /**
     * Test resource as string.
     */
    @Test
    public void testResourceAsString() throws Exception {
        final String actual = ResourceUtil.resourceAsString("ResourceUtilTest-simple.txt", this.getClass());

        assertThat(actual, equalTo("simple test resource."));
    }

    /**
     * Test build url nominal.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testBuildUrlNominal() throws Exception {
        final String endPoint = "http://localhost/proxy";
        final String actual = ResourceUtil.buildUrl( //
                "${ws.external.url}/v1/Record/{code}/use/{now}", //
                Collections.singletonMap("ws.external.url", endPoint), //
                "2017-05-REC-AAA-42", //
                "2017-05-15T14:30:00Z" //
        );
        assertEquals(endPoint + "/v1/Record/2017-05-REC-AAA-42/use/2017-05-15T14%3A30%3A00Z", actual);
    }

    /**
     * Test build url no configuration.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testBuildUrlNoConfiguration() throws Exception {
        final String actual = ResourceUtil.buildUrl("${ws.external.url}/v1/Record/{code}/use/{now}", null, "A Simple Code", "2017-05-15T14:30:00Z");
        assertEquals("${ws.external.url}/v1/Record/A%20Simple%20Code/use/2017-05-15T14%3A30%3A00Z", actual);
    }

    /**
     * Test build url not enough parameters.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testBuildUrlNotEnoughParameters() throws Exception {
        final String endPoint = "http://localhost/proxy";
        final String actual = ResourceUtil.buildUrl( //
                "${ws.external.url}/v1/Record/{code}/use/{now}", //
                Collections.singletonMap("ws.external.url", endPoint), //
                "2017-05-REC-AAA-42" //
        );
        assertEquals(endPoint + "/v1/Record/2017-05-REC-AAA-42/use/{now}", actual);
    }

    @Test
    public void testFilter() throws Exception {
        final List<String> lst = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");

        final Collection<String> actual = ResourceUtil.filter(lst, null);

        assertThat(actual, containsInAnyOrder(lst.stream().map(Matchers::equalTo).collect(Collectors.toList())));
    }

    @Test
    public void testFilterOne() throws Exception {
        final List<String> lst = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");

        final Collection<String> actual = ResourceUtil.filter(lst, lst.get(3));

        assertThat(actual, containsInAnyOrder(lst.get(3)));
    }

    @Test
    public void testFilterNone() throws Exception {
        final List<String> lst = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");

        final Collection<String> actual = ResourceUtil.filter(lst, "xxx/xxx/xxx.pdf");

        assertThat(actual, hasSize(0));
    }

    @Test
    public void testFilterFilePattern() throws Exception {
        final List<String> lst = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");

        final Collection<String> actual = ResourceUtil.filter(lst, "step02/*.xml");

        assertThat(actual, containsInAnyOrder(lst.get(2), lst.get(5)));
    }

    @Test
    public void testFilterFolderPattern() throws Exception {
        final List<String> lst = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");

        final Collection<String> actual = ResourceUtil.filter(lst, "**/*.pdf");

        assertThat(actual, containsInAnyOrder(lst.get(3), lst.get(4)));
    }

    @Test
    public void testFilterPatterns() throws Exception {
        final List<String> lst = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");

        final Collection<String> actual = ResourceUtil.filter(lst, "step02/*.xml, **/*.pdf");

        assertThat(actual, containsInAnyOrder(lst.get(2), lst.get(3), lst.get(4), lst.get(5)));
    }

}
