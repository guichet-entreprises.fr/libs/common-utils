/**
 *
 */
package fr.ge.common.utils.bean.search;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

/**
 * Class SearchQueryFilterTest.
 *
 * @author Christian Cougourdan
 */
public class SearchQueryFilterTest {

    /**
     * Test parse.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParse() throws Exception {
        this.testParseOperator(":");
        this.testParseOperator("<");
        this.testParseOperator("<=");
        this.testParseOperator(">");
        this.testParseOperator(">=");
    }

    @Test
    public void testNullValues() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("col01", "=", (List<String>) null);
        assertThat(actual.getValue(), nullValue());
    }

    @Test
    public void testEmptyValues() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("col01", "=", Collections.emptyList());
        assertThat(actual.getValue(), nullValue());
    }

    @Test
    public void testEqualsNull() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("col01", "=", Collections.emptyList());
        assertThat(actual.equals(null), equalTo(false));
    }

    @Test
    public void testEqualsOther() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("col01", "=", Collections.emptyList());
        assertThat(actual.equals("other type"), equalTo(false));
    }

    @Test
    public void testInitNull() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter(null);

        assertThat( //
                actual, //
                allOf( //
                        hasProperty("column", nullValue()), //
                        hasProperty("operator", nullValue()), //
                        hasProperty("value", nullValue()) //
                ) //
        );
    }

    @Test
    public void testInitEmpty() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("");

        assertThat( //
                actual, //
                allOf( //
                        hasProperty("column", nullValue()), //
                        hasProperty("operator", nullValue()), //
                        hasProperty("value", nullValue()) //
                ) //
        );
    }

    @Test
    public void testInitBadFormat() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("nothing to parse");

        assertThat( //
                actual, //
                allOf( //
                        hasProperty("column", nullValue()), //
                        hasProperty("operator", nullValue()), //
                        hasProperty("value", nullValue()) //
                ) //
        );
    }

    /**
     * Test parse operator.
     *
     * @param operator
     *            operator
     * @throws Exception
     *             exception
     */
    private void testParseOperator(final String operator) throws Exception {
        final SearchQueryFilter query = new SearchQueryFilter("col01" + operator + "val01");

        assertThat( //
                query, //
                allOf( //
                        hasProperty("column", equalTo("col01")), //
                        hasProperty("operator", equalTo(operator)), //
                        hasProperty("value", equalTo("val01")) //
                ) //
        );
    }

    @Test
    public void testCaseSensitive() throws Exception {
        final SearchQueryFilter query = new SearchQueryFilter("queueCode:AWAIT");

        assertThat( //
                query, //
                allOf( //
                        hasProperty("column", equalTo("queueCode")), //
                        hasProperty("operator", equalTo(":")), //
                        hasProperty("value", equalTo("AWAIT")) //
                ) //
        );
    }

    @Test
    public void testForPath() throws Exception {
        final SearchQueryFilter query = new SearchQueryFilter("queueCode.test.test.test:AWAIT");

        assertThat( //
                query, //
                allOf( //
                        hasProperty("column", equalTo("queueCode")), //
                        hasProperty("path", equalTo("test.test.test")), //
                        hasProperty("operator", equalTo(":")), //
                        hasProperty("value", equalTo("AWAIT")) //
                ) //
        );
    }

    @Test
    public void testListValues() throws Exception {
        final SearchQueryFilter actual = new SearchQueryFilter("groupe", "in", Arrays.asList("all", "referent"));

        assertThat( //
                actual, //
                allOf( //
                        hasProperty("column", equalTo("groupe")), //
                        hasProperty("operator", equalTo("in")), //
                        hasProperty("values", equalTo(Arrays.asList("all", "referent"))) //
                ) //
        );
    }
}
