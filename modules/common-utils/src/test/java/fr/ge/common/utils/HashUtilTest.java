package fr.ge.common.utils;

import static org.junit.Assert.assertEquals;

import javax.xml.bind.DatatypeConverter;

import org.junit.Assert;
import org.junit.Test;

public class HashUtilTest {

    @Test
    public void testHashAsString() throws Exception {
        final String actual = HashUtil.hashAsString("a simple string");
        assertEquals("BDB1C6331DB99AE0CBDFB2EBF9519BAB768585E667F555F09752CE60618A6D4E", actual);
    }

    @Test
    public void testHashAsBytes() throws Exception {
        final byte[] actual = HashUtil.hashAsBytes("a simple string");
        assertEquals("BDB1C6331DB99AE0CBDFB2EBF9519BAB768585E667F555F09752CE60618A6D4E", DatatypeConverter.printHexBinary(actual));
    }

    @Test
    public void testHashWithNullAsStringFromString() throws Exception {
        final String actual = HashUtil.hashAsString((String) null);
        Assert.assertNull(actual);
    }

    @Test
    public void testHashWithNullAsBytesFromString() throws Exception {
        final byte[] actual = HashUtil.hashAsBytes((String) null);
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.length);
    }

    @Test
    public void testHashWithNullAsStringFromBytes() throws Exception {
        final String actual = HashUtil.hashAsString((byte[]) null);
        Assert.assertNull(actual);
    }

    @Test
    public void testHashWithNullAsBytesFromBytes() throws Exception {
        final byte[] actual = HashUtil.hashAsBytes((byte[]) null);
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.length);
    }

}
