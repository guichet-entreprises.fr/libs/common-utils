/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author Christian Cougourdan
 *
 */
public class ZipUtilTest {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    /**
     * Test create.
     */
    @Test
    public void testCreate() {
        final Map<String, byte[]> srcEntries = new HashMap<>();
        srcEntries.put("description.txt", "description file content".getBytes(DEFAULT_CHARSET));
        srcEntries.put("0-context/context.txt", "context file content".getBytes(DEFAULT_CHARSET));
        srcEntries.put("1-data/data.txt", "data file content".getBytes(DEFAULT_CHARSET));

        final byte[] zipContent = ZipUtil.create(srcEntries);
        assertThat(zipContent, notNullValue());

        final List<String> entries = ZipUtil.entries(zipContent);
        assertThat(entries, hasSize(3));

        srcEntries.forEach((key, value) -> assertEquals(new String(value, DEFAULT_CHARSET), new String(ZipUtil.entryAsBytes(key, zipContent), DEFAULT_CHARSET)));
    }

    /**
     * Test create empty.
     */
    @Test
    public void testCreateEmpty() {
        final byte[] zipContent = ZipUtil.create((Map<String, byte[]>) null);
        assertThat(zipContent, notNullValue());

        final List<String> entries = ZipUtil.entries(zipContent);
        assertThat(entries, hasSize(0));
    }

    /**
     * Test create error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testCreateError() throws Exception {
        try {
            ZipUtil.create(Arrays.asList("file01.txt", "file02.txt"), resourceName -> null);
            fail("TechnicalException expected");
        } catch (final TechnicalException ex) {
            assertThat(ex, hasProperty("message", equalTo("No content for file 'file01.txt'")));
        } catch (final Throwable ex) {
            fail("TechnicalException expected");
        }
    }

    /**
     * Test read entry.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testReadEntry() throws Exception {
        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));
        final byte[] entryContent = ZipUtil.entryAsBytes("description.xml", content);

        Assert.assertNotNull(entryContent);
        assertEquals("﻿<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<description>\n    <uid />\n    <title>Cerfa 14004*02</title>\n</description>\n", //
                new String(entryContent, DEFAULT_CHARSET));
    }

    /**
     * Test read unknown entry.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testReadUnknownEntry() throws Exception {
        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));
        final byte[] entryContent = ZipUtil.entryAsBytes("unknown.xml", content);

        Assert.assertNotNull(entryContent);
        assertEquals(0, entryContent.length);
    }

    /**
     * Test read entry from empty.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testReadEntryFromEmpty() throws Exception {
        final byte[] content = new byte[] {};
        final byte[] entryContent = ZipUtil.entryAsBytes("description.xml", content);

        Assert.assertNotNull(entryContent);
        assertEquals(0, entryContent.length);
    }

    /**
     * Test write entry.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testWriteEntry() throws Exception {
        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));
        final byte[] newContent = ZipUtil.entry("description.xml", content, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>".getBytes(DEFAULT_CHARSET));

        Assert.assertNotNull(newContent);

        final byte[] newEntryContent = ZipUtil.entryAsBytes("description.xml", newContent);

        Assert.assertNotNull(newEntryContent);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>", new String(newEntryContent, DEFAULT_CHARSET));
    }

    /**
     * Test write entry not existing.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testWriteEntryNotExisting() throws Exception {
        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));
        final byte[] newContent = ZipUtil.entry("description2.xml", content, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>".getBytes(DEFAULT_CHARSET));

        Assert.assertNotNull(newContent);

        final byte[] newEntryContent = ZipUtil.entryAsBytes("description2.xml", newContent);

        Assert.assertNotNull(newEntryContent);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>", new String(newEntryContent, DEFAULT_CHARSET));
    }

    /**
     * Test write entry error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testWriteEntryError() throws Exception {
        final String errorMessage = "IO Exception ...";

        final ZipEntry entry = mock(ZipEntry.class);
        final byte[] content = "zip content".getBytes(DEFAULT_CHARSET);
        final ZipOutputStream out = mock(ZipOutputStream.class);

        Mockito.doThrow(new IOException(errorMessage)).when(out).putNextEntry(any(ZipEntry.class));

        try {
            ZipUtil.write(entry, content, out);
            fail("TechnicalException expected");
        } catch (final TechnicalException ex) {
            final Throwable cause = ex.getCause();
            assertTrue(cause instanceof IOException);
            assertEquals(errorMessage, cause.getMessage());
        } catch (final Exception ex) {
            fail("TechnicalException expected");
        }
    }

    /**
     * Test write output error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testWriteOutputError() throws Exception {
        final String errorMessage = "IO Exception ...";

        final ZipEntry entry = mock(ZipEntry.class);
        final byte[] content = "zip content".getBytes(DEFAULT_CHARSET);
        final ZipOutputStream out = mock(ZipOutputStream.class);

        doThrow(new IOException(errorMessage)).when(out).write(any(byte[].class));
        doThrow(new IOException(errorMessage)).when(out).write(any(byte[].class), any(int.class), any(int.class));

        try {
            ZipUtil.write(entry, content, out);
            fail("TechnicalException expected");
        } catch (final TechnicalException ex) {
            final Throwable cause = ex.getCause();
            assertTrue(cause instanceof IOException);
            assertEquals(errorMessage, cause.getMessage());
        } catch (final Exception ex) {
            fail("TechnicalException expected");
        }
    }

    /**
     * Test entries.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEntries() throws Exception {
        final List<String> expected = Arrays.asList("0-context/context.css", "0-context/context.xml", "0-context/context.xsd", "1-data/data.xml", "1-data/data.css", "1-data/data.xsd",
                "description.xml");

        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));
        final List<String> entries = ZipUtil.entries(content);

        assertNotNull(entries);
        assertEquals(expected.size(), entries.size());
        expected.forEach(s -> assertTrue(entries.contains(s)));
    }

    @Test
    public void testScan() throws Exception {
        final List<String> expected = Arrays.asList("0-context/context.css", "0-context/context.xml", "0-context/context.xsd", "1-data/data.xml", "1-data/data.css", "1-data/data.xsd",
                "description.xml");
        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));

        final Map<String, FileEntry> actual = new HashMap<>();
        ZipUtil.scan(new ByteArrayInputStream(content), entry -> actual.put(entry.name(), entry));

        assertThat(actual.size(), equalTo(expected.size()));
        expected.forEach(s -> assertTrue(actual.containsKey(s)));
    }

    @Test
    public void testIterator() throws Exception {
        final List<String> expected = Arrays.asList("0-context/context.css", "0-context/context.xml", "0-context/context.xsd", "1-data/data.xml", "1-data/data.css", "1-data/data.xsd",
                "description.xml");
        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-core"));

        final Map<String, FileEntry> fromMapper = new HashMap<>();
        final Map<String, FileEntry> fromIterate = new HashMap<>();

        final Iterator<FileEntry> entries = ZipUtil.iterator(new ByteArrayInputStream(content), entry -> {
            fromMapper.put(entry.name(), entry);
            return entry;
        });

        assertThat(fromMapper.size(), equalTo(0));

        while (entries.hasNext()) {
            final FileEntry entry = entries.next();
            fromIterate.put(entry.name(), entry);
        }

        assertThat(fromMapper.size(), equalTo(expected.size()));
        expected.forEach(s -> assertTrue(fromMapper.containsKey(s)));

        assertThat(fromIterate.size(), equalTo(expected.size()));
        expected.forEach(s -> assertTrue(fromIterate.containsKey(s)));

        try {
            entries.next();
            fail("NoSuchElementException expected");
        } catch (final NoSuchElementException ex) {
            assertThat(ex.getMessage(), nullValue());
        }
    }

    /**
     * An empty Zip returns an empty list.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEntriesEmpty() throws Exception {
        final byte[] content = IOUtils.toByteArray(this.getClass().getResourceAsStream("ZipUtilTest-empty.zip"));
        final List<String> entries = ZipUtil.entries(content);
        assertNotNull(entries);
        assertTrue(entries.isEmpty());
    }

    /**
     * Test entries from null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEntriesFromNull() throws Exception {
        final List<String> entries = ZipUtil.entries(null);
        assertNotNull(entries);
        assertTrue(entries.isEmpty());
    }

    /**
     * Test entries.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEntriesWithSpecialCharacters() throws Exception {
        final List<String> expected = Arrays.asList("0-context/context.css", "0-context/context.xml", "0-context/context.xsd", "1-data/data.xml", "1-data/data.css", "1-data/data.xsd",
                "1-data/pj_àéù.pdf", "description.xml");

        final byte[] content = ZipUtil.create(this.getClass().getResource("ZipUtilTest-special-characters"));
        final List<String> entries = ZipUtil.entries(content);

        assertNotNull(entries);
        assertEquals(expected.size(), entries.size());
        expected.forEach(s -> assertTrue(entries.contains(s)));
    }
}
