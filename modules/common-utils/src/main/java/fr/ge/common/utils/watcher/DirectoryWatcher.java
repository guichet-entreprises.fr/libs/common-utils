/**
 *
 */
package fr.ge.common.utils.watcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class DirectoryWatcher.
 *
 * @author bsadil
 */
public class DirectoryWatcher implements IWatcher {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryWatcher.class);

    /** Buffer size. */
    protected static final int BUFFER_SIZE = 8192;

    /** the ZIP extension of record. */
    protected static final String ZIP_EXTENSION = ".zip";

    /** the log extension of record. */
    protected static final String LOG_EXTENSION = ".log";

    /** THE DIGEST_ALGORITHM_SHA1 extension of record. */
    protected static final String SHA1_EXTENSION = ".sha1";

    /** THE DIGEST_ALGORITHM_SHA1 extension of record. */
    protected static final String MD5_EXTENSION = ".md5";

    /** THE DIGEST_ALGORITHM_SHA1 algorithm of record. */
    protected static final String DIGEST_ALGORITHM_SHA1 = "sha1";

    /** the watcheService. */
    private WatchService watchService;

    /** The keys. */
    private final Map<WatchKey, Path> keys = new HashMap<>();

    /** the inputDirectory to watch. */
    @Value("${nash.watcher.directory.input:#{null}}")
    private String inputDirectory;

    /** the errorDirectory. */
    @Value("${nash.watcher.directory.error:#{null}}")
    private String errorDirectory;

    /** The process event service. */
    @Autowired
    private IProcessEvent processEvent;

    /** The watcher author. **/
    @Value("${nash.watcher.directory.author:#{null}}")
    private String watcherAuthor;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() throws IOException {
        if (this.isActive()) {
            this.watchService = FileSystems.getDefault().newWatchService();
            this.walkAndRegisterDirectories(Paths.get(this.inputDirectory));
        }
    }

    /**
     * Register the given directory with the WatchService; This function will be
     * called by FileVisitor.
     *
     * @param dir
     *            the directory
     * @throws IOException
     *             an {@link IOException}
     */
    public void registerDirectory(final Path dir) throws IOException {

        final File directory = dir.toFile();
        final File[] files = directory.listFiles();
        if (null != files) {
            for (final File file : files) {
                final String basePath = FilenameUtils.removeExtension(file.getPath());
                final Path md5Path = Paths.get(basePath + MD5_EXTENSION);
                final Path sha1Path = Paths.get(basePath + SHA1_EXTENSION);
                final boolean foundChecksumFiles = Files.exists(md5Path) && Files.exists(sha1Path);
                if (foundChecksumFiles) {
                    LOGGER.info("upload already existing Zip with path {}", file.getPath());
                    this.processZipFile(file.toPath());
                }
            }
        }
        final WatchKey key = dir.register(this.watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
        this.keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     *
     * @param rootPath
     *            the root path
     */
    public void walkAndRegisterDirectories(final Path rootPath) {
        // register directory and sub-directories
        try {
            Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                    DirectoryWatcher.this.registerDirectory(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (final IOException ex) {
            LOGGER.warn("Unable to walk '{}'", rootPath, ex);
        }
    }

    /**
     * Read the file and calculate checksum with a specific algorithm.
     *
     * @param file
     *            the file to read
     * @param algorithm
     *            algorithm
     * @return the SHA-1 using byte
     * @throws TechnicalException
     *             the technical exception
     */
    public byte[] createChecksum(final File file, final String algorithm) throws TechnicalException {
        try (InputStream fis = new FileInputStream(file)) {
            final MessageDigest digest = MessageDigest.getInstance(algorithm);
            int n = 0;
            final byte[] buffer = new byte[BUFFER_SIZE];

            while (n != -1) {
                n = fis.read(buffer);
                if (n > 0) {
                    digest.update(buffer, 0, n);
                }
            }
            return digest.digest();
        } catch (final NoSuchAlgorithmException ex) {
            throw new TechnicalException(String.format("Unable to retrieve %s digest algorithm", algorithm), ex);
        } catch (final FileNotFoundException ex) {
            throw new TechnicalException(String.format("File '%s' not found", file), ex);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to read '%s' file", file), ex);
        }
    }

    /**
     * Process all events for keys queued to the watchService.
     *
     * @throws TechnicalException
     *             the technical exception
     */
    @Override
    public boolean processEvents() throws TechnicalException {
        // wait for key to be signaled
        final WatchKey watchKey;
        try {
            watchKey = this.watchService.take();
        } catch (final InterruptedException x) {
            LOGGER.debug("Directory Watch Service has been interrupted");
            return true;
        }

        if (!this.keys.containsKey(watchKey)) {
            LOGGER.error("WatchKey not recognized!!");
            return false;
        }

        this.processWatchKey(watchKey);

        // all directories are inaccessible
        if (this.keys.isEmpty()) {
            LOGGER.error("No more directory to watch, including root one.");
            return false;
        }
        return true;
    }

    /**
     * Process watch key.
     *
     * @param watchKey
     *            the watch key
     */
    void processWatchKey(final WatchKey watchKey) {
        final Path dir = this.keys.get(watchKey);

        for (final WatchEvent<?> watchEvent : watchKey.pollEvents()) {
            final Kind<?> kind = watchEvent.kind();

            // Context for directory entry event is the file name of entry
            final Path name = (Path) watchEvent.context();
            final Path child = dir.resolve(name);

            // print out event
            LOGGER.debug("the event is {} for path {}", kind.name(), child);

            // if directory is created, and watching recursively, then
            // register it and its sub-directories
            if (StandardWatchEventKinds.ENTRY_CREATE.equals(kind)) {
                if (Files.isDirectory(child)) {
                    this.walkAndRegisterDirectories(child);
                } else if (child.toString().endsWith(SHA1_EXTENSION)) {
                    LOGGER.debug("the event is {} for path {}");
                    this.processZipFile(child);
                }
            }

            // reset key and remove from set if directory no longer
            // accessible
            if (!watchKey.reset()) {
                this.keys.remove(watchKey);
            }
        }
    }

    /**
     * upload Zip to markov.
     *
     * @param sha1Path
     *            the path of record
     */
    public void processZipFile(final Path sha1Path) {

        final String basePath = FilenameUtils.removeExtension(sha1Path.toString());

        final Path resourcePath = Paths.get(basePath + ZIP_EXTENSION);
        final Path md5Path = Paths.get(basePath + MD5_EXTENSION);

        try {
            final byte[] expectedSha1AsBytes = this.readHashFile(sha1Path);

            // calculate the SHA1 value of Zip File
            final byte[] actualSha1AsBytes = new HexBinaryAdapter().marshal(this.createChecksum(resourcePath.toFile(), DIGEST_ALGORITHM_SHA1)).getBytes();

            // compare SHA1 file already exist in FS and the
            // DIGEST_ALGORITHM_SHA1
            // value of Zip File
            try (InputStream inputStream = new FileInputStream(resourcePath.toFile())) {
                if (Arrays.equals(expectedSha1AsBytes, actualSha1AsBytes)) {
                    this.processEvent.process(IOUtils.toByteArray(inputStream), this.watcherAuthor);
                    resourcePath.toFile().delete();
                } else {
                    this.moveToErrorDirectory(resourcePath);
                }
            } catch (final FileNotFoundException ex) {
                throw new TechnicalException(String.format("File '%s' not found", resourcePath), ex);
            } catch (final IOException ex) {
                throw new TechnicalException(String.format("Unable to read '%s' file", resourcePath), ex);
            }

            md5Path.toFile().delete();
            sha1Path.toFile().delete();
        } catch (final TechnicalException ex) {
            LOGGER.warn("Upload file '{}' : {}", resourcePath, ex.getMessage(), ex);
        }
    }

    /**
     * Moves to error directory.
     *
     * @param resourcePath
     *            the resource path
     */
    private void moveToErrorDirectory(final Path resourcePath) {
        String uid = null;
        final Path fileName = resourcePath.getFileName();
        if (fileName != null) {
            uid = FilenameUtils.removeExtension(fileName.toString());
        }

        try {
            FileUtils.moveFile(resourcePath.toFile(), new File(this.errorDirectory + File.separator + uid + ZIP_EXTENSION));
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to move file '%s' from '%s' to '%s'", resourcePath.getFileName(), resourcePath.getParent(), this.errorDirectory), ex);
        }

        final List<String> lines = Arrays.asList("The zip File is corrupted,the sha1 calculated is different of existing sha1 in the FS");
        final Path file = Paths.get(this.errorDirectory + File.separator + uid + LOG_EXTENSION);

        try {
            Files.write(file, lines, StandardCharsets.UTF_8);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to write log file '%s'", file), ex);
        }
    }

    /**
     * Reads a hash file.
     *
     * @param path
     *            the path
     * @return the byte[]
     * @throws TechnicalException
     *             the technical exception
     */
    private byte[] readHashFile(final Path path) throws TechnicalException {
        try (InputStream stream = new FileInputStream(path.toString())) {
            // get the value of DIGEST_ALGORITHM_SHA1 file already exist in FS
            return IOUtils.toByteArray(stream);
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to read SHA1 from file " + path, ex);
        }
    }

    /**
     * Sets the input directory.
     *
     * @param inputDirectory
     *            the inputDirectory to set
     * @return the directory watcher
     */
    public DirectoryWatcher setInputDirectory(final String inputDirectory) {
        this.inputDirectory = inputDirectory;
        return this;
    }

    /**
     * Sets the error directory.
     *
     * @param errorDirectory
     *            the errorDirectory to set
     * @return the directory watcher
     */
    public DirectoryWatcher setErrorDirectory(final String errorDirectory) {
        this.errorDirectory = errorDirectory;
        return this;
    }

    /**
     * Gets the keys.
     *
     * @return the keys
     */
    public Map<WatchKey, Path> getKeys() {
        return this.keys;
    }

    /**
     * Mutateur sur l'attribut {@link #processEvent}.
     *
     * @param processEvent
     *            la nouvelle valeur de l'attribut messageProcess
     */
    public DirectoryWatcher setProcessEvent(IProcessEvent processEvent) {
        this.processEvent = processEvent;
        return this;
    }

    /**
     * Mutateur sur l'attribut {@link #watcherAuthor}.
     *
     * @param watcherAuthor
     *            la nouvelle valeur de l'attribut watcherAuthor
     */
    public DirectoryWatcher setWatcherAuthor(final String watcherAuthor) {
        this.watcherAuthor = watcherAuthor;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isActive() {
        return StringUtils.isNotEmpty(this.inputDirectory) && StringUtils.isNotEmpty(this.errorDirectory);
    }
}
