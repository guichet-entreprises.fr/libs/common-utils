/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class ResourceUtil.
 *
 * @author Christian Cougourdan
 */
public final class ResourceUtil {

    private ResourceUtil() {
        // Nothing to do
    }

    /**
     * Resource as bytes.
     *
     * @param resourceName
     *            the resource name
     * @return the byte[]
     */
    public static byte[] resourceAsBytes(final String resourceName) {
        return resourceAsBytes(resourceName, ResourceUtil.class);
    }

    /**
     * Resource as bytes.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @return the byte[]
     */
    public static byte[] resourceAsBytes(final String resourceName, final Class<?> fromClass) {
        try (InputStream in = resourceAsStream(resourceName, fromClass)) {
            if (null == in) {
                return new byte[] {};
            } else {
                return IOUtils.toByteArray(in);
            }
        } catch (final IOException ex) {
            throw new TechnicalException("Error while reading resource " + resourceName, ex);
        }
    }

    /**
     * Resource as stream from classpath, relative to current class.
     *
     * @param resourceName
     *            the resource name
     * @return the input stream
     */
    public static InputStream resourceAsStream(final String resourceName) {
        return resourceAsStream(resourceName, ResourceUtil.class);
    }

    /**
     * Resource as stream from classpath, relative to specified class.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @return the input stream
     */
    public static InputStream resourceAsStream(final String resourceName, final Class<?> fromClass) {
        return fromClass.getResourceAsStream(resourceName);
    }

    /**
     * Resource as string.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @return the string
     */
    public static String resourceAsString(final String resourceName, final Class<?> fromClass) {
        return resourceAsString(resourceName, fromClass, StandardCharsets.UTF_8);
    }

    /**
     * Resource as string.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @param charset
     *            the charset
     * @return the string
     */
    public static String resourceAsString(final String resourceName, final Class<?> fromClass, final Charset charset) {
        return new String(resourceAsBytes(resourceName, fromClass), charset);
    }

    /**
     * Builds an URL from pattern, injecting indexed parameters.
     *
     * @param url
     *            the url
     * @param params
     *            the params
     * @return the string
     */
    public static String buildUrl(final String url, final Map<?, ?> model, final Object... params) {
        final List<Object> tokens = new ArrayList<>(Arrays.asList(params));
        final Optional<Map<?, ?>> modelAsOptional = Optional.ofNullable(model);

        return CoreUtil.searchAndReplace(url, "(\\$)?\\{([^}]+)\\}", m -> {
            if ("$".equals(m.group(1))) {
                final String key = m.group(2);
                return modelAsOptional.map(cfg -> cfg.get(key)) //
                        .map(String.class::cast) //
                        .orElse(null);
            } else {
                try {
                    return tokens.isEmpty() ? null : URLEncoder.encode("" + tokens.remove(0), StandardCharsets.UTF_8.name()).replace("+", "%20");
                } catch (final UnsupportedEncodingException ex) {
                    throw new TechnicalException("Unsupported encoding 'UTF-8'", ex);
                }

            }
        });
    }

    /**
     * Filter string collection with ANT blob pattern.
     *
     * @param lst
     *            items collections to filter
     * @param pattern
     *            filtering pattern
     * @return filtered collection
     */
    public static Collection<String> filter(final Collection<String> lst, final String pattern) {
        if (StringUtils.isEmpty(pattern)) {
            return lst;
        } else {
            return lst.stream().filter(filterAsPredicate(pattern)).collect(Collectors.toList());
        }
    }

    /**
     * Translate ANT blob pattern to predicate.
     *
     * @param pattern
     *            filtering pattern
     * @return predicate filter
     */
    public static Predicate<String> filterAsPredicate(final String pattern) {
        if (StringUtils.isEmpty(pattern)) {
            return str -> true;
        } else {
            final Pattern specialCharsPattern = Pattern.compile("([$()\\[\\]{}.+])");
            final Pattern folderPattern = Pattern.compile("[*]{2}/");
            final Pattern filePattern = Pattern.compile("(?<![*])[*](?![*])");

            return Arrays.stream(pattern.split(",")) //
                    .map(String::trim) //
                    .map(str -> specialCharsPattern.matcher(str).replaceAll("[$1]")) //
                    .map(str -> filePattern.matcher(str).replaceAll("[^/.]*")) //
                    .map(str -> folderPattern.matcher(str).replaceAll("(?:[^/]*/)*")) //
                    .map(str -> '^' + str + '$') //
                    .map(Pattern::compile) //
                    .map(Pattern::asPredicate) //
                    .reduce(null, (result, elm) -> null == result ? elm : result.or(elm));
        }
    }

}
