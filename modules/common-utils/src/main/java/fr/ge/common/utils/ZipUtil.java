/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.IOUtils;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class ZipUtil.
 *
 * @author Christian Cougourdan
 */
public final class ZipUtil {

    /** The Constant STREAM_BUFFER_SIZE. */
    private static final int STREAM_BUFFER_SIZE = 32 * 1024;

    private static final Charset ZIP_ENCODING = StandardCharsets.ISO_8859_1;

    private ZipUtil() {
        // Nothing to do
    }

    /**
     * Creates the.
     *
     * @param filesToAdd
     *            the files to add
     * @return the byte[]
     */
    public static byte[] create(final Map<String, byte[]> filesToAdd) {
        if (null == filesToAdd) {
            return create(null, null);
        } else {
            return create(filesToAdd.keySet(), filesToAdd::get);
        }
    }

    /**
     * Creates a ZIP resource from files.
     *
     * @param filesToAdd
     *            the files to add
     * @param producer
     *            files content producers
     * @return ZIP resource as byte[]
     */
    public static byte[] create(final Collection<String> filesToAdd, final Function<String, byte[]> producer) {
        try (final ByteArrayOutputStream out = new ByteArrayOutputStream(); final ZipOutputStream zipOutputStream = create(out)) {

            for (final String fileToAdd : CoreUtil.coalesce(filesToAdd, Collections.<String>emptyList())) {
                final ZipEntry zipEntry = new ZipEntry(fileToAdd);
                write(zipEntry, producer.apply(fileToAdd), zipOutputStream);
            }

            zipOutputStream.finish();

            return out.toByteArray();
        } catch (final IOException ex) {
            throw new RuntimeException("Error while creating zip content", ex);
        }
    }

    /**
     * Create a resource from URL.
     *
     * @param resourceUrl
     *            the resource url
     * @return the byte[]
     */
    public static byte[] create(final URL resourceUrl) {
        final URI resourceUri = Optional.ofNullable(resourceUrl) //
                .map(url -> {
                    try {
                        return url.toURI();
                    } catch (final URISyntaxException ex) {
                        throw new RuntimeException("", ex);
                    }
                }) //
                .orElseThrow(() -> new TechnicalException(String.format("Resource '%s' not found", resourceUrl)));

        final Path rootPath = Paths.get(resourceUri);
        final File rootFile = rootPath.toFile();
        final List<String> entryNames = buildEntryList("", rootFile);

        final byte[] asBytes = ZipUtil.create(entryNames, (entryName) -> {
            try {
                return Files.readAllBytes(rootPath.resolve(entryName));
            } catch (final IOException ex) {
                throw new TechnicalException(String.format("Unable to read resource '%s'", entryName), ex);
            }
        });

        return asBytes;
    }

    /**
     * Builds the entry list.
     *
     * @param prefix
     *            the prefix
     * @param rootPath
     *            the root path
     * @return the list
     */
    private static List<String> buildEntryList(final String prefix, final File rootPath) {
        final List<String> entryNames = new ArrayList<>();

        for (final File f : Optional.ofNullable(rootPath.listFiles()).orElse(new File[] {})) {
            if (f.isDirectory()) {
                entryNames.addAll(buildEntryList(prefix + f.getName() + "/", f));
            } else {
                entryNames.add(prefix + f.getName());
            }
        }

        return entryNames;
    }

    /**
     * Create a zip output stream using ZIP_ENCODING charset.
     *
     * @param dst
     *            destination output stream
     * @return newly created zip output stream
     */
    private static ZipOutputStream create(final OutputStream dst) {
        return new ZipOutputStream(dst);
    }

    /**
     * Create a zip input stream using ZIP_ENCODING charset.
     *
     * @param src
     *            source input stream
     * @return newly created zip input stream
     */
    private static ZipInputStream open(final InputStream src) {
        return new ZipInputStream(src, ZIP_ENCODING);
    }

    /**
     * Create a zip input stream using ZIP_ENCODING charset.
     *
     * @param src
     *            source bytes array
     * @return newly created zip input stream
     */
    private static ZipInputStream open(final byte[] src) {
        return open(new ByteArrayInputStream(src));
    }

    /**
     * Return the entries' names contained in a Zip content.
     *
     * @param content
     *            byte array of a Zip content
     * @return The entries' names as a List - cannot be null
     */
    public static List<String> entries(final byte[] content) {
        final List<String> entries = new ArrayList<>();

        if (null == content) {
            return entries;
        }

        ZipEntry entry;
        try (ZipInputStream in = open(content)) {
            while (null != (entry = in.getNextEntry())) {
                if (!entry.isDirectory()) {
                    entries.add(entry.getName());
                }
            }
        } catch (final IOException e) {
            throw new TechnicalException("Error while read zip entries", e);
        }

        return entries;
    }

    /**
     * Retrieve an entry by its name in a Zip content.
     *
     * @param entryName
     *            the name of the entry to retrieve as a String
     * @param zipContent
     *            The zip content as a byte array
     * @return The entry as a byte array
     */
    public static byte[] entryAsBytes(final String entryName, final byte[] zipContent) {
        final FileEntry entry = entry(entryName, zipContent);
        if (null == entry) {
            return new byte[] {};
        } else {
            return entry.asBytes();
        }
    }

    /**
     * Retrieve an entry by its name in a Zip content.
     *
     * @param entryName
     *            the name of the entry to retrieve as a String
     * @param zipContent
     *            The zip content as a byte array
     * @return The entry as {@link FileEntry}
     */
    public static FileEntry entry(final String entryName, final byte[] zipContent) {
        try (InputStream resourceStream = new ByteArrayInputStream(zipContent)) {
            return entry(resourceStream, entryName);
        } catch (final IOException ex) {
            throw new TechnicalException("Error while read zip resource", ex);
        } catch (final Exception ex) {
            throw new TechnicalException("Error while read zip resource", ex);
        }
    }

    public static FileEntry entry(final InputStream resourceStream, final String entryName) {
        try (ZipInputStream zipResourceStream = open(resourceStream)) {
            ZipEntry zipEntry;
            while (null != (zipEntry = zipResourceStream.getNextEntry())) {
                if (entryName.equals(zipEntry.getName())) {
                    return new FileEntry(zipEntry.getName()).lastModified(zipEntry.getLastModifiedTime().toMillis()).asBytes(IOUtils.toByteArray(zipResourceStream));
                }
            }
        } catch (final IOException ex) {
            throw new TechnicalException("Error while read zip entry", ex);
        }

        return null;
    }

    /**
     * Update zip content.
     *
     * @param entryName
     *            the name of the entry to update as a String
     * @param content
     *            the zip content as a byte array
     * @param entryContent
     *            the content of the entry to update as a byte array
     * @return the updated zip content as a byte array
     */
    public static byte[] entry(final String entryName, final byte[] content, final byte[] entryContent) {
        return entries(Collections.singletonMap(entryName, entryContent), content);
    }

    public static byte[] entries(final Map<String, byte[]> filesToInsert, final byte[] content) {
        if (MapUtils.isEmpty(filesToInsert)) {
            return content;
        }

        try ( //
                ZipInputStream zipInputStream = open(content); //
                ByteArrayOutputStream out = new ByteArrayOutputStream(); //
                ZipOutputStream zipOutputStream = create(out) //
        ) {
            ZipEntry entry;
            while (null != (entry = zipInputStream.getNextEntry())) {
                final ZipEntry newEntry = new ZipEntry(entry);
                newEntry.setCompressedSize(-1);
                if (!filesToInsert.containsKey(entry.getName())) {
                    write(newEntry, zipInputStream, zipOutputStream);
                }
            }

            for (final Map.Entry<String, byte[]> entryToInsert : filesToInsert.entrySet()) {
                if (null != entryToInsert.getValue()) {
                    final ZipEntry newZipEntry = new ZipEntry(entryToInsert.getKey());
                    write(newZipEntry, entryToInsert.getValue(), zipOutputStream);
                }
            }

            zipOutputStream.finish();

            return out.toByteArray();
        } catch (final IOException ex) {
            throw new TechnicalException("Error while read zip entry", ex);
        }
    }

    public static void scan(final InputStream resourceStream, final Consumer<FileEntry> fn) {
        ZipEntry entry;
        try (ZipInputStream in = open(resourceStream)) {
            while (null != (entry = in.getNextEntry())) {
                if (!entry.isDirectory()) {
                    fn.accept(new FileEntry(entry.getName()).lastModified(entry.getLastModifiedTime().toInstant()).setContent(in));
                }
            }
        } catch (final IOException e) {
            throw new TechnicalException("Error while read zip entries", e);
        }
    }

    public static Iterator<FileEntry> iterator(final InputStream resourceStream, final Function<FileEntry, FileEntry> fn) {
        final Function<FileEntry, FileEntry> map = null == fn ? entry -> entry : fn;
        final ZipInputStream in = open(resourceStream);

        return new Iterator<FileEntry>() {

            private boolean closed = false;

            private FileEntry nextEntry;

            @Override
            public boolean hasNext() {
                if (null != this.nextEntry) {
                    return true;
                }

                if (!this.closed) {
                    ZipEntry zipEntry;
                    try {
                        while (null != (zipEntry = in.getNextEntry())) {
                            if (zipEntry.isDirectory()) {
                                zipEntry = null;
                            } else {
                                this.nextEntry = new FileEntry(zipEntry.getName()).lastModified(zipEntry.getLastModifiedTime().toInstant()).setContent(in);
                                return true;
                            }
                        }

                        in.close();
                        this.closed = true;
                    } catch (final IOException ex) {
                        throw new TechnicalException("Error while read zip entries", ex);
                    }
                }

                return false;
            }

            @Override
            public FileEntry next() {
                if (this.hasNext()) {
                    final FileEntry entry = map.apply(this.nextEntry);
                    this.nextEntry = null;
                    return entry;
                } else {
                    throw new NoSuchElementException();
                }
            }
        };
    }

    /**
     * Write.
     *
     * @param entry
     *            zip entry
     * @param content
     *            entry content as byte array
     * @param out
     *            target output stream
     */
    static void write(final ZipEntry entry, final byte[] content, final ZipOutputStream out) {
        if (null == content) {
            throw new TechnicalException("No content for file '" + entry.getName() + "'");
        }

        entry.setSize(content.length);

        try (ByteArrayInputStream in = new ByteArrayInputStream(content)) {
            write(entry, in, out);
        } catch (final IOException ex) {
            throw new TechnicalException("Error while reading source content", ex);
        }
    }

    /**
     * Write.
     *
     * @param entry
     *            zip entry
     * @param in
     *            entry content as input stream
     * @param out
     *            target output stream
     */
    static void write(final ZipEntry entry, final InputStream in, final ZipOutputStream out) {
        try {
            out.putNextEntry(entry);
        } catch (final IOException ex) {
            throw new TechnicalException("Error while setting next zip entry", ex);
        }

        int len;
        final byte[] buffer = new byte[STREAM_BUFFER_SIZE];
        try {
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } catch (final IOException ex) {
            throw new TechnicalException("Error while copying from one stream to another", ex);
        }
    }

}
