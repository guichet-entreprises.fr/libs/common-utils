/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.query.sql.reader;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.query.sql.Operator;

/**
 * Create an Operator from a input string.
 *
 * @author $Author: LABEMONT $
 */
public class OperatorReader {

    private static final Map<String, Operator> TRANSCODING;

    static {
        final Map<String, Operator> m = new HashMap<>();

        m.put(":", Operator.EQUAL);
        m.put("&lt;", Operator.LESS);
        m.put("&gt;", Operator.GREATER);
        m.put("~", Operator.LIKE_ALL);
        m.put("~:", Operator.LIKE_BEFORE);
        m.put(":~", Operator.LIKE_AFTER);

        TRANSCODING = Collections.unmodifiableMap(m);
    }

    /**
     * Read the operator.
     *
     * @param operator
     *            the input operator
     * @return {@link Operator#UNKNOWN} if not found, {@link Operator} otherwise
     */
    public Operator read(final String operator) {
        if (StringUtils.isBlank(operator)) {
            return Operator.UNKNOWN;
        } else {
            return Optional.ofNullable(TRANSCODING.get(operator)).orElse(Operator.UNKNOWN);
        }
    }
}