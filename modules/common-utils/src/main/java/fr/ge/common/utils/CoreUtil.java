/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CoreUtil.
 *
 * @author Christian Cougourdan
 */
public final class CoreUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger("monitoring");

    private CoreUtil() {
        // Nothing to do
    }

    /**
     * Return first non null specified arguments.
     *
     * @param <T>
     *     the generic type
     * @param values
     *     the values
     * @return first not null value
     */
    @SafeVarargs
    public static <T> T coalesce(final T... values) {
        return Arrays.stream(values).filter(v -> null != v).findFirst().orElse(null);
    }

    /**
     * Return first non null returned value from specified suppliers.
     *
     * @param <T>
     *     the generic type
     * @param suppliers
     *     the suppliers
     * @return first not null value
     */
    @SafeVarargs
    public static <T> T coalesce(final Supplier<T>... suppliers) {
        return Arrays.stream(suppliers).map(Supplier::get).filter(value -> null != value).findFirst().orElse(null);
    }

    /**
     * Cast any object to expected returned type.
     *
     * @param <T>
     *     the generic type
     * @param o
     *     object to cast
     * @return casted object
     */
    @SuppressWarnings("unchecked")
    public static <T> T cast(final Object o) {
        return (T) o;
    }

    /**
     * Search for specific throwable in thrown stack.
     *
     * @param <T>
     *     type générique
     * @param root
     *     root throwable
     * @param expectedClass
     *     expected throwable class
     * @return found throwable
     */
    @SuppressWarnings("unchecked")
    public static <T> T findException(final Throwable root, final Class<T> expectedClass) {
        Throwable found = root;

        for (; null != found && !expectedClass.isInstance(found); found = found.getCause()) {
            // Nothing to do
        }

        return (T) found;
    }

    /**
     * Search and replace.
     *
     * @param source
     *     the source
     * @param regex
     *     the regex
     * @param resolver
     *     the resolver
     * @return the string
     */
    public static String searchAndReplace(final String source, final String regex, final Function<Matcher, Object> resolver) {
        return searchAndReplace(source, Pattern.compile(regex), resolver);
    }

    /**
     * Search and replace.
     *
     * @param source
     *     the source
     * @param pattern
     *     the pattern
     * @param resolver
     *     the resolver
     * @return the string
     */
    public static String searchAndReplace(final String source, final Pattern pattern, final Function<Matcher, Object> resolver) {
        final Matcher matcher = pattern.matcher(source);
        final StringBuffer buffer = new StringBuffer();

        while (matcher.find()) {
            final Object replacement = resolver.apply(matcher);
            if (null == replacement) {
                matcher.appendReplacement(buffer, "$0");
            } else {
//                matcher.appendReplacement(buffer, replacement.toString().replaceAll("[$]", "\\\\\\$"));
                matcher.appendReplacement(buffer, replacement.toString());
            }
        }

        matcher.appendTail(buffer);

        return buffer.toString();
    }

    /**
     * Stringify a string, ie add quote arround value and escape inner quotes and
     * line-feed.
     *
     * @param src
     *     source string
     * @return stringify string
     */
    public static String stringify(final String src) {
        return '"' + src.replaceAll("\n", "\\\\n") //
                .replaceAll("\"", "\\\\\"") //
                + '"';
    }

    /**
     * Execute specified supplier and log (debug) its execution duration in
     * milliseconds.
     *
     * @param <R>
     *     supplier result type
     * @param msg
     *     log prefix message
     * @param fn
     *     supplier to execute
     * @return supplier result
     */
    public static <R> R time(final String msg, final Supplier<R> fn) {
        final long time = System.currentTimeMillis();
        try {
            return fn.get();
        } finally {
            LOGGER.debug("{} : {} ms", msg, System.currentTimeMillis() - time);
        }
    }

    public static void time(final String msg, final Runnable fn) {
        final long time = System.currentTimeMillis();
        try {
            fn.run();
        } finally {
            LOGGER.debug("{} : {} ms", msg, System.currentTimeMillis() - time);
        }
    }

}
