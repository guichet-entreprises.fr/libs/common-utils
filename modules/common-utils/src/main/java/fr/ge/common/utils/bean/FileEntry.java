/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FileEntry.
 *
 * @author Christian Cougourdan
 */
public class FileEntry implements AutoCloseable {

    /** The Constant DEFAULT_TYPE. */
    public static final String DEFAULT_TYPE = "application/octet-stream";

    private static Logger LOGGER = LoggerFactory.getLogger(FileEntry.class);

    /** The _name. */
    private final String _name;

    /** The _type. */
    private final String _type;

    /** The _last modified. */
    private Instant _lastModified;

    /** The _bytes content. */
    private byte[] contentAsBytes;

    private InputStream contentAsStream;

    /**
     * Instantiates a new file entry.
     *
     * @param name
     *            the name
     */
    public FileEntry(final String name) {
        this(name, DEFAULT_TYPE);
    }

    /**
     * Instantiates a new file entry.
     *
     * @param name
     *            the name
     * @param type
     *            the type
     */
    public FileEntry(final String name, final String type) {
        this._name = name;
        this._type = type;
    }

    /**
     * Last modified.
     *
     * @param timeInMillis
     *            the time in millis
     * @return the file entry
     */
    public FileEntry lastModified(final long timeInMillis) {
        this._lastModified = Instant.ofEpochMilli(timeInMillis);
        return this;
    }

    /**
     * Last modified.
     *
     * @param instant
     *
     *            the instant
     * @return the file entry
     */
    public FileEntry lastModified(final Calendar instant) {
        this._lastModified = Optional.ofNullable(instant).map(Calendar::getTimeInMillis).map(Instant::ofEpochMilli).orElse(null);
        return this;
    }

    /**
     * Last modified.
     *
     * @param instant
     *
     *            the instant
     * @return the file entry
     */
    public FileEntry lastModified(final Date instant) {
        this._lastModified = Optional.ofNullable(instant).map(Date::getTime).map(Instant::ofEpochMilli).orElse(null);
        return this;
    }

    public FileEntry lastModified(final Temporal instant) {
        this._lastModified = Optional.ofNullable(instant).map(Instant::from).orElse(null);
        return this;
    }

    /**
     * As bytes.
     *
     * @param bytesContent
     *            the bytes content
     * @return the file entry
     */
    public FileEntry asBytes(final byte[] bytesContent) {
        this.contentAsBytes = Optional.ofNullable(bytesContent).map(bytes -> bytes.clone()).orElse(null);
        this.contentAsStream = null;
        return this;
    }

    public FileEntry setContent(final InputStream resourceStream) {
        this.contentAsBytes = null;
        this.contentAsStream = resourceStream;
        return this;
    }

    /**
     * Name.
     *
     * @return the string
     */
    public String name() {
        return this._name;
    }

    /**
     * Type.
     *
     * @return the string
     */
    public String type() {
        return this._type;
    }

    /**
     * Last modified.
     *
     * @return the date time
     */
    public Instant lastModified() {
        return this._lastModified;
    }

    /**
     * As bytes.
     *
     * @return the byte[]
     */
    public byte[] asBytes() {
        if (null == this.contentAsBytes && null != this.contentAsStream) {
            try {
                this.contentAsBytes = IOUtils.toByteArray(this.contentAsStream);
            } catch (final IOException ex) {
                LOGGER.warn("Unable to retrieve [{}] resource content", this.name(), ex);
            }
        }

        return Optional.ofNullable(this.contentAsBytes).map(bytes -> bytes.clone()).orElse(null);
    }

    public InputStream getContentAsStream() {
        if (null == this.contentAsStream && null != this.contentAsBytes) {
            this.contentAsStream = Optional.ofNullable(this.contentAsBytes).map(ByteArrayInputStream::new).orElse(null);
        }

        return this.contentAsStream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this._name;
    }

    @Override
    public void close() throws IOException {
        if (null != this.contentAsStream) {
            this.contentAsStream.close();
            this.contentAsStream = null;
        }
    }

}
