package fr.ge.common.query.sql;

/**
 * List of all operators.
 * 
 * @author $Author: LABEMONT $
 */
public enum Operator {
    /** Equal. */
    EQUAL,
    /** Less or smaller. */
    LESS,
    /** Less or equals. */
    LESS_OR_EQUAL,
    /** Greater or bigger. */
    GREATER,
    /** Greater or equal. */
    GREATER_OR_EQUAL,
    /** Contains. */
    CONTAINS,
    /** The variable part is before the searched value. */
    LIKE_BEFORE,
    /** The variable part is after the searched value. */
    LIKE_AFTER,
    /** The variable part is before and after the searched value. */
    LIKE_ALL,
    /** Unknown operator. */
    UNKNOWN;
}
