/**
 * 
 */
package fr.ge.common.query.sql;

/**
 * FROM element with a Json origin. Used while computing all the Element to
 * check if the FROM Json element has the right type : text or json.
 * 
 * @author $Author: LABEMONT $
 */
public class FromElementJson extends FromElement {

    /**
     * A distinct type for each json origin data.
     */
    public enum JsonOriginType {
        /** The origin is a text array. */
        TEXT,
        /** The origin is a Json array. */
        JSON;
    }

    /**
     * Type of the data origin : text or json.<br/>
     * Default is {@link JsonOriginType#TEXT}.
     */
    private JsonOriginType originType = JsonOriginType.TEXT;

    /**
     * Constructor. Add json definition of the origin.
     *
     * @param definition
     *            {@link FromElement#definition}
     * @param alias
     *            {@link FromElement#alias}, may be null or empty
     * @param originType
     *            {@link FromElementJson#originType}
     */
    public FromElementJson(String definition, String alias, JsonOriginType originType) {
        super(definition, alias);
        this.originType = originType;

        switch (originType) {
        case JSON:
            this.definition = String.format("json_array_elements(%s)", definition);
            break;
        case TEXT:
            this.definition = String.format("json_array_elements_text(%s)", definition);
            break;
        }

    }

    /**
     * Accesseur sur l'attribut {@link #originType}.
     *
     * @return JsonOriginType originType
     */
    public JsonOriginType getOriginType() {
        return originType;
    }

}
