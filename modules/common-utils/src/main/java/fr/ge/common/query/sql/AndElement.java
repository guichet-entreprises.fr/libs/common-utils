/**
 * 
 */
package fr.ge.common.query.sql;

/**
 * A Conjonction.
 * 
 * @author $Author: LABEMONT $
 */
public class AndElement {

    /** the left side of the conjonction. */
    private String path;
    /** the right side of the conjonction. */
    private String value;
    /** the operator of the conjonction. */
    private OperatorElement operator;

    /**
     * Constructeur de la classe.
     *
     * @param path
     *            {@link #path}
     * @param value
     *            {@link #value}
     * @param operator
     *            {@link #operator}
     */
    public AndElement(String path, OperatorElement operator, String value) {
        this.path = path;
        this.operator = operator;
        this.value = value;
    }

    /**
     * Accesseur sur l'attribut {@link #path}.
     *
     * @return String path
     */
    public String getPath() {
        return path;
    }

    /**
     * Accesseur sur l'attribut {@link #operator}.
     *
     * @return OperatorElement operator
     */
    public OperatorElement getOperator() {
        return operator;
    }

    /**
     * Accesseur sur l'attribut {@link #value}.
     *
     * @return String value
     */
    public String getValue() {
        return value;
    }

}
