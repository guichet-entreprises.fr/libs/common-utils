/**
 * 
 */
package fr.ge.common.query.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.query.sql.FromElementJson.JsonOriginType;

/**
 * Enable to do a dynamic request on a JSON field.<br/>
 * Not thread safe.
 * 
 * @author $Author: LABEMONT $
 */
public class QueryBuilder {

    /** Simple field accessor. */
    public static final String FIELD_ACCESSOR = ".";
    /** Array accessor. */
    public static final String ARRAY_ACCESSOR = "..";

    /** Unique alias index. */
    private int aliasIndex = 0;

    private Map<FromElement, List<AndElement>> links = new HashMap<>();

    /** List of the AND clauses. */
    private List<AndElement> andClauses = new ArrayList<>();

    /** List of the FROM clauses. */
    private List<FromElement> fromClauses = new ArrayList<>();

    /**
     * Add a filter for the query.
     * 
     * @param filterTarget
     *            the right element for the filter
     * @param filterOperator
     *            the operator
     * @param filterValue
     *            the value
     */
    public void addFilter(String filterTarget, String filterOperator, String filterValue) {
        addFilter(filterTarget, OperatorElement.read(filterOperator), filterValue);
    }

    /**
     * Add a filter for the query.
     * 
     * @param filterTarget
     *            the right element for the filter
     * @param filterOperator
     *            the operator
     * @param filterValue
     *            the value
     */
    public void addFilter(String filterTarget, Operator operator, String filterValue) {
        addFilter(filterTarget, new OperatorElement(operator), filterValue);
    }

    /**
     * Add a filter for the query.
     * 
     * @param filterTarget
     *            the right element for the filter
     * @param filterOperator
     *            the operator
     * @param filterValue
     *            the value
     */
    public void addFilter(String filterTarget, OperatorElement filterOperator, String filterValue) {
        if (StringUtils.contains(filterTarget, ARRAY_ACCESSOR)) {
            // accessing a json array
            addJsonArrayFilter(filterTarget, filterOperator, filterValue);
        } else {
            if (StringUtils.contains(filterTarget, FIELD_ACCESSOR)) {
                // simple json field access
                addJsonFilter(filterTarget, filterOperator, filterValue);
            } else {
                // simple field access
                addAndClause(filterTarget, filterOperator, filterValue);
            }
        }
    }

    /**
     * Accesseur sur l'attribut {@link #andClauses}.
     *
     * @return List<AndElement> andClauses
     */
    public List<AndElement> getAndClauses() {
        return andClauses;
    }

    /**
     * Accesseur sur l'attribut {@link #fromClauses}.
     *
     * @return List<FromElement> fromClauses
     */
    public List<FromElement> getFromClauses() {
        return fromClauses;
    }

    /**
     * Add a json field conjonction (AND).
     * 
     * @param filterTarget
     *            the field
     * @param filterValue
     *            the value
     * @param filterOperator
     *            the operator
     */
    private AndElement addJsonFilter(String filterTarget, OperatorElement filterOperator, String filterValue) {
        String[] fields = StringUtils.split(filterTarget, FIELD_ACCESSOR);
        // end of json processing, redirect to classic process
        if (fields.length == 1) {
            return addAndClause(filterTarget, filterOperator, filterValue);
        }

        // processing simple json target
        String jsonPath = fields[0];
        // first element is the column name
        for (int i = 1; i < fields.length - 1; i++) {
            // next elements are the json fields names, with ''
            jsonPath = String.format("%s->%s", jsonPath, StringUtils.wrap(fields[i], '\''));
        }
        // the last element is read as text
        jsonPath = String.format("%s->>%s", jsonPath, StringUtils.wrap(fields[fields.length - 1], '\''));
        // create the And clause
        AndElement andElement = addAndClause(jsonPath, filterOperator, filterValue);
        return andElement;
    }

    /**
     * 
     * @param filterTarget
     *            the field
     * @param filterValue
     *            the value
     * @param filterOperator
     *            the operator
     * @return the FROM element, null if not created as expected, may have
     *         created a simple json conjonction
     */
    private FromElementJson addJsonArrayFilter(String filterTarget, OperatorElement filterOperator, String filterValue) {
        // no arrays here
        if (filterTarget.contains(ARRAY_ACCESSOR) == false) {
            addJsonFilter(filterTarget, filterOperator, filterValue);
            return null;
        }
        // extract json path
        String[] jsonPaths = StringUtils.splitByWholeSeparator(filterTarget, ARRAY_ACCESSOR);
        // extract the prefix
        String[] prefixFields = StringUtils.split(jsonPaths[0], FIELD_ACCESSOR);
        String arrayPath = prefixFields[0];
        for (int i = 1; i < prefixFields.length; i++) {
            // next elements are the json fields names, with ''
            arrayPath = String.format("%s->%s", arrayPath, StringUtils.wrap(prefixFields[i], '\''));
        }

        // find the name of the array
        String[] fields = StringUtils.split(jsonPaths[1], FIELD_ACCESSOR);
        // first element is the column name
        arrayPath = String.format("%s->%s", arrayPath, StringUtils.wrap(fields[0], '\''));
        boolean hasMoreElement = jsonPaths.length > 2 || fields.length > 1;
        FromElementJson fromElement = addFromClause(arrayPath, String.format("%s_%d", fields[0], ++this.aliasIndex), hasMoreElement ? JsonOriginType.JSON : JsonOriginType.TEXT);

        // compute new path with alias name
        String newPath = filterTarget.substring(jsonPaths[0].length() + ARRAY_ACCESSOR.length() + fields[0].length());
        newPath = String.format("%s%s", fromElement.getAlias(), newPath);

        // recall
        return addJsonArrayFilter(newPath, filterOperator, filterValue);
    }

    /**
     * Associate a AndElement and its original FROM Clause. A FROM clause may
     * have several AndElement.
     * 
     * @param fromElement
     *            the FROM clause alias
     * @param andElement
     *            the conjonction element
     */
    private void link(FromElement fromElement, AndElement andElement) {
        List<AndElement> linkedElement = this.links.get(fromElement);
        if (linkedElement == null) {
            linkedElement = new ArrayList<>();
        }
        linkedElement.add(andElement);
        this.links.put(fromElement, linkedElement);
    }

    /**
     * Add a AND clause link.
     * 
     * @param path
     *            the path
     * @param value
     *            the value
     * @param operator
     *            the operator
     * @return
     */
    private AndElement addAndClause(String path, OperatorElement operator, String value) {
        // create the And clause
        AndElement andElement = new AndElement(path, operator, value);
        this.andClauses.add(andElement);
        return andElement;
    }

    /**
     * Add a FROM clause. Specifically for JSON alias.<br/>
     * Produce :
     * <code>json_array_elements_text(details->'codeDepartement') as deps</code>
     * 
     * @param string
     *            the origin
     * @return the name of the alias
     */
    private FromElementJson addFromClause(String jsonPath, String alias, JsonOriginType originType) {
        // find out if the target or the alias is already used
        // TODO check pré-existance
        // create FROM clause
        FromElementJson fromElement = new FromElementJson(jsonPath, alias, originType);
        this.fromClauses.add(fromElement);
        return fromElement;
    }
}
