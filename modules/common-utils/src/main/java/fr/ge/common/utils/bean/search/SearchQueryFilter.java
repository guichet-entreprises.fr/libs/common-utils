/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.bean.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * The Class SearchQueryOrder.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "filter", namespace = "http://v1.ws.cube.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public final class SearchQueryFilter {

    /** The Constant DEFAULT_OPERATOR. */
    private static final String DEFAULT_OPERATOR = ":";

    /** The Constant PATTERN. */
    private static final Pattern PATTERN = Pattern.compile("([a-zA-Z0-9_.\\-\\/]*)([:<>=~!|]+)(.*)", Pattern.CASE_INSENSITIVE);

    /** Column group index. */
    private static final int COLUMN_GROUP_INDEX = 1;

    /** Operator group index. */
    private static final int OPERATOR_GROUP_INDEX = 2;

    /** Value group index. */
    private static final int VALUE_GROUP_INDEX = 3;

    /** The column. */
    private String column;

    /** The operator. */
    private String operator;

    /** the path for json value */
    private String path;

    /** The values. */
    private Collection<String> values;

    /**
     * Instantiates a new search query filter.
     */
    public SearchQueryFilter() {
        // Nothing to do
    }

    /**
     * Instantiates a new search query filter.
     *
     * @param filter
     *            the filter
     */
    public SearchQueryFilter(final String filter) {
        if (null != filter) {
            final Matcher m = PATTERN.matcher(filter);
            if (m.matches()) {
                if (StringUtils.contains(m.group(COLUMN_GROUP_INDEX), ".")) {
                    this.setColumn(StringUtils.substringBefore(m.group(COLUMN_GROUP_INDEX), "."));
                    this.setPath(StringUtils.substringAfter(m.group(COLUMN_GROUP_INDEX), "."));
                } else {
                    this.setColumn(m.group(COLUMN_GROUP_INDEX));
                }
                this.setOperator(m.group(OPERATOR_GROUP_INDEX));
                final String value = m.group(VALUE_GROUP_INDEX);

                this.values = Stream.of(value.split(",")).map(elem -> new String(elem).trim()).collect(Collectors.toList());
            }
        }
    }

    /**
     * Instantiates a new search query order.
     *
     * @param column
     *            the column
     * @param value
     *            the value
     */
    public SearchQueryFilter(final String column, final String value) {
        this(column, null, value);
    }

    /**
     * Instantiates a new search query filter.
     *
     * @param column
     *            the column
     * @param operator
     *            the operator
     * @param values
     *            the values
     */
    public SearchQueryFilter(final String column, final String operator, final Collection<String> values) {
        this.column = column;
        this.operator = null == operator ? DEFAULT_OPERATOR : operator;
        this.values = values;
    }

    /**
     * Instantiates a new search query filter.
     *
     * @param column
     *            the column
     * @param operator
     *            the operator
     * @param value
     *            the value@
     */
    public SearchQueryFilter(final String column, final String operator, final String value) {
        this.column = column;
        this.operator = null == operator ? DEFAULT_OPERATOR : operator;
        this.values = new ArrayList<String>();
        this.values.add(value);
    }

    /**
     * Gets the column.
     *
     * @return the column
     */
    public String getColumn() {
        return this.column;
    }

    /**
     * Sets the column.
     *
     * @param column
     *            the new column
     * @return the search query filter
     */
    public SearchQueryFilter setColumn(final String column) {
        this.column = column;
        return this;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * @param path
     *            the path to set
     */
    public SearchQueryFilter setPath(final String path) {
        this.path = path;
        return this;
    }

    /**
     * Gets the operator.
     *
     * @return the operator
     */
    public String getOperator() {
        return this.operator;
    }

    /**
     * Sets the operator.
     *
     * @param operator
     *            the new operator
     * @return the search query filter
     */
    public SearchQueryFilter setOperator(final String operator) {
        this.operator = operator;
        return this;
    }

    /**
     * Gets the value.
     *
     * @return the first value
     */
    public String getValue() {
        if (null == this.values || this.values.isEmpty()) {
            return null;
        }
        if (this.values.size() == 1) {
            return this.values.iterator().next();
        } else {
            return this.values.toString();
        }

    }

    /**
     * Gets the value in a list.
     *
     * @return the value
     */
    public Collection<String> getValues() {
        return this.values;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     * @return the search query filter
     */
    public SearchQueryFilter setValue(final String value) {
        this.values = new ArrayList<String>();
        this.values.add(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format( //
                "%s%s%s", //
                (null != this.path) ? String.join(".", this.column, this.path) : this.column, //
                this.operator, //
                Optional.ofNullable(this.values) //
                        .map(Collection::stream) //
                        .map(stream -> stream.collect(Collectors.joining(", "))) //
                        .orElse("null") //
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof SearchQueryFilter)) {
            return false;
        }

        final SearchQueryFilter other = (SearchQueryFilter) obj;

        return new EqualsBuilder() //
                .append(this.column, other.column) //
                .append(this.operator, other.operator) //
                .append(this.values, other.values) //
                .isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.column).append(this.operator).append(this.values).toHashCode();
    }

}
