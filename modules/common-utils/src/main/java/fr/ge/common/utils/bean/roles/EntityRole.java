package fr.ge.common.utils.bean.roles;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class EntityRole {

    private final String entity;

    private final String role;

    public EntityRole(final String entityRole) {
        final String[] tokens = entityRole.split(":", 2);

        this.entity = tokens[0];
        if (tokens.length > 1) {
            this.role = tokens[1];
        } else {
            this.role = "*";
        }
    }

    public EntityRole(final String entity, final String role) {
        this.entity = entity;
        this.role = role;
    }

    /**
     * @return the entity
     */
    public String getEntity() {
        return this.entity;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return this.role;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
                .append(this.entity) //
                .append(this.role) //
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof EntityRole)) {
            return false;
        }
        final EntityRole other = (EntityRole) obj;

        return new EqualsBuilder() //
                .append(other.entity, this.entity) //
                .append(other.role, this.role) //
                .isEquals();
    }

    @Override
    public String toString() {
        return String.format("%s:%s", this.entity, this.role);
    }

}
