/**
 * 
 */
package fr.ge.common.query.sql;

/**
 * Definition of the data origin.<br/>
 * <ul>
 * <li>a table name, ex: <code>repository repo</code></li>
 * <li>a json definition, ex:
 * <code>json_array_elements(details->'users') users</code></li>
 * </ul>
 * 
 * @author $Author: LABEMONT $
 */
public class FromElement {

    /**
     * The real name of the data :
     * <ul>
     * <li>a table name, ex: repository.</li>
     * <li>a json definition, ex: json_array_elements(details->'users').</li>
     * </ul>
     */
    protected String definition;

    /** The alias of the FROM clause. */
    private String alias;

    /**
     * Constructeur de la classe.
     *
     * @param definition
     *            {@link #definition}
     * @param alias
     *            {@link #alias}, may be null or empty
     */
    public FromElement(String definition, String alias) {
        super();
        this.alias = alias;
        this.definition = definition;
    }

    /**
     * Accesseur sur l'attribut {@link #alias}.
     *
     * @return String alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Accesseur sur l'attribut {@link #definition}.
     *
     * @return String definition
     */
    public String getDefinition() {
        return definition;
    }

}
