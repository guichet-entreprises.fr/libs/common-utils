/**
 * 
 */
package fr.ge.common.query.sql;

import fr.ge.common.query.sql.reader.OperatorReader;

/**
 * Define an operator element, created from an input dialect.
 * 
 * @author $Author: LABEMONT $
 */
public class OperatorElement {

    /** Default Operator Reader. */
    protected static OperatorReader operatorReader = new OperatorReader();

    /** The recognized operator. */
    private Operator operator;

    /**
     * Constructeur de la classe.
     *
     * @param operator
     *            the operator
     */
    public OperatorElement(Operator operator) {
        this.operator = operator;
    }

    /**
     * Create an operator thanks to an {@link OperatorReader}.
     * 
     * @param operator
     *            the operator to read
     * @return the operator element
     */
    public static OperatorElement read(String operator) {
        // recognize the operator
        Operator foundOperator = operatorReader.read(operator);
        // create and return
        return new OperatorElement(foundOperator);
    }

    /**
     * Accesseur sur l'attribut {@link #operator}.
     *
     * @return Operator operator
     */
    public Operator getOperator() {
        return operator;
    }

}
