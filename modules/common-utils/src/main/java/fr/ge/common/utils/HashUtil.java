/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.ArrayUtils;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * 
 * @author Christian Cougourdan
 */
public final class HashUtil {

    /** The Constant HASH_ALGORITHM. */
    private static final String HASH_ALGORITHM = "SHA-256";

    /** The Constant HEX_MAPPING. */
    private static final char[] HEX_MAPPING = "0123456789ABCDEF".toCharArray();

    private HashUtil() {
        // Nothing to do
    }

    /**
     * Build a hash from specified value.
     *
     * @param value
     *            the value
     * @return hash as hexadecimal string
     */
    public static String hashAsString(final byte[] value) {
        if (ArrayUtils.isEmpty(value)) {
            return null;
        }

        final byte[] raw = hashAsBytes(value);

        final StringBuilder hash = new StringBuilder();
        for (final byte b : raw) {
            hash.append(HEX_MAPPING[b >> 4 & 0x0f]).append(HEX_MAPPING[b & 0x0f]);
        }

        return hash.toString();
    }

    /**
     * Build a hash from specified value.
     *
     * @param value
     *            the value
     * @return hash as hexadecimal string
     */
    public static String hashAsString(final String value) {
        if (null == value) {
            return null;
        } else {
            return hashAsString(value.getBytes(StandardCharsets.UTF_8));
        }
    }

    /**
     * Build a raw hash from specified value.
     *
     * @param value
     *            the value
     * @return hash as byte array
     */
    public static byte[] hashAsBytes(final byte[] value) {
        if (ArrayUtils.isEmpty(value)) {
            return new byte[] {};
        }

        final MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(HASH_ALGORITHM);
        } catch (final NoSuchAlgorithmException e) {
            throw new TechnicalException(String.format("No %s digest found", HASH_ALGORITHM), e);
        }

        return digest.digest(value);
    }

    /**
     * Build a raw hash from specified value.
     *
     * @param value
     *            the value
     * @return hash as byte array
     */
    public static byte[] hashAsBytes(final String value) {
        if (null == value) {
            return new byte[] {};
        } else {
            return hashAsBytes(value.getBytes(StandardCharsets.UTF_8));
        }
    }

}
