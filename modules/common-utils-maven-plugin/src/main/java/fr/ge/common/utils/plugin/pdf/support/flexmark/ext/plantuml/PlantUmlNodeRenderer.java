package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.plantuml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import com.vladsch.flexmark.ast.FencedCodeBlock;
import com.vladsch.flexmark.html.HtmlWriter;
import com.vladsch.flexmark.html.renderer.NodeRenderer;
import com.vladsch.flexmark.html.renderer.NodeRendererContext;
import com.vladsch.flexmark.html.renderer.NodeRendererFactory;
import com.vladsch.flexmark.html.renderer.NodeRenderingHandler;
import com.vladsch.flexmark.util.data.DataHolder;

import net.sourceforge.plantuml.*;

public class PlantUmlNodeRenderer implements NodeRenderer {

    public PlantUmlNodeRenderer(final DataHolder options) {
    }

    @Override
    public Set<NodeRenderingHandler<?>> getNodeRenderingHandlers() {
        final Set<NodeRenderingHandler<?>> set = new HashSet<>();

        set.add( //
                new NodeRenderingHandler<>( //
                        FencedCodeBlock.class, //
                        this::render //
                ) //
        );

        return set;
    }

    private void render(final FencedCodeBlock node, final NodeRendererContext context, final HtmlWriter html) {
        if (node.getInfo().equals("plant-uml")) {
            final SourceStringReader reader = new SourceStringReader(node.getContentChars().normalizeEOL());
            try (ByteArrayOutputStream png = new ByteArrayOutputStream() /* ; ByteArrayOutputStream svg = new ByteArrayOutputStream() */) {
                reader.outputImage(png);
                // reader.outputImage(svg, new FileFormatOption(FileFormat.SVG));

                html //
                        .attr("class", "plant-uml") //
                        .attr("src", "data:image/png;base64," + Base64.getEncoder().encodeToString(png.toByteArray())) //
                        .withAttr() //
                        .tagVoid("img").line();

                // html //
                // .attr("class", "plant-uml") //
                // .withAttr() //
                // .tag("div").line() //
                // .attr("style", "width: 100%").withAttr() //
                // .append(new String(svg.toByteArray(), StandardCharsets.UTF_8)).line() //
                // .closeTag("div").line();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        } else {
            context.delegateRender();
        }
    }

    public static final class Factory implements NodeRendererFactory {

        @Override
        public NodeRenderer apply(final DataHolder options) {
            return new PlantUmlNodeRenderer(options);
        }

    }
}
