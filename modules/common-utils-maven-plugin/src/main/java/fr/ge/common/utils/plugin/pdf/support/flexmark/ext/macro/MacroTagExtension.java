package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import java.util.Locale;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.html.HtmlRenderer.Builder;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.data.DataKey;
import com.vladsch.flexmark.util.data.MutableDataHolder;

/**
 *
 * @author Christian Cougourdan
 */
public class MacroTagExtension implements Parser.ParserExtension, HtmlRenderer.HtmlRendererExtension {

    public static final DataKey<Locale> LOCALE = new DataKey<>("MACRO_LOCALE", Locale.getDefault());

    public static MacroTagExtension create() {
        return new MacroTagExtension();
    }

    @Override
    public void rendererOptions(final MutableDataHolder options) {
    }

    @Override
    public void extend(final Builder rendererBuilder, final String rendererType) {
        if ("HTML".equals(rendererType)) {
            rendererBuilder.nodeRendererFactory(new MacroTagNodeRenderer.Factory());
        }
    }

    @Override
    public void parserOptions(final MutableDataHolder options) {
    }

    @Override
    public void extend(final Parser.Builder parserBuilder) {
        parserBuilder.customInlineParserExtensionFactory(new MacroTagInlineParser.Factory());
    }

}
