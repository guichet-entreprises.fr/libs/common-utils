package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vladsch.flexmark.parser.InlineParser;
import com.vladsch.flexmark.parser.InlineParserExtension;
import com.vladsch.flexmark.parser.InlineParserExtensionFactory;
import com.vladsch.flexmark.parser.LightInlineParser;
import com.vladsch.flexmark.util.sequence.BasedSequence;

/**
 *
 * @author Christian Cougourdan
 */
public class MacroTagInlineParser implements InlineParserExtension {

    private static final Map<String, IMacroTagParser> PARSER;

    static {
        final Map<String, IMacroTagParser> m = new HashMap<>();

        m.put("openapi", new OpenApiMacroTagParser());

        PARSER = Collections.unmodifiableMap(m);
    }

    private final Pattern PATTERN_TAG;

    public MacroTagInlineParser(final LightInlineParser inlineParser) {
        this.PATTERN_TAG = Pattern.compile("%\\{(" + inlineParser.getParsing().TAGNAME + ")(?::([^}]+))\\}");
    }

    @Override
    public void finalizeDocument(final InlineParser inlineParser) {
    }

    @Override
    public void finalizeBlock(final InlineParser inlineParser) {
    }

    @Override
    public boolean parse(final LightInlineParser inlineParser) {
        final BasedSequence input = inlineParser.getInput();
        final Matcher matcher = inlineParser.matcher(this.PATTERN_TAG);

        if (null != matcher) {
            final BasedSequence tag = input.subSequence(matcher.start(), matcher.end());
            final BasedSequence macroName = input.subSequence(matcher.start(1), matcher.end(1));
            final BasedSequence parameters = input.subSequence(matcher.start(2), matcher.end(2));

            final MacroTag macro = new MacroTag(tag.subSequence(0, 2), macroName, parameters, tag.endSequence(1));

            inlineParser.flushTextNode();

            if (PARSER.containsKey(macroName.toString())) {
                PARSER.get(macroName.toString()).parse(inlineParser, macro);
            } else {
                inlineParser.getBlock().appendChild(macro);
            }

            return true;
        }

        return false;
    }

    public static class Factory implements InlineParserExtensionFactory {

        @Override
        public Set<? extends Class<?>> getAfterDependents() {
            return null;
        }

        @Override
        public Set<? extends Class<?>> getBeforeDependents() {
            return null;
        }

        @Override
        public boolean affectsGlobalScope() {
            return false;
        }

        @Override
        public CharSequence getCharacters() {
            return "%{";
        }

        @Override
        public InlineParserExtension apply(final LightInlineParser inlineParser) {
            return new MacroTagInlineParser(inlineParser);
        }

    }

}
