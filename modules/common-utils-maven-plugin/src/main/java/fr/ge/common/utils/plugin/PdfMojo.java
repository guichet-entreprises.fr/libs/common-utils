package fr.ge.common.utils.plugin;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Predicate;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.plugin.pdf.MarkdownOptions;
import fr.ge.common.utils.plugin.pdf.MarkdownToPdf;

/**
 * Maven plugin used to generate PDF documentation from Markdown.
 *
 * @author Christian Cougourdan
 */
@Mojo(name = "pdf")
public class PdfMojo extends AbstractMojo {

    @Parameter(defaultValue = "${utils.doc.directory}", required = true)
    private String directory;

    @Parameter(defaultValue = "${utils.doc.includes}", required = false)
    private final Collection<String> includes = Arrays.asList("**/*.*");

    @Parameter(defaultValue = "${utils.doc.excludes}", required = false)
    private final Collection<String> excludes = Collections.emptyList();

    @Parameter(defaultValue = "${utils.doc.targetPath}", required = true)
    private String targetPath;

    @Parameter(defaultValue = "${utils.doc.styles}", required = false)
    private String styles;

    @Parameter(defaultValue = "${utils.doc.locale}", required = false)
    private String locale;

    @Parameter(required = false)
    private Properties properties;

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException {
        this.getLog().info("Project : " + this.project);
        MarkdownOptions.get() //
                .setLocale(Optional.ofNullable(this.locale).map(Locale::forLanguageTag).orElse(Locale.getDefault())) //
                .set(MarkdownOptions.PROJECT, this.project) //
                .set(MarkdownOptions.PROPERTIES, this.properties);

        if (null != this.styles) {
            MarkdownOptions.get().set(MarkdownOptions.STYLES, this.styles);
        }

        final MarkdownToPdf generator = new MarkdownToPdf().setLog(this.getLog());
        final Path sourcePath = Paths.get(this.directory);
        final Path destinationPath = Paths.get(this.targetPath);

        this.getLog().info("source directory : " + this.directory);
        this.getLog().info("destination path : " + this.targetPath);

        if (!Files.isDirectory(sourcePath)) {
            this.getLog().warn(String.format("[directory] parameters is not a ... directory", sourcePath));
            return;
        }

        final Path baseDestinationPath = !Files.exists(destinationPath) || Files.isDirectory(destinationPath) ? destinationPath : destinationPath.getParent();

        if (!Files.exists(baseDestinationPath)) {
            try {
                Files.createDirectories(baseDestinationPath);
            } catch (final IOException e) {
                this.getLog().error("Unable to create directory '" + baseDestinationPath + "'");
                throw new MojoExecutionException("Unable to create directory '" + baseDestinationPath + "'", e);
            }
        }

        final Predicate<String> includeFilter = this.includes.stream() //
                .map(ResourceUtil::filterAsPredicate) //
                .reduce(value -> false, (all, elm) -> all.or(elm));

        final Predicate<String> excludeFilter = this.excludes.stream() //
                .map(ResourceUtil::filterAsPredicate) //
                .reduce(value -> false, (all, elm) -> all.or(elm));

        final FileVisitor<? super Path> visitor = new FileVisitor<Path>() {

            @Override
            public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(final Path src, final BasicFileAttributes attrs) throws IOException {

                if (!src.toString().endsWith(".md")) {
                    return FileVisitResult.CONTINUE;
                }

                final Path relativeSourcePath = sourcePath.relativize(src);
                final String pathToTest = relativeSourcePath.toString().replace('\\', '/');
                if (includeFilter.test(pathToTest) && !excludeFilter.test(pathToTest)) {
                    final Path dst = baseDestinationPath.resolve(relativeSourcePath.toString().replaceFirst("\\.md$", ".pdf"));
                    PdfMojo.this.getLog().debug("Generating '" + dst + "' from '" + src + "' ...");
                    generator.generate(src.toString(), dst.toString());
                }

                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(final Path file, final IOException exc) throws IOException {
                exc.printStackTrace();
                return FileVisitResult.CONTINUE;
            }
        };

        try {
            Files.walkFileTree(sourcePath, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE, visitor);
        } catch (final IOException e) {
            throw new MojoExecutionException("Unable to process files", e);
        }
    }

}
