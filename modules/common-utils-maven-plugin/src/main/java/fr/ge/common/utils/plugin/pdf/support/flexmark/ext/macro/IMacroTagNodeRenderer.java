package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import com.vladsch.flexmark.html.HtmlWriter;

/**
 * 
 * @author Christian Cougourdan
 */
public interface IMacroTagNodeRenderer {

    void render(MacroTag node, HtmlWriter html);

}
