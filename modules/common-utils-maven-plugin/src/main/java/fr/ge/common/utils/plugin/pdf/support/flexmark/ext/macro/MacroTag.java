package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.sequence.BasedSequence;

/**
 *
 * @author Christian Cougourdan
 */
public class MacroTag extends Node {

    private BasedSequence openingMarker;

    private BasedSequence name;

    private BasedSequence parameters;

    private BasedSequence closingMarker;

    public MacroTag(final BasedSequence tag) {
        super(tag);
    }

    public MacroTag(final BasedSequence openingMarker, final BasedSequence name, final BasedSequence parameters, final BasedSequence closingMarker) {
        super(openingMarker.baseSubSequence(openingMarker.getStartOffset(), closingMarker.getEndOffset()));

        this.openingMarker = openingMarker;
        this.name = name;
        this.parameters = parameters;
        this.closingMarker = closingMarker;
    }

    @Override
    public BasedSequence[] getSegments() {
        return null;
    }

    /**
     * @return the openingMarker
     */
    public BasedSequence getOpeningMarker() {
        return this.openingMarker;
    }

    /**
     * @param openingMarker
     *            the openingMarker to set
     */
    public void setOpeningMarker(final BasedSequence openingMarker) {
        this.openingMarker = openingMarker;
    }

    /**
     * @return the name
     */
    public BasedSequence getName() {
        return this.name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final BasedSequence name) {
        this.name = name;
    }

    /**
     * @return the parameters
     */
    public BasedSequence getParameters() {
        return this.parameters;
    }

    /**
     * @param parameters
     *            the parameters to set
     */
    public void setParameters(final BasedSequence parameters) {
        this.parameters = parameters;
    }

    /**
     * @return the closingMarker
     */
    public BasedSequence getClosingMarker() {
        return this.closingMarker;
    }

    /**
     * @param closingMarker
     *            the closingMarker to set
     */
    public void setClosingMarker(final BasedSequence closingMarker) {
        this.closingMarker = closingMarker;
    }

}
