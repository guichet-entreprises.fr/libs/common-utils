package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import org.apache.maven.project.MavenProject;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.messageresolver.IMessageResolver;
import org.thymeleaf.messageresolver.StandardMessageResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vladsch.flexmark.parser.LightInlineParser;
import com.vladsch.flexmark.util.ast.Document;

import fr.ge.common.utils.plugin.pdf.MarkdownOptions;
import fr.ge.common.utils.plugin.pdf.MarkdownReader;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.tags.Tag;
import io.swagger.v3.parser.OpenAPIV3Parser;

public class OpenApiMacroTagParser implements IMacroTagParser {

    @Override
    public void parse(final LightInlineParser inlineParser, final MacroTag node) {
        final String resourcePath = node.getParameters().normalizeEOL().toString();
        final OpenAPI openApi = new OpenAPIV3Parser().read(resourcePath);

        if (null == openApi) {
            return;
        }

        final MavenProject project = MarkdownOptions.get().get(MarkdownOptions.PROJECT);
        if (null != project) {
            System.out.println("" + project);
            System.out.println(String.join(", ", project.getFilters()));
        }

        final Map<String, Function<PathItem, Operation>> suppliers = new LinkedHashMap<>();
        suppliers.put("HEAD", PathItem::getHead);
        suppliers.put("OPTIONS", PathItem::getOptions);
        suppliers.put("TRACE", PathItem::getTrace);
        suppliers.put("GET", PathItem::getGet);
        suppliers.put("POST", PathItem::getPost);
        suppliers.put("PUT", PathItem::getPut);
        suppliers.put("PATCH", PathItem::getPatch);
        suppliers.put("DELETE", PathItem::getDelete);

        final Map<String, Tag> tagsInfo = new HashMap<>();
        if (null != openApi.getTags()) {
            for (final Tag tag : openApi.getTags()) {
                tagsInfo.put(tag.getName(), tag);
            }
        }

        final Map<String, TagInfo> tags = new LinkedHashMap<>();
        for (final Entry<String, PathItem> entry : openApi.getPaths().entrySet()) {
            final PathItem item = entry.getValue();

            for (final Entry<String, Function<PathItem, Operation>> supplier : suppliers.entrySet()) {
                final Operation operation = supplier.getValue().apply(item);

                if (null == operation) {
                    continue;
                }

                for (final Entry<String, ApiResponse> responseEntry : operation.getResponses().entrySet()) {
                    Optional.ofNullable(responseEntry.getValue().getContent()).ifPresent(contents -> {
                        for (final Entry<String, MediaType> contentEntry : contents.entrySet()) {
                            if ("application/json".contentEquals(contentEntry.getKey())) {

                                Optional.ofNullable(contentEntry.getValue().getExample()) //
                                        .map(Object::toString) //
                                        .map(OpenApiMacroTagParser::prettifyJson) //
                                        .ifPresent(contentEntry.getValue()::setExample);

                                Optional.ofNullable(contentEntry.getValue().getExamples()).ifPresent(examples -> {
                                    for (final Entry<String, Example> exampleEntry : examples.entrySet()) {
                                        Optional.ofNullable(exampleEntry.getValue().getValue()) //
                                                .map(Object::toString) //
                                                .map(OpenApiMacroTagParser::prettifyJson) //
                                                .ifPresent(exampleEntry.getValue()::setValue);
                                    }
                                });

                            }
                        }
                    });
                }

                List<String> operationTags = operation.getTags();
                if (null == operationTags || operationTags.isEmpty()) {
                    operationTags = Arrays.asList("Default");
                    if (!tagsInfo.containsKey("Default")) {
                        final Tag defaultTag = new Tag();
                        defaultTag.setName("Default");
                        tagsInfo.put("Default", defaultTag);
                    }
                }

                for (final String tagName : operationTags) {
                    if (!tags.containsKey(tagName)) {
                        Tag tagInfo = tagsInfo.get(tagName);
                        if (null == tagInfo) {
                            tagInfo = new Tag();
                            tagInfo.setName(tagName);
                            tagsInfo.put(tagName, tagInfo);
                        }
                        tags.put(tagName, new TagInfo(tagInfo));
                    }

                    final TagInfo nfo = tags.get(tagName);
                    nfo.addOperation(new OperationTagInfo(supplier.getKey(), entry.getKey(), item, operation));

                    // operation.getParameters().get(0) //
                    // ;
                    // .getSchema().getT

                    operation.getRequestBody();
                    operation.getResponses();
                }
            }

            item.getSummary();
        }

        final MarkdownReader reader = new MarkdownReader();

        final Context context = new Context(MarkdownOptions.get().get(MacroTagExtension.LOCALE));
        context.setVariable("tags", tags.values());
        context.setVariable("api", openApi);

        final String str = this.getTemplateEngine().process("openapi-v3", context);
        final Document doc = reader.parse(str);
        inlineParser.getBlock().appendChain(doc.getFirstChild());

        // final Node root = inlineParser.getBlock();
        // for (final Tag tag : tagsInfo.values()) {
        // root.appendChild(this.buildHeading(tag.getName(), 3));
        // if (StringUtils.isNotEmpty(tag.getDescription())) {
        // final Document doc = reader.parse(tag.getDescription());
        // root.appendChain(doc.getFirstChild());
        // }
        // }

    }

    private static String prettifyJson(final String json) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final JsonNode root = mapper.readTree(json);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
        } catch (final JsonProcessingException e) {
            System.err.println(MessageFormat.format("{0} for {1}", e.getMessage(), json));
        }
        return json;
    }

    private TemplateEngine getTemplateEngine() {
        final ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setOrder(1);
        resolver.setPrefix("/templates/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode(TemplateMode.TEXT);
        resolver.setCacheable(false);
        resolver.setCharacterEncoding("UTF-8");

        final IMessageResolver messageResolver = new StandardMessageResolver();

        final TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        engine.setMessageResolver(messageResolver);

        return engine;
    }

    public static class TagInfo {

        private final Tag tag;

        private final Collection<OperationTagInfo> operations = new ArrayList<>();

        public TagInfo(final Tag tag) {
            this.tag = tag;
        }

        public Tag getTag() {
            return this.tag;
        }

        public Collection<OperationTagInfo> getOperations() {
            return this.operations;
        }

        public void addOperation(final OperationTagInfo operation) {
            this.operations.add(operation);
        }

    }

    public static class OperationTagInfo {

        private final String method;

        private final String path;

        private final PathItem pathItem;

        private final Operation operation;

        public OperationTagInfo(final String method, final String path, final PathItem pathItem, final Operation operation) {
            this.method = method;
            this.path = path;
            this.pathItem = pathItem;
            this.operation = operation;
        }

        public String getMethod() {
            return this.method;
        }

        public String getPath() {
            return this.path;
        }

        public PathItem getPathItem() {
            return this.pathItem;
        }

        public Operation getOperation() {
            return this.operation;
        }

    }

}
