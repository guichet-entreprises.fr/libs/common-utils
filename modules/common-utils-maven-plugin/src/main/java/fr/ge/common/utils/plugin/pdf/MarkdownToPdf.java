package fr.ge.common.utils.plugin.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder.TextDirection;
import com.vladsch.flexmark.pdf.converter.PdfConverterExtension;

import fr.ge.common.utils.plugin.core.AbstractLog;

/**
 * @author Christian Cougourdan
 */
public class MarkdownToPdf extends AbstractLog<MarkdownToPdf> {

    public void generate(final String src, final String dst) {
        File f = new File(src);
        Path basePath = Paths.get(f.getParent());
        try (InputStream in = new FileInputStream(src)) {
            this.generate(in, dst, basePath);
        } catch (final FileNotFoundException ex) {
            this.log.error("Source file '" + src + "' not found", ex);
        } catch (final IOException ex) {
            this.log.error("Error while reading source file '" + src + "'", ex);
        }
    }

    public void generate(final InputStream src, final String dst, Path basePath) {
        final Path dstBasePath = Paths.get(dst).getParent();
        if (!Files.exists(dstBasePath)) {
            try {
                Files.createDirectories(dstBasePath);
            } catch (final IOException ex) {
                this.log.error("Error while creating destination path '" + dst + "'", ex);
            }
        }

        try (OutputStream dstOutputStream = new FileOutputStream(dst)) {
            this.generate(src, dstOutputStream, basePath);
        } catch (final FileNotFoundException ex) {
            this.log.error("Destination file '" + src + "' not found", ex);
        } catch (final IOException ex) {
            this.log.error("Error while writing destination file '" + src + "'", ex);
        }
    }

    public void generate(final InputStream src, final OutputStream dst, Path basePath) {
        final String html = new MarkdownToHtml().convert(src, basePath);
        this.generateFromHtml(html, dst, basePath);
    }

    public void generateFromHtml(final String html, final OutputStream dst, Path basePath) {
        PdfConverterExtension.exportToPdf( //
                dst, //
                html, //
                Optional.ofNullable(basePath).orElse(Paths.get(".")).toUri().toString(), //
                TextDirection.LTR //
        );
    }

}
