package fr.ge.common.utils.plugin.core;

import org.apache.maven.plugin.logging.Log;

/**
 *
 * @author Christian Cougourdan
 *
 * @param <T>
 */
public class AbstractLog<T extends AbstractLog<?>> {

    protected Log log;

    /**
     *
     * @param value
     *            new log instance
     * @return current instance
     */
    @SuppressWarnings("unchecked")
    public T setLog(final Log value) {
        this.log = value;
        return (T) this;
    }
}
