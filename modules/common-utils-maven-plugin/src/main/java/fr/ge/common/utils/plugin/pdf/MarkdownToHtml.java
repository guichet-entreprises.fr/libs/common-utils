package fr.ge.common.utils.plugin.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.ext.jekyll.tag.JekyllTag;
import com.vladsch.flexmark.ext.jekyll.tag.JekyllTagExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.pdf.converter.PdfConverterExtension;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.IRender;

import fr.ge.common.utils.plugin.core.AbstractLog;

/**
 * @author Christian Cougourdan
 */
public class MarkdownToHtml extends AbstractLog<MarkdownToHtml> {

    /**
     * Convert markdown resource to HTML.
     *
     * @param srcPath
     *     markdown resource path
     * @return html converted resource
     */
    public String convert(final String srcPath) {
        return this.convert(this.getReader().read(srcPath), new File(srcPath).toPath().getParent());
    }

    public String convert(final InputStream src, Path basePath) {
        return this.convert(this.getReader().read(src), basePath);
    }

    public String convert(final Document doc, Path basePath) {
        if (doc.contains(JekyllTagExtension.TAG_LIST)) {
            List<JekyllTag> tagList = doc.get(JekyllTagExtension.TAG_LIST);
            for (JekyllTag tag : tagList) {
                String includeFile = tag.getParameters().toString();
                if (tag.getTag().equals("include") && !includeFile.isEmpty()) {
                    if (includeFile.endsWith(".md")) {
                        Optional.ofNullable(this.getReader().read(basePath.resolve(includeFile).toString())) //
                                .ifPresent(tag::insertAfter);
                    } else {
                        Optional.ofNullable(includeFile) //
                                .map(basePath::resolve) //
                                .map(this::readResourceAsString) //
                                .map(Text::new) //
                                .ifPresent(tag::insertAfter);
                    }
                }
            }
        }

        final String rawHtml = this.getHtmlRenderer().render(doc);

        return PdfConverterExtension.embedCss( //
                rawHtml, //
                this.loadStylesheets() //
        );
    }

    private String loadStylesheets() {
        final String rawStyles = this.readResourceAsString(MarkdownOptions.get().get(MarkdownOptions.STYLES));

        if (null == rawStyles) {
            return null;
        }

        final URL fontUrl = this.getClass().getResource("/styles/fonts");
        return rawStyles.replace("${fonts}", fontUrl.toString());
    }

    private String readResourceAsString(Path resourcePath) {
        try {
            byte[] bytes = Files.readAllBytes(resourcePath);
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            this.log.error(MessageFormat.format("Unable to read resource {} : {}", resourcePath, ex.getMessage()));
        }

        return null;
    }

    private String readResourceAsString(final String resourcePath) {
        try (InputStream src = this.getResourceStream(resourcePath)) {
            if (null == src) {
                return "";
            } else {
                return IOUtils.toString(src, StandardCharsets.UTF_8);
            }
        } catch (final IOException ex) {
            this.log.error(MessageFormat.format("Unable to read resource {} : {}", resourcePath, ex.getMessage()));
        }

        return null;
    }

    private InputStream getResourceStream(final String resourcePath) {
        try {
            final URI resourceUri = this.getResourceUri(resourcePath);

            if ("classpath".equals(resourceUri.getScheme())) {
                return Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceUri.getSchemeSpecificPart());
            } else {
                return new FileInputStream(Paths.get(resourceUri).toFile());
            }
        } catch (final URISyntaxException ex) {
            this.log.error(MessageFormat.format("URI syntax error on {} : {}", resourcePath, ex.getMessage()));
        } catch (final FileNotFoundException ex) {
            this.log.error(MessageFormat.format("File {} not found : {}", resourcePath, ex.getMessage()));
        }

        return null;
    }

    private URI getResourceUri(String resourcePath) throws URISyntaxException {
        Pattern pattern = Pattern.compile("^([a-z][a-z0-9+.-]+):(.*)$", Pattern.CASE_INSENSITIVE);
        if (pattern.matcher(resourcePath).matches()) {
            return URI.create(resourcePath);
        } else {
            return Paths.get(resourcePath).toUri();
        }
    }

    private IRender getHtmlRenderer() {
        return HtmlRenderer.builder(MarkdownOptions.get()).build();
    }

    private MarkdownReader getReader() {
        return new MarkdownReader().setLog(this.log);
    }

}
