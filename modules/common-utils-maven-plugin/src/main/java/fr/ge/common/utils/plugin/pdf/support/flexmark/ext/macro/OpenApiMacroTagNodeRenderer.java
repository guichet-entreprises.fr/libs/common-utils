package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.vladsch.flexmark.html.HtmlWriter;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.tags.Tag;
import io.swagger.v3.parser.OpenAPIV3Parser;

/**
 *
 * @author Christian Cougourdan
 */
public class OpenApiMacroTagNodeRenderer implements IMacroTagNodeRenderer {

    @Override
    public void render(final MacroTag node, final HtmlWriter html) {
        final OpenAPI openApi = new OpenAPIV3Parser().read(node.getParameters().normalizeEOL().toString());

        if (null == openApi) {
            return;
        }

        final Map<String, Function<PathItem, Operation>> suppliers = new LinkedHashMap<>();
        suppliers.put("HEAD", PathItem::getHead);
        suppliers.put("OPTIONS", PathItem::getOptions);
        suppliers.put("TRACE", PathItem::getTrace);
        suppliers.put("GET", PathItem::getGet);
        suppliers.put("POST", PathItem::getPost);
        suppliers.put("PUT", PathItem::getPut);
        suppliers.put("PATCH", PathItem::getPatch);
        suppliers.put("DELETE", PathItem::getDelete);

        final Map<String, Tag> tagsInfo = new HashMap<>();
        openApi.getTags().get(0);
        for (final Tag tag : openApi.getTags()) {
            tagsInfo.put(tag.getName(), tag);
        }

        final Map<String, TagInfo> tags = new LinkedHashMap<>();
        for (final Entry<String, PathItem> entry : openApi.getPaths().entrySet()) {
            final PathItem item = entry.getValue();

            for (final Entry<String, Function<PathItem, Operation>> supplier : suppliers.entrySet()) {
                final Operation operation = supplier.getValue().apply(item);

                if (null == operation) {
                    continue;
                }

                for (final String tag : operation.getTags()) {
                    if (!tags.containsKey(tag)) {
                        tags.put(tag, new TagInfo(tagsInfo.get(tag)));
                    }

                    final TagInfo nfo = tags.get(tag);
                    nfo.addOperation(new OperationTagInfo(supplier.getKey(), item, operation));
                }
            }

            item.getSummary();
        }

        final Context context = new Context(Locale.getDefault());
        context.setVariable("tags", tags.values());

        html.append(this.getTemplateEngine().process("openapi-v3", context));
    }

    private TemplateEngine getTemplateEngine() {
        final ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setOrder(1);
        resolver.setPrefix("/templates/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode(TemplateMode.HTML);
        resolver.setCacheable(false);
        resolver.setCharacterEncoding("UTF-8");

        final TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);

        return engine;
    }

    public static class TagInfo {

        private final Tag tag;

        private final Collection<OperationTagInfo> operations = new ArrayList<>();

        public TagInfo(final Tag tag) {
            this.tag = tag;
        }

        public Tag getTag() {
            return this.tag;
        }

        public Collection<OperationTagInfo> getOperations() {
            return this.operations;
        }

        public void addOperation(final OperationTagInfo operation) {
            this.operations.add(operation);
        }

    }

    public static class OperationTagInfo {

        private final String method;

        private final PathItem pathItem;

        private final Operation operation;

        public OperationTagInfo(final String method, final PathItem pathItem, final Operation operation) {
            this.method = method;
            this.pathItem = pathItem;
            this.operation = operation;
        }

        public String getMethod() {
            return this.method;
        }

        public PathItem getPathItem() {
            return this.pathItem;
        }

        public Operation getOperation() {
            return this.operation;
        }

    }

}
