package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;

import com.vladsch.flexmark.html.HtmlWriter;

import fr.ge.common.utils.plugin.pdf.MarkdownOptions;
import fr.ge.common.utils.plugin.pdf.reader.PropertyDescription;
import fr.ge.common.utils.plugin.pdf.reader.PropertyReader;

public class PropertiesMacroTagNodeRenderer implements IMacroTagNodeRenderer {

    @Override
    public void render(final MacroTag node, final HtmlWriter html) {
        final ResourceBundle bundle = ResourceBundle.getBundle("templates/openapi-v3", MarkdownOptions.get().get(MacroTagExtension.LOCALE));
        final String[] headers = new String[] { //
                bundle.getString("Parameter"), //
                bundle.getString("Description"), //
                bundle.getString("Value") //
        };

        final String resourcePath = node.getParameters().toString();
        final Collection<PropertyDescription> properties = this.readProperties(resourcePath);

        html.tagLineIndent("table", () -> {
            html.tagLineIndent("thead", () -> {
                html.tagLineIndent("tr", () -> {
                    for (final String header : headers) {
                        html.tagLine("th", () -> html.text(header));
                    }
                });
            });
            html.tagLineIndent("tbody", () -> {
                if (properties.isEmpty()) {
                    html.tagLineIndent("tr", () -> {
                        html.withAttr() //
                                .attr("colspan", "3") //
                                .tagLine("td", () -> {
                                    html.text("Resource '" + resourcePath + "' not found or is empty");
                                });
                    });
                } else {
                    for (final PropertyDescription property : properties) {
                        html.tagLineIndent("tr", () -> {
                            for (final String value : new String[] { property.getName(), property.getDescription(), property.getValue() }) {
                                html.tagLine("td", () -> {
                                    if (null != value) {
                                        html.raw(value.replaceAll("\\*{3}([^*]+)\\*{3}", "<strong>$1</strong>"));
                                    }
                                });
                            }
                        });
                    }
                }
            });
        });
    }

    private Collection<PropertyDescription> readProperties(final String src) {
        try {
            return new PropertyReader().read(src);
        } catch (final Exception ex) {
            ex.printStackTrace();
        }

        return Collections.emptyList();
    }

}
