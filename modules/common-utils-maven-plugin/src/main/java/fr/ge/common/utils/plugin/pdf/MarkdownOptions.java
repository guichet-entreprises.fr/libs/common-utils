package fr.ge.common.utils.plugin.pdf;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.project.MavenProject;

import com.vladsch.flexmark.ext.admonition.AdmonitionExtension;
import com.vladsch.flexmark.ext.attributes.AttributesExtension;
import com.vladsch.flexmark.ext.emoji.EmojiExtension;
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension;
import com.vladsch.flexmark.ext.gitlab.GitLabExtension;
import com.vladsch.flexmark.ext.jekyll.tag.JekyllTagExtension;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.ext.toc.TocExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.data.DataKey;
import com.vladsch.flexmark.util.data.MutableDataSet;

import fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro.MacroTagExtension;
import fr.ge.common.utils.plugin.pdf.support.flexmark.ext.plantuml.PlantUmlExtension;

public class MarkdownOptions extends MutableDataSet {

    public static final DataKey<MavenProject> PROJECT = new DataKey<>("global.project", (MavenProject) null);

    public static final DataKey<Map<?, ?>> CONTEXT = new DataKey<>("global.context", (Map<?, ?>) null);

    public static final DataKey<Properties> PROPERTIES = new DataKey<>("global.properties", new Properties());

    public static final DataKey<String> STYLES = new DataKey<>("global.styles", "classpath:styles/default.css");

    private static ThreadLocal<MarkdownOptions> instance = ThreadLocal.withInitial(MarkdownOptions::new);

    private MarkdownOptions() {
        super();
        this //
                .set( //
                        Parser.EXTENSIONS, //
                        Arrays.asList( //
                                AdmonitionExtension.create(), //
                                AttributesExtension.create(), //
                                JekyllTagExtension.create(), //
                                TablesExtension.create(), //
                                StrikethroughExtension.create(), //
                                TocExtension.create(), //
                                GitLabExtension.create(), //
                                MacroTagExtension.create(), //
                                EmojiExtension.create(), //
                                PlantUmlExtension.create() //
                        ) //
                ) //
                .set(HtmlRenderer.INDENT_SIZE, 4) //
                .set(HtmlRenderer.GENERATE_HEADER_ID, true) //
                .set(TocExtension.LIST_CLASS, "toc");
    }

    public MarkdownOptions setLocale(final Locale locale) {
        this.set(MacroTagExtension.LOCALE, locale);
        return this;
    }

    public static MarkdownOptions get() {
        return instance.get();
    }

}
