/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.plugin.pdf.reader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyReader.class);

    private static final Pattern PATTERN_PROPERTY = Pattern.compile("\\s*([^=]+)\\s*=\\s*(.*)");

    private static final Pattern PATTERN_COMMENT = Pattern.compile("\\s*#\\s*((@Value|)\\s*(.*))");

    private PropertyDescription currentPropertyDescription;

    private LineType previousLineType;

    private enum LineType {
        COMMENT, EMPTY, PROPERTY;
    }

    private final BiFunction<String, Collection<PropertyDescription>, Boolean> readProperty = (line, properties) -> {
        final Matcher m = PATTERN_PROPERTY.matcher(line);
        if (m.matches()) {
            if (null == this.currentPropertyDescription) {
                this.currentPropertyDescription = new PropertyDescription();
            }
            this.currentPropertyDescription.setName(m.group(1).trim()) //
                    .setToken(m.group(2).trim());

            this.previousLineType = LineType.PROPERTY;

            properties.add(this.currentPropertyDescription);
            this.currentPropertyDescription = null;

            return true;
        } else {
            return false;
        }
    };

    private final Function<String, Boolean> readComment = (line) -> {
        final Matcher m = PATTERN_COMMENT.matcher(line);
        if (m.matches()) {
            if (null == this.currentPropertyDescription || this.previousLineType != LineType.COMMENT) {
                this.currentPropertyDescription = new PropertyDescription();
            }

            if (!"".equals(m.group(1).trim())) {
                if (!"".equals(m.group(2).trim())) {
                    this.currentPropertyDescription.setValue(m.group(3).trim());
                } else if (null == this.currentPropertyDescription.getDescription()) {
                    this.currentPropertyDescription.setDescription(m.group(3).trim());
                } else {
                    this.currentPropertyDescription.setDescription(this.currentPropertyDescription.getDescription() + " " + m.group(3).trim());
                }
            }

            this.previousLineType = LineType.COMMENT;

            return true;
        }
        return false;
    };

    public Collection<PropertyDescription> read(final String sourceFilename) throws Exception {
        final Collection<PropertyDescription> properties = new ArrayList<>();

        this.previousLineType = LineType.EMPTY;
        this.currentPropertyDescription = null;

        try (Scanner reader = new Scanner(Paths.get(sourceFilename), "ISO-8859-1")) {
            String line;
            while (reader.hasNextLine()) {
                line = reader.nextLine();
                if ("".equals(line.trim())) {
                    this.previousLineType = LineType.EMPTY;
                    continue;
                }

                if (!this.readProperty.apply(line, properties) && !this.readComment.apply(line)) {
                    LOGGER.warn("Unable to parse line : " + line);
                }
            }
        } catch (final FileNotFoundException ex) {
            LOGGER.error("[" + sourceFilename + "] source file not found");
            throw new Exception("[" + sourceFilename + "] source file not found");
        } catch (final IOException ex) {
            LOGGER.error("[" + sourceFilename + "] error on reading file");
            throw new Exception("[" + sourceFilename + "] error on reading file", ex);
        }

        return properties;
    }

}
