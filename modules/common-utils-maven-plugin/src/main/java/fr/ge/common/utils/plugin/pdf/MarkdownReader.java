package fr.ge.common.utils.plugin.pdf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;

import fr.ge.common.utils.plugin.core.AbstractLog;

/**
 * @author Christian Cougourdan
 */
public class MarkdownReader extends AbstractLog<MarkdownReader> {

    /**
     * Parse Markdown document.
     *
     * @param srcPath
     *            Markdown source file path
     * @return read document
     */
    public Document read(final String srcPath) {
        try (InputStream src = new FileInputStream(srcPath)) {
            return this.read(src);
        } catch (final FileNotFoundException ex) {
            this.log.error(MessageFormat.format("Source file {0} not found ({1})", srcPath, ex.getMessage()));
        } catch (final IOException ex) {
            this.log.error(MessageFormat.format("Error reading Source file {0} : {1}", srcPath, ex.getMessage()));
        }

        return null;
    }

    /**
     * Parse Markdown document.
     *
     * @param src
     *            Markdown source input stream
     * @return read document
     */
    public Document read(final InputStream src) {
        try (InputStreamReader reader = new InputStreamReader(src, StandardCharsets.UTF_8)) {
            return this.getParser().parseReader(reader);
        } catch (final IOException ex) {
            this.log.error(MessageFormat.format("Error reading source file : {0}", ex.getMessage()), ex);
        }

        return null;
    }

    public Document parse(final String input) {
        return this.getParser().parse(input);
    }

    private Parser getParser() {
        return Parser.builder(MarkdownOptions.get()).build();
    }

}