package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import com.vladsch.flexmark.parser.LightInlineParser;

public interface IMacroTagParser {

    void parse(final LightInlineParser inlineParser, final MacroTag node);

}
