package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.plantuml;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.html.HtmlRenderer.Builder;
import com.vladsch.flexmark.util.data.MutableDataHolder;

public class PlantUmlExtension implements HtmlRenderer.HtmlRendererExtension {

    public static PlantUmlExtension create() {
        return new PlantUmlExtension();
    }

    @Override
    public void rendererOptions(final MutableDataHolder options) {
    }

    @Override
    public void extend(final Builder rendererBuilder, final String rendererType) {
        rendererBuilder.nodeRendererFactory(new PlantUmlNodeRenderer.Factory());
    }

}
