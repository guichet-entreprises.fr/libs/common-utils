package fr.ge.common.utils.plugin.pdf.support.flexmark.ext.macro;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.vladsch.flexmark.html.HtmlWriter;
import com.vladsch.flexmark.html.renderer.NodeRenderer;
import com.vladsch.flexmark.html.renderer.NodeRendererContext;
import com.vladsch.flexmark.html.renderer.NodeRendererFactory;
import com.vladsch.flexmark.html.renderer.NodeRenderingHandler;
import com.vladsch.flexmark.util.data.DataHolder;

/**
 *
 * @author Christian Cougourdan
 */
public class MacroTagNodeRenderer implements NodeRenderer {

    private static final Map<String, IMacroTagNodeRenderer> RENDERER;

    static {
        final Map<String, IMacroTagNodeRenderer> m = new HashMap<>();

        m.put("properties", new PropertiesMacroTagNodeRenderer());
        m.put("openapi", new OpenApiMacroTagNodeRenderer());

        RENDERER = Collections.unmodifiableMap(m);
    }

    public MacroTagNodeRenderer(final DataHolder options) {
    }

    @Override
    public Set<NodeRenderingHandler<?>> getNodeRenderingHandlers() {
        final Set<NodeRenderingHandler<?>> set = new HashSet<>();

        set.add(new NodeRenderingHandler<>(MacroTag.class, this::render));

        return set;
    }

    private void render(final MacroTag node, final NodeRendererContext context, final HtmlWriter writer) {
        final IMacroTagNodeRenderer renderer = RENDERER.get(node.getName().toString());
        if (null == renderer) {
            writer.text(node.getChars());
        } else {
            renderer.render(node, writer);
        }
    }

    public static class Factory implements NodeRendererFactory {

        @Override
        public NodeRenderer apply(final DataHolder options) {
            return new MacroTagNodeRenderer(options);
        }

    }

}
