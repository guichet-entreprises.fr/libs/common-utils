package fr.ge.common.utils.plugin;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Rule;
import org.junit.Test;

public class PdfMojoTest {

    @Rule
    public MojoRule rule = new MojoRule() {
        @Override
        protected void before() throws Throwable {
            super.before();
        }

        @Override
        protected void after() {
            super.after();
        }
    };

    @Test
    public void testSimple() throws Exception {
        final File pom = Paths.get(this.getClass().getResource("pom.xml").toURI()).toFile();
        assertNotNull(pom);
        assertTrue(pom.exists());

        final PdfMojo mojo = (PdfMojo) this.rule.lookupMojo("pdf", pom);
        assertNotNull(mojo);

        mojo.execute();
    }

}
