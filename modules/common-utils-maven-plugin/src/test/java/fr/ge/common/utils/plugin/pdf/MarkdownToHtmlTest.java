package fr.ge.common.utils.plugin.pdf;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Locale;

import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

import fr.ge.common.utils.test.AbstractTest;

public class MarkdownToHtmlTest extends AbstractTest {

    private MarkdownToHtml generator;

    @Before
    public void setUp() throws Exception {
        MarkdownOptions.get().setLocale(Locale.ENGLISH);
        this.generator = new MarkdownToHtml().setLog(new SystemStreamLog());
    }

    @Test
    public void testSimple() throws Exception {
        this.convert("simple.md");
    }

    @Test
    public void testBl() throws Exception {
        this.convert("bl.md");
    }

    private String convert(final String resourcePath) {
        final String resourceFullPath = this.getClass().getResource(this.resourceName(resourcePath)).getPath();
        final String html = this.generator.convert(resourceFullPath);

        try {
            Files.write( //
                    html.getBytes(StandardCharsets.UTF_8), //
                    Paths.get("target", resourcePath.replaceFirst("\\.[^.]+$", ".html")).toFile() //
            );
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return html;
    }

}
