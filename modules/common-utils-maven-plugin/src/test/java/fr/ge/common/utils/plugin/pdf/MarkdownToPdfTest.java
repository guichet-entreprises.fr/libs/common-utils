package fr.ge.common.utils.plugin.pdf;

import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.Before;
import org.junit.Test;

import fr.ge.common.utils.test.AbstractTest;

public class MarkdownToPdfTest extends AbstractTest {

    private MarkdownToPdf generator;

    @Before
    public void setUp() throws Exception {
        this.generator = new MarkdownToPdf().setLog(new SystemStreamLog());
    }

    @Test
    public void testSimple() throws Exception {
        this.generate("simple.md");
    }

    @Test
    public void testBl() throws Exception {
        this.generate("bl.md");
    }

    private void generate(final String src) {
        final String translatedResourceName = this.getClass().getResource(this.resourceName(src)).getPath();
        this.generator.generate(translatedResourceName, "target/" + src.replaceFirst("\\.[^.]+$", ".pdf"));
    }

}
