package fr.ge.common.utils.plugin;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.utils.ReaderFactory;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

public class TestingProjectStub extends MavenProject {

    public TestingProjectStub() {
        super(loadModel());
    }

    private static Model loadModel() {
        try {
            final File basePath = Paths.get(TestingProjectStub.class.getResource("pom.xml").toURI()).toFile();
            final MavenXpp3Reader reader = new MavenXpp3Reader();
            return reader.read(ReaderFactory.newXmlReader(basePath));
        } catch (IOException | XmlPullParserException e) {
            throw new RuntimeException(e);
        } catch (final URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
