# Bordereau de livraison de FEEDBACK

[TOC]


## ChangeLog

### User Stories & Enalers

### Changelog 
https://tools.projet-ge.fr/gitlab/sonic/exchange/blob/exchange-${project.version}/Changelog.md


## noeud applicatif BDD

### référence du binaire

| --- | --- |
| **Group ID**    | ${project.groupId} |
| **Artifact ID** | ge-feedback-webclient |
| **URL Nexus**   | https://tools.projet-ge.fr/nexus/content/groups/global/fr/ge/common/feedback/ge-feedback-service/${project.version}/ge-feedback-service-${project.version}.tar.gz |
| **Version**     | ${project.version} |

### URLs des sources et build

| ordre de build | URL du SCM                                                                                                              | commande de build | composant | binaire final |
|----------------|-------------------------------------------------------------------------------------------------------------------------|-------------------|-----------|---------------|
| 1              | https://tools.projet-ge.fr/gitlab/minecraft/ge-feedback/tree/ge-feedback-${project.version}/modules/ge-feedback-service | mvn clean deploy  | livrable  | non           |


## noeud applicatif REST SERVER

### référence du binaire

| --- | --- |
| **Group ID**    | ${project.groupId} |
| **Artifact ID** | ge-feedback-ws-server |
| **URL Nexus**   | https://tools.projet-ge.fr/nexus/content/groups/global/fr/ge/common/feedback/ge-feedback-ws-server/${project.version}/ge-feedback-ws-server-${project.version}.war |
| **Version**     | ${project.version} |

### URLs des sources et build
| ordre de build | URL du SCM                                                                                                                                                | commande de build | composant | binaire final |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|-----------|---------------|
| 1              | https://tools.projet-ge.fr/gitlab/minecraft/ge-feedback/tree/ge-feedback-${project.version}/modules/ge-feedback-webservices/modules/ge-feedback-ws-server | mvn clean deploy  | livrable  | non           |


## noeud applicatif IHM

### référence du binaire

| --- | --- |
| **Group ID**    | ${project.groupId} |
| **Artifact ID** | ge-feedback-webclient |
| **URL Nexus**   | https://tools.projet-ge.fr/nexus/content/groups/global/fr/ge/common/feedback/ge-feedback-webclient/${project.version}/ge-feedback-webclient-${project.version}.war |
| **Version**     | ${project.version} |

### URLs des sources et build

| ordre de build | URL du SCM                                                                                                                | commande de build | composant | binaire final |
|----------------|---------------------------------------------------------------------------------------------------------------------------|-------------------|-----------|---------------|
| 1              | https://tools.projet-ge.fr/gitlab/minecraft/ge-feedback/tree/ge-feedback-${project.version}/modules/ge-feedback-webclient | mvn clean deploy  | livrable  | non           |


