# Common Markdown Plugin Cheat Sheet

[TOC levels=2-4]



## Inline formatting

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Or</th>
            <th>... to Get</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>*Italic*</code></td>
            <td><code>_Italic_</code></td>
            <td><i>Italic</i></td>
        </tr>
        <tr>
            <td><code>**Bold**</code></td>
            <td><code>__Bold__</code></td>
            <td><strong>Bold</strong></td>
        </tr>
        <tr>
            <td><code>~~Strike~~</code></td>
            <td><code>{-Strike-}</code> (GFM: remove)</td>
            <td><del>Strike</del></td>
        </tr>
        <tr>
            <td><code></code></td>
            <td><code>{+Underline+}</code> (GFM: add)</td>
            <td><ins>Underline</ins></td>
        </tr>
        <tr>
            <td><code># Heading 1</code></td>
            <td><code>Heading 1<br/>=========</code></td>
            <td><h1>Heading 1</h1></td>
        </tr>
        <tr>
            <td><code>## Heading 1</code></td>
            <td><code>Heading 2<br/>---------</code></td>
            <td><h2>Heading 2</h2></td>
        </tr>
        <tr>
            <td><code>[Link](http://a.com)</code></td>
            <td><code>[Link][1]<br/>&vellip;<br/>[1]: http://b.org</td>
            <td>[Link](http://a.com)</td>
        </tr>
        <tr>
            <td><code>![Image](http://url/a.png)</code></td>
            <td><code>![Image][1]<br/>&vellip;<br/>[1]: http://url/b.jpg</code></td>
            <td><img alt="Image" src="https://cdn.commonmark.org/uploads/default/original/2X/6/65d5254a09786b1bb8c19ad9c8e2285bffef558a.png" style="height: 1.4em" /></td>
        </tr>
        <tr>
            <td><code>&gt; Blockquote</code></td>
            <td></td>
            <td><blockquote>Blockquote</blockquote></td>
        </tr>
        <tr>
            <td><code>* List<br/>* List<br/>* List</code></td>
            <td><code>- List<br/>- List<br/>- List</code></td>
            <td><ul><li>List</li><li>List</li><li>List</li></ul></td>
        </tr>
        <tr>
            <td><code>1. One<br/>2. Two<br/>3. Three</code> | <code>1) One<br/>2) Two<br/>3) Three</code></td>
            <td><ol><li>One</li><li>Two</li><li>Three</li></ol></td>
        </tr>
        <tr>
            <td><code>Horizontal Rule<br/>---</code></td>
            <td><code>Horizontal Rule<br/>***</code></td>
            <td>Horizontal Rule<br/><hr/></td>
        </tr>
        <tr>
            <td><code>`Inline code` with backticks</code></td>
            <td></td>
            <td><code>Inline code</code> with backticks</td>
        </tr>
        <tr>
            <td><code>```<br/># code block<br/>print '3 backticks or'<br/>print 'indent 4 spaces'<br/>```</td>
            <td><code>&middot;&middot;&middot;&middot;# code block<br/>&middot;&middot;&middot;&middot;print '3 backticks or'<br/>&middot;&middot;&middot;&middot;print 'indent 4 spaces'</code></td>
            <td><pre><code># code block
print '3 backticks or'
print 'indent 4 spaces'</code></pre></td>
        </tr>
    </tbody>
</table>


## Admonitions

```markdown
!!! <keyword> <title>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

Argument `title` is optional. If empty, no title bar will be drawn.

You can use all of these admonition keywords :

!!! abstract "Optional title"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! bug ""
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! danger
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! example
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! fail
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! faq
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! info
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! note
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! quote
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! success
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! tip
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! warning
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


## Adding properties documentation

Using this syntax :

    %{properties:src/test/resources/application.properties}

With this properties file :

    # Environment name
    # @Value "Development", "Qualification", ... .
    environment=@environment@
    
    # Applied theme
    # @Value default-one
    ui.theme.default=default-one
    ui.theme.defaultSubTheme=sub-default-one
    
    # Managed user type
    # @Value user
    engine.user.type=@engine.user.type@

You will obtains :
%{properties:src/test/resources/application.properties}


## Inserting PlantUML diagram

Using this syntax :

    ~~~plant-uml
    @startuml
    Bob -> Alice : hello
    @enduml
    ~~~

You will obtains :

~~~plant-uml
@startuml
Bob -> Alice : hello
@enduml
~~~


## Adding OpenAPI v3 documentation

Using this syntax :

    %{openapi:src/test/resources/openapi.yaml}

You will obtains (Swagger v2) :
%{openapi:src/test/resources/swagger-v2.yaml}


## PetStore REST API

Or (OpenAPI v3) :
%{openapi:src/test/resources/openapi-v3.yaml}

## Tracker REST API

%{openapi:src/test/resources/openapi-tracker.yaml}
