/*
 * 
 */
package fr.ge.common.utils.bdd.service;

/**
 * Schema version service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface ISchemaVersionService {

    String getCurrentVersion();

}
