package fr.ge.common.utils.bdd.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Schema version service implementation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class SchemaVersionServiceImpl implements ISchemaVersionService {

    @Autowired
    private SchemaVersionDao dao;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCurrentVersion() {
        return Optional.of(this.dao.getCurrentVersion()).map(SchemaVersionBean::getVersion).orElse(null);
    }

}
