package fr.ge.common.utils.bdd.ws.service;

import java.util.Optional;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bdd.service.ISchemaVersionService;

/**
 * Schema version REST Service Implementation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SchemaVersionRestServiceImpl implements ISchemaVersionRestService {

    /** Le dao de schema version. */
    @Autowired
    private ISchemaVersionService service;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response getCurrentVersion() {
        return Response.ok(Optional.of(this.service.getCurrentVersion()).orElse(null)).build();
    }

}
