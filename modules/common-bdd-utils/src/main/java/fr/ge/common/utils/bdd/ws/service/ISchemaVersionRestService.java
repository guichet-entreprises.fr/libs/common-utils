/**
 * 
 */
package fr.ge.common.utils.bdd.ws.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Schema version REST Service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface ISchemaVersionRestService {

    /**
     * Get the current version.
     * 
     * @return the response
     */
    @GET
    @Path("/version")
    @Produces(MediaType.TEXT_PLAIN)
    Response getCurrentVersion();

}
