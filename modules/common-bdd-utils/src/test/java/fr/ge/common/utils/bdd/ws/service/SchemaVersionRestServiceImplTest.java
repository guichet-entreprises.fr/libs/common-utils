package fr.ge.common.utils.bdd.ws.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bdd.service.ISchemaVersionService;
import fr.ge.common.utils.test.AbstractRestTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-service-context.xml", "classpath:spring/rest-ws-context.xml" })
public class SchemaVersionRestServiceImplTest extends AbstractRestTest {

    @Autowired
    private ISchemaVersionService service;

    @Test
    public void testGetCurrentVersion() throws Exception {
        Mockito.when(this.service.getCurrentVersion()).thenReturn("1.1.0.0");

        final Response response = this.client().path("/version").get();

        Mockito.verify(this.service).getCurrentVersion();
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsString(response), equalTo("1.1.0.0"));
    }
}
