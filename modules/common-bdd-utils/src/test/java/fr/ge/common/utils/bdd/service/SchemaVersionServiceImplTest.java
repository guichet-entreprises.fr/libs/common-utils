package fr.ge.common.utils.bdd.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class SchemaVersionServiceImplTest extends AbstractPostgreSqlDbTest {

    @Autowired
    private SchemaVersionDao dao;

    @Autowired
    private ISchemaVersionService service;

    @Test
    public void testGetCurrentVersion() throws Exception {
        Mockito.when(this.dao.getCurrentVersion()).thenReturn(new SchemaVersionBean().setVersion("1.1.0.0"));

        assertThat(this.service.getCurrentVersion(), equalTo("1.1.0.0"));

        Mockito.verify(this.dao).getCurrentVersion();
    }
}
