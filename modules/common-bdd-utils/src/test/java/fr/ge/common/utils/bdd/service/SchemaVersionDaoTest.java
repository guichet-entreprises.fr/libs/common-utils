package fr.ge.common.utils.bdd.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class SchemaVersionDaoTest extends AbstractPostgreSqlDbTest {

    @Autowired
    private SchemaVersionDao dao;

    @Test
    public void testGetCurrentVersion() throws Exception {
        assertThat(this.dao.getCurrentVersion(), allOf( //
                notNullValue(), //
                hasProperty("version", equalTo("1.1.0.0")) //
        ));
    }

}
