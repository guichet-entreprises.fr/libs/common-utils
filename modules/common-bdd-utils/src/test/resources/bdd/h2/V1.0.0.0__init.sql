CREATE TABLE IF NOT EXISTS schema_version
(
  installed_rank integer NOT NULL,
  version character varying(50),
  description character varying(200) NOT NULL,
  type character varying(20) NOT NULL,
  script character varying(1000) NOT NULL,
  checksum integer,
  installed_by character varying(100) NOT NULL,
  installed_on timestamp without time zone NOT NULL DEFAULT now(),
  execution_time integer NOT NULL,
  success boolean NOT NULL,
  CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank)
);

INSERT INTO schema_version (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success)
VALUES (1, '1.0.0.0', 'description 1', 'SQL', 'v1/V1.0.0.0.sql', 1111111, 'test', now(), 1, true);

INSERT INTO schema_version (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success)
VALUES (2, '1.1.0.0', 'description 2', 'SQL', 'v1/V1.1.0.0.sql', 1111112, 'test', now(), 2, true);
