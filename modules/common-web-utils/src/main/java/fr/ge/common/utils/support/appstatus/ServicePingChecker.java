/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.support.appstatus;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.utils.exception.TechnicalException;
import net.sf.appstatus.core.check.CheckResultBuilder;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Service ping checker.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ServicePingChecker extends AbstractPingChecker {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServicePingChecker.class);

    /** Application name. */
    private String appName;

    /** URL. */
    private String url;

    /**
     * Constructor.
     *
     * @param name
     *            the name
     * @param url
     *            the URL
     */
    public ServicePingChecker(final String name, final String url) {
        this.appName = name;
        this.url = url;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICheckResult checkStatus(final Locale locale) {
        final CheckResultBuilder result = this.result().from(this);
        try {
            this.pingService.ping(getAppName(), getUrl());
            result.code(ICheckResult.OK).description(this.appName + " available");
        } catch (TechnicalException e) {
            result.code(ICheckResult.ERROR).description(this.appName + " unavailable").resolutionSteps(this.url + " - " + e.getMessage());
            LOGGER.error(StringUtils.EMPTY, e);
        }
        ICheckResult checkResult = result.build();
        LOGGER.debug(String.format("App : %s, Code result : %s and resolution steps : %s", appName, checkResult.getCode(), checkResult.getResolutionSteps()));
        return checkResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getGroup() {
        return "Applications";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Availability";
    }

    /**
     * Accesseur sur l'attribut {@link #appName}.
     *
     * @return String appName
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Mutateur sur l'attribut {@link #appName}.
     *
     * @param appName
     *            la nouvelle valeur de l'attribut appName
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * Accesseur sur l'attribut {@link #url}.
     *
     * @return String url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Mutateur sur l'attribut {@link #url}.
     *
     * @param url
     *            la nouvelle valeur de l'attribut url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
