/**
 *
 */
package fr.ge.common.utils.property;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

/**
 * Read properties file containing public links as properties to display.
 *
 * @author $Author: aolubi $
 */
@Component
public class LinkPropertyPlaceholder extends PropertyPlaceholderConfigurer {

    /** Property value pattern expected. **/
    private final Pattern PROPERTY_VALUE_PATTERN = Pattern.compile("(.+)\\|(.+)");

    /** Property values as map. **/
    private Map<String, String> props = new LinkedHashMap<String, String>();

    @Override
    protected void processProperties(final ConfigurableListableBeanFactory beanFactory, final Properties props) throws BeansException {
        this.getProps().clear();

        // -->Read properties line by line
        final Properties tmp = new Properties() {
            @Override
            public synchronized Set<Map.Entry<Object, Object>> entrySet() {
                return Collections.synchronizedSet(super.entrySet().stream().sorted(Comparator.comparing(e -> e.getKey().toString())).collect(Collectors.toCollection(LinkedHashSet::new)));
            }
        };
        tmp.putAll(props);
        // <--

        for (final Entry<Object, Object> e : tmp.entrySet()) {
            final String value = e.getValue().toString();
            final Matcher matcher = PROPERTY_VALUE_PATTERN.matcher(value);
            if (matcher.matches()) {
                final String label = matcher.group(1);
                final String target = matcher.group(2);
                this.getProps().put(label, target);
            }
        }
        super.processProperties(beanFactory, props);
    }

    @Override
    protected void doProcessProperties(final ConfigurableListableBeanFactory beanFactoryToProcess, final StringValueResolver valueResolver) {
        super.doProcessProperties(beanFactoryToProcess, valueResolver);

        for (final Entry<String, String> e : this.getProps().entrySet()) {
            e.setValue(valueResolver.resolveStringValue(e.getValue()));
        }
    }

    /**
     * Return properties as a map.
     * 
     * @return
     */
    public Map<String, String> getProps() {
        return this.props;
    }
}
