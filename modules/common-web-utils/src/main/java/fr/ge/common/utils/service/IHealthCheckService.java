/**
 * 
 */
package fr.ge.common.utils.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author $Author: ijijon $
 */

public interface IHealthCheckService {

    /**
     * This method has a default behavior; please overload if needed. Gives the
     * application status : "OK" if app is up and running.
     *
     * @return the response
     */
    @GET
    @Path("/healthcheck")
    @Produces(MediaType.TEXT_PLAIN)
    default Response healthCheck() {
        return Response.ok().entity("ok").build();
    }

}
