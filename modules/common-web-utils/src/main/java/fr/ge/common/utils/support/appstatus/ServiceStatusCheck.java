/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.support.appstatus;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.Base64;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.appstatus.core.check.AbstractCheck;
import net.sf.appstatus.core.check.CheckResultBuilder;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Application availability.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class ServiceStatusCheck extends AbstractCheck {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceStatusCheck.class);

    /** HTTP OK code. */
    private static final int HTTP_OK = 200;

    /** Application name. */
    private final String appName;

    /** URL. */
    private final String url;

    /** Username. */
    private String username;

    /** Password. */
    private String password;

    /** Connect timeout (milliseconds). */
    private int connectTimeout = 500;

    /** Read timeout (milliseconds). */
    private int readTimeout = 500;

    /** Availability TTL (seconds). */
    private Duration availabilityTtl = Duration.ofSeconds(5);

    /** Maximum reliable availability date. */
    private Temporal maxReliableAvailabilityDate;

    /**
     * Constructor.
     *
     * @param name
     *            the name
     * @param url
     *            the URL
     */
    public ServiceStatusCheck(final String name, final String url) {
        this.appName = name;
        this.url = url;
    }

    /**
     * Gets a connection from the URL.
     *
     * @return the connection
     * @throws IOException
     *             an {@link IOException}
     */
    public HttpURLConnection getConnection() throws IOException {
        return (HttpURLConnection) new URL(this.url).openConnection();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICheckResult checkStatus(final Locale locale) {
        final CheckResultBuilder result = this.result().from(this);
        final Temporal now = Instant.now();
        boolean available = false;
        if (this.maxReliableAvailabilityDate != null && !Duration.between(now, this.maxReliableAvailabilityDate).isNegative()) {
            available = true;
        } else {
            try {
                final HttpURLConnection httpUrlConnection = this.getConnection();
                httpUrlConnection.setConnectTimeout(this.connectTimeout);
                httpUrlConnection.setReadTimeout(this.readTimeout);
                if (StringUtils.isNotEmpty(this.username)) {
                    final String credentials = Base64.getEncoder().encodeToString((this.username + ":" + this.password).getBytes(StandardCharsets.UTF_8));
                    httpUrlConnection.setRequestProperty("Authorization", "Basic " + credentials);
                }
                if (httpUrlConnection.getResponseCode() == HTTP_OK) {
                    available = true;
                    this.maxReliableAvailabilityDate = now.plus(this.availabilityTtl);
                } else {
                    result.resolutionSteps(this.url + " - HTTP " + httpUrlConnection.getResponseCode() + " : " + httpUrlConnection.getResponseMessage());
                }
            } catch (final Exception e) {
                result.resolutionSteps(this.url + " - " + e.getMessage());
                LOGGER.info(StringUtils.EMPTY, e);
            }
        }
        if (available) {
            result.code(ICheckResult.OK).description(this.appName + " available");
        } else {
            result.code(ICheckResult.ERROR).description(this.appName + " unavailable");
            this.maxReliableAvailabilityDate = null;
        }
        ICheckResult checkResult = result.build();
        LOGGER.info("Code return {} and availibility '{}'", checkResult.getCode(), checkResult.getDescription());
        return checkResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getGroup() {
        return "Applications";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Availability";
    }

    /**
     * Setter on attribute {@link #username}.
     *
     * @param username
     *            the new value of attribute username
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Setter on attribute {@link #password}.
     *
     * @param password
     *            the new value of attribute password
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Set connection timeout attribute, in milliseconds.
     *
     * @param connectTimeout
     *            the new value of attribute connectTimeout
     */
    public void setConnectTimeout(final int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    /**
     * Set read timeout attribute, in milliseconds.
     *
     * @param readTimeout
     *            the new value of attribute readTimeout
     */
    public void setReadTimeout(final int readTimeout) {
        this.readTimeout = readTimeout;
    }

    /**
     * Set TTL cache in seconds.
     *
     * @param availabilityTtl
     *            the new value of attribute availabilityTtl
     */
    public void setAvailabilityTtl(final int availabilityTtl) {
        this.availabilityTtl = Duration.ofSeconds(availabilityTtl);
    }

}
