/**
 *
 */
package fr.ge.common.utils.support.appstatus;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import net.sf.appstatus.core.check.ICheckResult;

@RunWith(MockitoJUnitRunner.class)
public class ServiceStatusCheckTest {

    @Spy
    final ServiceStatusCheck checker = new ServiceStatusCheck("FooBar app", "http://localhost/fooBar");

    @Before
    public void setUp() throws Exception {
        reset(this.checker);
        this.checker.setAvailabilityTtl(2 * 60 * 60);
    }

    /**
     * Tests {@link AppAvailability#checkStatus(java.util.Locale)}.
     */
    @Test
    public void testCheckStatusAvailable() throws Exception {
        final String username = "usr-test";
        final String password = "pwd-test";
        final int connectTimeout = 2000;
        final int readTimeout = 5000;

        this.checker.setUsername(username);
        this.checker.setPassword(password);
        this.checker.setConnectTimeout(connectTimeout);
        this.checker.setReadTimeout(readTimeout);

        final HttpURLConnection urlConnection = this.buildConnectionMock(200);
        doReturn(urlConnection).when(this.checker).getConnection();

        final ICheckResult result = this.checker.checkStatus(Locale.FRANCE);

        assertThat(result, //
                allOf( //
                        hasProperty("code", equalTo(0)), //
                        hasProperty("description", equalTo("FooBar app available")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", nullValue()) //
                ) //
        );

        verify(urlConnection).setConnectTimeout(eq(connectTimeout));
        verify(urlConnection).setReadTimeout(eq(readTimeout));
        verify(urlConnection).setRequestProperty(eq("Authorization"), eq("Basic " + Base64.getEncoder().encodeToString(String.format("%s:%s", username, password).getBytes(StandardCharsets.UTF_8))));
    }

    private HttpURLConnection buildConnectionMock(final int httpCode) throws Exception {
        final HttpURLConnection urlConnection = mock(HttpURLConnection.class);

        when(urlConnection.getResponseCode()).thenReturn(httpCode);
        when(urlConnection.getResponseMessage()).thenThrow(new IOException("Error during HTTP call"));

        return urlConnection;
    }

    private HttpURLConnection buildConnectionMock(final int httpCode, final String message) throws Exception {
        final HttpURLConnection urlConnection = mock(HttpURLConnection.class);

        when(urlConnection.getResponseCode()).thenReturn(httpCode);
        when(urlConnection.getResponseMessage()).thenReturn(message);

        return urlConnection;
    }

    /**
     * Tests {@link AppAvailability#checkStatus(java.util.Locale)}.
     */
    @Test
    public void testCheckStatusAvailableCacheHit() throws Exception {
        doReturn(this.buildConnectionMock(200, "OK")).when(this.checker).getConnection();

        final ICheckResult resultFirst = this.checker.checkStatus(Locale.FRANCE);
        final ICheckResult resultSecond = this.checker.checkStatus(Locale.FRANCE);

        assertThat(resultFirst, //
                allOf( //
                        hasProperty("code", equalTo(0)), //
                        hasProperty("description", equalTo("FooBar app available")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", nullValue()) //
                ) //
        );

        assertThat(resultSecond, //
                allOf( //
                        hasProperty("code", equalTo(0)), //
                        hasProperty("description", equalTo("FooBar app available")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", nullValue()) //
                ) //
        );

        verify(this.checker, times(1)).getConnection();
    }

    /**
     * Tests {@link AppAvailability#checkStatus(java.util.Locale)}.
     */
    @Test
    public void testCheckStatusAvailableCacheMiss() throws Exception {
        doReturn(this.buildConnectionMock(200, "OK")).when(this.checker).getConnection();
        this.checker.setAvailabilityTtl(-1);

        final ICheckResult resultFirst = this.checker.checkStatus(Locale.FRANCE);
        final ICheckResult resultSecond = this.checker.checkStatus(Locale.FRANCE);

        assertThat(resultFirst, //
                allOf( //
                        hasProperty("code", equalTo(0)), //
                        hasProperty("description", equalTo("FooBar app available")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", nullValue()) //
                ) //
        );

        assertThat(resultSecond, //
                allOf( //
                        hasProperty("code", equalTo(0)), //
                        hasProperty("description", equalTo("FooBar app available")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", nullValue()) //
                ) //
        );

        verify(this.checker, times(2)).getConnection();
    }

    /**
     * Tests {@link AppAvailability#checkStatus(java.util.Locale)}.
     */
    @Test
    public void testCheckStatusUnavailableHttpCode() throws Exception {
        doReturn(this.buildConnectionMock(503, "Service Unavailable")).when(this.checker).getConnection();

        final ICheckResult result = this.checker.checkStatus(Locale.FRANCE);

        assertThat(result, //
                allOf( //
                        hasProperty("code", equalTo(-1)), //
                        hasProperty("description", equalTo("FooBar app unavailable")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", equalTo("http://localhost/fooBar - HTTP 503 : Service Unavailable")) //
                ) //
        );
    }

    /**
     * Tests {@link AppAvailability#checkStatus(java.util.Locale)}.
     */
    @Test
    public void testCheckStatusUnavailableException() throws Exception {
        doReturn(this.buildConnectionMock(500)).when(this.checker).getConnection();

        final ICheckResult result = this.checker.checkStatus(Locale.FRANCE);

        assertThat(result, //
                allOf( //
                        hasProperty("code", equalTo(-1)), //
                        hasProperty("description", equalTo("FooBar app unavailable")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", equalTo("http://localhost/fooBar - Error during HTTP call")) //
                ) //
        );
    }

}
