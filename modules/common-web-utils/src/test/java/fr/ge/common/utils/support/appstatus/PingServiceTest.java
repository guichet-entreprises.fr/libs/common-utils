package fr.ge.common.utils.support.appstatus;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.exception.TechnicalException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/spring/appstatus-cache-context.xml" })
public class PingServiceTest {

    @Autowired
    private PingService pingService;

    @Test
    public void testPingWithSuccess() throws TechnicalException {
        boolean isReachable1 = pingService.ping("Hello world", "localhost");
        boolean isReachable2 = pingService.ping("Hello world", "localhost");
        assertEquals(isReachable1, isReachable2);
    }

    @Test(expected = TechnicalException.class)
    public void testPingWithFailure() throws TechnicalException {
        pingService.ping("Hello world", "a?§%1");
    }
}
