/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.utils.support.appstatus;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Properties;

import org.junit.Test;

/**
 * Class PropertyProviderTest.
 *
 * @author Christian Cougourdan
 */
public class PropertyProviderTest {

    /**
     * Test simple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSimple() throws Exception {
        final Properties properties = new Properties();
        properties.setProperty("prop01", "value 01");
        properties.setProperty("prop02", "value 02");
        properties.setProperty("prop03.password", "value 03.1");
        properties.setProperty("prop03.passwd", "value 03.2");
        properties.setProperty("prop03.pwd", "value 03.3");

        final PropertyProvider provider = new PropertyProvider();
        provider.setProperties(properties);

        assertEquals("Configuration", provider.getCategory());
        assertThat(provider.getProperties().entrySet(), hasSize(5));
        assertThat( //
                provider.getProperties(), //
                allOf( //
                        hasEntry("prop01", "value 01"), //
                        hasEntry("prop02", "value 02"), //
                        hasEntry("prop03.password", "********"), //
                        hasEntry("prop03.passwd", "********"), //
                        hasEntry("prop03.pwd", "********") //
                ) //
        );
    }

    /**
     * Test no properties.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testNoProperties() throws Exception {
        final PropertyProvider provider = new PropertyProvider();
        provider.setProperties(null);

        assertEquals("Configuration", provider.getCategory());
        assertThat(provider.getProperties().entrySet(), hasSize(0));
    }

    /**
     * Test empty.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEmpty() throws Exception {
        final PropertyProvider provider = new PropertyProvider();
        provider.setProperties(new Properties());

        assertEquals("Configuration", provider.getCategory());
        assertThat(provider.getProperties().entrySet(), hasSize(0));
    }

}
