package fr.ge.common.utils.support.appstatus;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.exception.TechnicalException;
import net.sf.appstatus.core.check.ICheckResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/spring/appstatus-cache-context.xml" })
public class ServicePingCheckerTest {

    @InjectMocks
    private ServicePingChecker pingChecker = new ServicePingChecker("HelloWorld app", "localhost");

    @Mock
    private PingService pingService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPingAvailable() throws Exception {
        Mockito.when(this.pingService.ping(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        ICheckResult checkResult = pingChecker.checkStatus(null);

        assertThat(checkResult, //
                allOf( //
                        hasProperty("code", equalTo(0)), //
                        hasProperty("description", equalTo("HelloWorld app available")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", nullValue()) //
                ) //
        );
    }

    @Test
    public void testPingUnavailable() throws Exception {
        Mockito.when(this.pingService.ping(Mockito.anyString(), Mockito.anyString())).thenThrow(new TechnicalException("Ping failed !!"));
        ICheckResult checkResult = pingChecker.checkStatus(null);

        assertThat(checkResult, //
                allOf( //
                        hasProperty("code", equalTo(-1)), //
                        hasProperty("description", equalTo("HelloWorld app unavailable")), //
                        hasProperty("group", equalTo("Applications")), //
                        hasProperty("probeName", equalTo("Availability")), //
                        hasProperty("resolutionSteps", equalTo("localhost - Ping failed !!")) //
                ) //
        );
    }
}
