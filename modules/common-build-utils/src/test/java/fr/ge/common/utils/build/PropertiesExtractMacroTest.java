/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.build;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.apache.maven.doxia.macro.MacroExecutionException;
import org.junit.Test;

/**
 *
 * @author Christian Cougourdan
 */
public class PropertiesExtractMacroTest {

    @Test
    public void testReadPropertiesFromNull() throws Exception {
        try {
            new PropertiesExtractMacro().readProperties(null);
            fail("MacroExecutionException expected");
        } catch (final MacroExecutionException ex) {
            assertThat(ex.getMessage(), equalTo("No properties source file specified"));
        }
    }

    @Test
    public void testReadPropertiesFromUnknown() throws Exception {
        try {
            new PropertiesExtractMacro().readProperties("./test-unknown.properties");
            fail("MacroExecutionException expected");
        } catch (final MacroExecutionException ex) {
            assertThat(ex.getMessage(), equalTo("[./test-unknown.properties] source file not found"));
        }
    }

    @Test
    public void testReadProperties() throws Exception {
        final Collection<PropertyDescription> properties = new PropertiesExtractMacro().readProperties("target/test-classes/test.properties");

        assertThat(properties, //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("name", equalTo("environment")), //
                                        hasProperty("value", equalTo("\"Development\", \"Recette\", ... . Don't fill any value to hide ribbon.")), //
                                        hasProperty("description", equalTo("Environnement name, shown on every page in ribbon")), //
                                        hasProperty("token", equalTo("@environment@")) //
                                ), //
                                allOf( //
                                        hasProperty("name", equalTo("ui.theme.default")), //
                                        hasProperty("value", nullValue()), //
                                        hasProperty("description", equalTo("Used UI theme. Depends on application")), //
                                        hasProperty("token", equalTo("ge-v2")) //
                                ) //
                        ) //
                ) //
        );
    }

}
