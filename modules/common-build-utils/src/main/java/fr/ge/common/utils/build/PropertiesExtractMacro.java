/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.utils.build;

import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.doxia.macro.AbstractMacro;
import org.apache.maven.doxia.macro.Macro;
import org.apache.maven.doxia.macro.MacroExecutionException;
import org.apache.maven.doxia.macro.MacroRequest;
import org.apache.maven.doxia.sink.Sink;
import org.codehaus.plexus.component.annotations.Component;

/**
 *
 * @author Christian Cougourdan
 */
@Component(role = Macro.class, hint = "properties")
public class PropertiesExtractMacro extends AbstractMacro {

    @Override
    public void execute(final Sink sink, final MacroRequest request) throws MacroExecutionException {
        final Object sourceFilename = request.getParameter("source");
        this.getLog().info("Source : " + sourceFilename);
        if (null == sourceFilename) {
            throw new MacroExecutionException("Properties source file not specified");
        } else if (!(sourceFilename instanceof String)) {
            throw new MacroExecutionException("Bad properties source file parameters, expected String");
        }

        final Collection<PropertyDescription> properties = this.readProperties((String) sourceFilename);

        sink.table();

        sink.tableRow();
        Arrays.asList("Paramètre", "Description", "Valeur").forEach(str -> {
            sink.tableHeaderCell();
            sink.text(str);
            sink.tableHeaderCell_();
        });
        sink.tableRow_();

        properties.forEach(property -> {
            sink.tableRow();

            sink.tableCell();
            sink.text(property.getName());
            sink.tableCell_();

            sink.tableCell();
            sink.text(property.getDescription());
            sink.tableCell_();

            sink.tableCell();
            sink.text(property.getValue());
            sink.tableCell_();

            sink.tableRow_();
        });

        sink.table_();
    }

    public Collection<PropertyDescription> readProperties(final String sourceFilename) throws MacroExecutionException {
        if (StringUtils.isEmpty(sourceFilename)) {
            this.getLog().info("No properties source file specified");
            throw new MacroExecutionException("No properties source file specified");
        }

        return new PropertyReader(this.getLog()).read(sourceFilename);
    }

}
